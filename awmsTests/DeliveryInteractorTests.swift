//
//  DeliveryInteractorTests.swift
//  awmsTests
//
//  Created by michael on 18.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

@testable import awms
import XCTest
import RIBs

// MARK: - Mocking
class FakeDeliveryNetwork: NetworkProtocol {
    func uploadPNG(url: String, data: Data, name: String, completion: ((NSError?) -> Void)?) {
    }

    func changeCredentials(user: String, pass: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        /*    2    6
	         / \   |
		 0--1   4--5
			 \ /   |
			  3    7
         */
        let e1 = ["from": 0, "to": 1, "weight": 1.0]
        let e2 = ["from": 1, "to": 2, "weight": 2.0]
        let e3 = ["from": 2, "to": 4, "weight": 2.0]
        let e4 = ["from": 1, "to": 3, "weight": 1.0]
        let e5 = ["from": 3, "to": 4, "weight": 1.0]
        let e6 = ["from": 4, "to": 5, "weight": 1.0]
        let e7 = ["from": 5, "to": 6, "weight": 1.0]
        let e8 = ["from": 5, "to": 7, "weight": 1.0]
        let dict = ["v": 8,
                    "e": 16,
                    "vertices": [e1, e2, e3, e4, e5, e6, e7, e8]] as NSDictionary
        success!(dict)
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
		// coordinates of the graph's nodes
		let c1 = ["id": 0, "x": 0, 	 "y": 100]
		let c2 = ["id": 1, "x": 100, "y": 100]
		let c3 = ["id": 2, "x": 150, "y": 0]
		let c4 = ["id": 3, "x": 150, "y": 200]
		let c5 = ["id": 4, "x": 200, "y": 100]
		let c6 = ["id": 5, "x": 250, "y": 100]
		let c7 = ["id": 6, "x": 250, "y": 0]
		let c8 = ["id": 7, "x": 250, "y": 200]
		let dict = [c1, c2, c3, c4, c5, c6, c7, c8] as [NSDictionary]
		success!(dict)
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
    }
}

class FakeScanner: ScannerProtocol {
    func bindTo(delegate: ScannerDelegate) {
        // DO NOTHING
    }

    func unbind(delegate: ScannerDelegate) {
        // DO NOTHING
    }
}

// MARK: - Dependencies

class FakeDeliveryDependency: DeliveryDependency {
    var storage: StorageService
    var scanner: ScannerProtocol
    var saver: FileService
    var mapLoader: MapService
    var network: NetworkProtocol
    var storyboard: UIStoryboard {
        get {
            return UIStoryboard()
        }
    }
    var themeManager: ThemeManagerProtocol {
        get {
            return ThemeManager()
        }
    }
    init(storage: StorageService,
         scanner: ScannerProtocol,
         saver: FileService,
         mapLoader: MapService,
         network: NetworkProtocol) {
        self.storage = storage
        self.scanner = scanner
        self.saver = saver
        self.network =  network
        self.mapLoader = mapLoader
    }
}

class FakePresenter: DeliveryPresentable {
    var listener: DeliveryPresentableListener?
    var showCanvasProbe: (([MapElement]) -> Void)?
    var showFinishedStateProbe: ((DeliverState) -> Void)?

    init(showCanvasProbe: (([MapElement]) -> Void)?, finishedStateProbe: ((DeliverState) -> Void)?) {
        self.showCanvasProbe = showCanvasProbe
        self.showFinishedStateProbe = finishedStateProbe
    }

    func show(canvas: [MapElement]) {
        if let sc = showCanvasProbe {
            sc(canvas)
        }
    }

    func removePopUp() {
    }

    func showInputDialog(title: String, message: String, okAction: ((String) -> Void)?) {
    }

    func show(state: DeliverState) {
        if let st = showFinishedStateProbe {
            st(state)
        }
    }

    func show(help: String) {
    }

    func moveTo(point: CGPoint) {
    }

    func show(title: String, error: String) {
    }

    func show(title: String, error: String, action: ((UIAlertAction) -> Void)?) {
    }

    func show(title: String, message: String, action: ((UIAlertAction) -> Void)?) {
    }

    func showYesNO(title: String, message: String, actionYes: ((UIAlertAction) -> Void)?, actionNo: ((UIAlertAction) -> Void)?) {
    }

    func showBusyIndicator() {
    }

    func hideBusyIndicator() {
    }
}

// MARK: - Testing

class DeliveryInteractorTests: XCTestCase {
    var mapLoader: MapService!
    var interactor: DeliveryInteractor!
    var presenter: FakePresenter!
    let cache = Storage(login: kUser)
    let scanner = FakeScanner()
    let saver = FakeSaver()
    let network = FakeDeliveryNetwork()
    var callCount = 0

    override func setUp() {
        super.setUp()
        mapLoader = MapLoader(network: self.network,
                              cache: self.cache,
                              saver: self.saver)
    }

    override func tearDown() {
        unprepareData {
            super.tearDown()
        }
    }

    func prepareData(completion: @escaping (() -> Void)) {
        // given data, items must be put in the user's bag:
        var i1 = cItem(qr: "123", amount: 1, ean: 123, s: 1, h: 1, m: 1)
        var i2 = cItem(qr: "424", amount: 2, ean: 124, s: 1, h: 1, m: 2)
        var i3 = cItem(qr: "464", amount: 1, ean: 464, s: 1, h: 2, m: 1)
        let capability = Capability(id: 1, s: 10, h: 10, m: 10)
        let usr = User(login: kUser, name: kUser, avatar: "", capabilities: [capability])
        cache.update(user: usr) { [unowned self] in
            // items should be delivered
            i1.status = .toDeliver
            i1.storedIn = 4
            i2.status = .toDeliver
            i2.storedIn = 5
            i3.status = .toDeliver
            i3.storedIn = 7
            let task = TaskScan(items: [i1, i2, i3])
            // add tasks
            self.cache.create(tasks: [task]) {
                let notify = DispatchGroup()
                for i in [i1, i2, i3] {
                    notify.enter()
                    self.cache.putIntoBag(item: i) { notify.leave() }
                }
                notify.enter()
                self.mapLoader.fetch(graph: kMap) { err in
                    self.mapLoader.fetchCoordinatesOf(graph: kMap) { err in
                        notify.leave()
                    }
                }
                notify.notify(queue: .main) {
                    completion()
                }
            }
        }
    }

    func unprepareData(completion: @escaping (() -> Void)) {
        cache.delete(graph: kMap) { [unowned self] in
            self.cache.delete(user: kUser) {
                self.callCount = 0
                completion()
            }
        }
    }

    func testInteractorInit() {
        let exp = self.expectation(description: "DeliveryInteractor init test")
        prepareData { [unowned self] in
            let dependencies = FakeDeliveryDependency(storage: self.cache,
                                   scanner: self.scanner,
                                   saver: self.saver,
                                   mapLoader: self.mapLoader,
                                   network: self.network)
            let c = DeliveryComponent(dependency: dependencies,
                                      map: kMap,
                                      id: 1,
                                      startPoint: 0)
            self.presenter = FakePresenter(showCanvasProbe: { items in
                // ensure that we've got the initial path to the items
                XCTAssert(items.count == 4, "real count: \(items.count)")
                XCTAssert(items[0].type == .path, "0. real type: \(items[0].type)")
                XCTAssert(items[1].type == .currentPoint, "1.real type: \(items[1].type)")
                XCTAssert(items[2].type == .point, "2.real type: \(items[2].type)")
                XCTAssert(items[3].type == .point, "3.real type: \(items[3].type)")
                // check the path to the closest point
                let p = items[0] as! MapElementPath
                XCTAssert(p.path.count == 4, "real amount of points in path: \(p.path.count)")
                for i in p.path.indices {
                    let pp = p.path[i]
                    if i == 0 { // node -> 4
                        XCTAssert(pp.x == 200 && pp.y == 100, "(\(pp.x), \(pp.y))")
                    } else if i == 1 { // node -> 3
                        XCTAssert(pp.x == 150 && pp.y == 200, "(\(pp.x), \(pp.y))")
                    } else if i == 2 { // node -> 1
                        XCTAssert(pp.x == 100 && pp.y == 100, "(\(pp.x), \(pp.y))")
                    } else if i == 3 { // node -> 0
                        XCTAssert(pp.x == 0 && pp.y == 100, "(\(pp.x), \(pp.y))")
                    }
                }
                let pt = items[1] as! MapElementPoint
                XCTAssert(pt.point.x == 200 && pt.point.y == 100, "(\(pt.point.x), \(pt.point.y))")
                self.unprepareData {
                    exp.fulfill()
                }
            }, finishedStateProbe:nil)
            self.interactor = DeliveryInteractor(presenter: self.presenter,
                                                 component: c)
            self.interactor.viewWillAppear()
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
