//
//  ShortPathTests.swift
//  awmsTests
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
@testable import awms

class ShortPathTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testPriorityQueue() {
        struct indexKey {
            let ind: Int
            let key: Double

            init(_ i: Int, key: Double) {
                ind = i
                self.key = key
            }
        }
        let cmp = {(a: indexKey, b: indexKey) -> Bool in
            return a.key < b.key
        }
        let pq = PriorityQueue<indexKey>(cmp)
        pq.push(indexKey(1, key: 0))
        XCTAssertTrue(pq.pop()!.ind == 1)
        pq.push(indexKey(0, key: 8))
        pq.push(indexKey(2, key: 3))
        XCTAssertTrue(pq.pop()!.ind == 2)
        XCTAssertTrue(pq.pop()!.ind == 0)
        pq.push(indexKey(3, key: 7))
        XCTAssertTrue(pq.pop()!.ind == 3)
    }

    func testSquareRouteReversed() {
        let g = Graph(V: 6)
        let exp = self.expectation(description: "test graph")
        /*    2
	         / \
         0--1   4--5    Both directions are included    <-->
         	 \ /
              3
        */
        g.addEdgeDS(Edge(v: 0, w: 1, weight: 1.0))
        g.addEdgeDS(Edge(v: 1, w: 2, weight: 2.0))
        g.addEdgeDS(Edge(v: 1, w: 3, weight: 5.0))
        g.addEdgeDS(Edge(v: 2, w: 4, weight: 2.0))
        g.addEdgeDS(Edge(v: 3, w: 4, weight: 1.0))
        g.addEdgeDS(Edge(v: 4, w: 5, weight: 1.0))
        let sp = Dijkstra(graph: g)
        sp.calculateDistancesFrom(startPoint: 5) {
            let route = sp.pathTo(finalDestination: 0)
            XCTAssertTrue(route.count == 4)
            XCTAssertTrue(sp.distTo[0] == 6.0)
            exp.fulfill()
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
