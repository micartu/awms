//
//  ordererTests.swift
//  awmsTests
//
//  Created by michael on 16.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
@testable import awms

class FakeOrdererNetwork: NetworkProtocol {
    func uploadPNG(url: String, data: Data, name: String, completion: ((NSError?) -> Void)?) {
    }

    func changeCredentials(user: String, pass: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        let c1 = ["id": 1, "qr": "1", "s": 1, "h": 1, "m": 1, "cellid": 2] as [String:Any]
        let dict = [c1] as [NSDictionary]
        success!(dict)
    }
}

class ordererTests: XCTestCase {
    let cache = Storage(login: kUser)
    let network = FakeOrdererNetwork()
    var orderer: Orderer!

    override func setUp() {
        orderer = Orderer(network: network, cache: cache)
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testOrderItem() {
        let i = cItem(qr: "1", amount: 1, ean: 1, s: 1, h: 1, m: 1)
        orderer.order(items: [i]) { (items, err) in
            let item = items?.first
            XCTAssert(item?.storedIn == 2)
        }
    }
}
