//
//  storageTests.swift
//  storageTests
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
@testable import awms

// MARK: - Helper functions
func cItem(qr: String, amount: Double, ean: Int, s: Double, h: Double, m: Double) -> Item {
    let i = Item(id: 0,
                 qr: qr,
                 amount: amount,
                 ean: ean,
                 status: .unknown,
                 scanned: 0,
                 storedIn: -1,
                 title: "",
                 descr: "",
                 imgName: "",
                 img: nil,
                 s: s,
                 h: h,
                 m: m)
    return i
}

class storageTests: XCTestCase {
    private var cache: StorageService? = nil

    override func setUp() {
        super.setUp()
        cache = Storage(login: kUser)
    }

    override func tearDown() {
        cache?.delete(user: kUser) {
            super.tearDown()
        }
    }

    // MARK: - Tests
    func testUserCreation() {
        let exp = self.expectation(description: "user creation")
        let capability = Capability(id: 1, s: 1, h: 1, m: 1)
        let usr = User(login: kUser, name: kUser, avatar: "", capabilities: [capability])
        cache?.update(user: usr) {
            if let u = self.cache?.get(user: kUser) {
                XCTAssert(u.capabilities != nil)
                XCTAssert(u.capabilities!.count > 0)
                let c = u.capabilities!.first!
                XCTAssert(c.m == 1, "mass must be 1, but in reality it's: \(c.m)")
                if u.login == kUser {
                    self.cache?.delete(user: kUser) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testTasksCreation() {
        let exp = self.expectation(description: "tasks for user creation")
        let i1 = cItem(qr: "123", amount: 1, ean: 123, s: 1, h: 2, m: 3)
        let i2 = cItem(qr: "424", amount: 2, ean: 124, s: 1, h: 2, m: 2)
        let i3 = cItem(qr: "464", amount: 1, ean: 464, s: 1, h: 2, m: 1)
        let task1 = TaskScan(items: [i1, i2, i3])
        let task2 = TaskDelivery(map: "test",
                                 startNode: 0)
        cache?.update(tasks: [task1, task2]) {
            task1.state = .finished
            self.cache?.update(task: task1) {
                let tasks = self.cache?.getTasksFor(user: kUser)
                XCTAssert(tasks != nil)
                XCTAssert(tasks!.count > 0, "Tasks count must be more then zero!")
                // check the order of the returned tasks
                let t1 = tasks?.first! as! TaskScan
                let t2 = tasks?.last!
                XCTAssert(task1.type == t1.type)
                XCTAssert(task2.type == t2!.type)

                // check order of items
                let item1 = t1.items!.first!
                XCTAssert(i1.ean == item1.ean)

                XCTAssert(task1.state == .finished)
                self.cache?.delete(user: kUser) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testBagCapacity() {
        let exp = self.expectation(description: "bag testing")
        let capability = Capability(id: 1, s: 1, h: 1, m: 1)
        let usr = User(login: kUser, name: kUser, avatar: "", capabilities: [capability])
        cache?.update(user: usr) {
            var i1 = cItem(qr: "123", amount: 1, ean: 123, s: 1, h: 1, m: 1)
            var i2 = cItem(qr: "424", amount: 2, ean: 124, s: 1, h: 1, m: 2)
            var i3 = cItem(qr: "464", amount: 1, ean: 464, s: 1, h: 2, m: 1)
            let task = TaskScan(items: [i1, i2, i3])
            // add tasks
            self.cache?.create(tasks: [task]) {
                // we've scanned all items once
                i1.scanned = 1
                i2.scanned = 1
                i3.scanned = 1
                // can we put the first item into the bag? yes, should have
                XCTAssert(self.cache?.canPutIntoBag(item: i1) == true)
                // the third one should be too big for it already
                XCTAssert(self.cache?.canPutIntoBag(item: i3) == false)
                self.cache?.putIntoBag(item: i1) {
                    // and the second, no, it's already full
                    XCTAssert(self.cache?.canPutIntoBag(item: i2) == false)
                    self.cache?.delete(user: kUser) {
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
