//
//  serviceTests.swift
//  awmsTests
//
//  Created by Michael Artuerhof on 11.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import XCTest
@testable import awms

let kUser = "test"
let kMap = "testMap"
let kTimeout: TimeInterval = 5

class FakeNetwork: NetworkProtocol {
    func uploadPNG(url: String, data: Data, name: String, completion: ((NSError?) -> Void)?) {
    }

    func changeCredentials(user: String, pass: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        let e1 = ["from": 0, "to": 1, "weight": 0.5]
        let e2 = ["from": 0, "to": 2, "weight": 1.5]
        let e3 = ["from": 1, "to": 3, "weight": 0.5]
        let e4 = ["from": 2, "to": 3, "weight": 1.5]
        let dict = ["v": 4,
                    "e": 4,
                    "vertices": [e1, e2, e3, e4]] as NSDictionary
        success!(dict)
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        let c1 = ["id": 0, "x": 1, "y": 3.5]
        let c2 = ["id": 1, "x": 2, "y": 1.5]
        let c3 = ["id": 2, "x": 3, "y": 2.5]
        let c4 = ["id": 3, "x": 4, "y": 1.5]
        let dict = [c1, c2, c3, c4] as [NSDictionary]
        success!(dict)
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
    }
}

class FakeSaver: FileService {
    func remove(image: String) {
    }

    func contentsOf(image: String) -> Data? {
        return nil
    }

    func load(image: String) -> UIImage? {
        return nil
    }

    func save(_ type: FileTypes, name: String, with data: Data) {
    }

    func contentsOfMap(_ name: String) -> String {
        return ""
    }
}

class serviceTests: XCTestCase {
    let cache = Storage(login: kUser)
    let saver = FakeSaver()
    let network = FakeNetwork()
    var mapService: MapService! = nil

    override func setUp() {
        super.setUp()
        mapService = MapLoader(network: network, cache: cache, saver: saver)
    }

    override func tearDown() {
        super.tearDown()
    }

    func testMapService() {
        let exp = self.expectation(description: "map creation/loading")
        mapService.fetch(graph: kMap, answer: { [unowned self] err in
            self.mapService.get(graph: kMap) { gr, err in
                XCTAssert(gr!.V == 4, "but the amount of vertices was: \(gr!.V)")
                // test if the graph was saved in cache:
                let g = self.cache.get(graph: kMap)!
                XCTAssert(g.V == 4, "but the amount of vertices was: \(g.V)")
                self.cache.delete(graph: kMap) {
                    exp.fulfill()
                }
            }
        })
        wait(for: [exp], timeout: kTimeout)
    }

    func testCoordsOfMapService() {
        let exp = self.expectation(description: "map loading of coordinates")
        mapService.fetchCoordinatesOf(graph: kMap) { [unowned self] err in
            self.mapService.getMapCoordinatesOf(graph: kMap) { coords, err in
                XCTAssert(coords!.count == 4, "but the amount of coordinates were: \(coords!.count)")
                let cx = self.cache.getCoordForMap(name: kMap)!
                XCTAssert(coords!.count == cx.count)
                for c in cx {
                    if c.Id == 1 {
                        XCTAssert(c.x == 2 && c.y == 1.5,
                                  "but the coordinates were x: \(c.x) y: \(c.y)")
                        break
                    }
                }
                self.cache.delete(graph: kMap) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
}
