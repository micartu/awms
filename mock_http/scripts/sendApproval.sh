#!/bin/sh

ean=$1
node=$2

if [[ -z "$ean" || -z "$node" ]]; then
	echo "Usage: `basename $0` ean node"
	exit -1
fi

echo "QR node|$node|node" | nc 127.0.0.1 8778
sleep 1
echo "EAN13 $ean|ean" | nc 127.0.0.1 8778
