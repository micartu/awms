package main

const (
	ERROR_TOKEN_EXPIRED = -1
	ERROR_CODE_EMPTY    = -2
	ERROR_DB            = -3
)
