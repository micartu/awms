package main

import (
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

func pngHandler(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	vars := mux.Vars(r)
	file, err := ioutil.ReadFile("img/" + vars["item"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.Header().Set("Content-type", "image/png")
	w.Write(file)
}
