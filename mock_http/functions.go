package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func max(v, o int) int {
	if v > o {
		return v
	}
	return o
}

func checkCode(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	code := r.Header.Get("code")

	if code == "" {
		desc := "code field is empty"
		errJson := errorJson{
			Code:  ERROR_CODE_EMPTY,
			Descr: desc,
		}
		w.WriteHeader(401) // Unauthorized
		json.NewEncoder(w).Encode(errJson)
		return fmt.Errorf(desc)
	}

	auth, err := state.tstorage.LoadAccess(code)
	if err != nil {
		desc := "token not found!"
		errJson := errorJson{
			Code:  ERROR_CODE_EMPTY,
			Descr: desc,
		}
		w.WriteHeader(401) // Unauthorized
		json.NewEncoder(w).Encode(errJson)
		return err
	}

	if auth.IsExpired() {
		desc := "token is expired"
		errJson := errorJson{
			Code:  ERROR_TOKEN_EXPIRED,
			Descr: desc,
		}
		w.WriteHeader(401) // Unauthorized
		json.NewEncoder(w).Encode(errJson)
		return fmt.Errorf(desc)
	}

	return nil
}

func printGraph(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	json.NewEncoder(w).Encode(state.g)
}

func printCoordinates(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	json.NewEncoder(w).Encode(state.coords)
}

func showTasks(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	json.NewEncoder(w).Encode(state.tasks)
}

func showUser(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	json.NewEncoder(w).Encode(state.user)
}

func showCapabilities(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	json.NewEncoder(w).Encode(state.caps)
}

func addItems(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	var items []itemJson
	_ = json.NewDecoder(r.Body).Decode(&items)
	sendItems := []itemJson{}
	for _, i := range items {
		i.CellId = nextCellStoreId()
		sendItems = append(sendItems, i)
	}
	json.NewEncoder(w).Encode(sendItems)
}

func nextCellStoreId() int {
	last := state.lastCell
	if last < len(state.cells) {
		state.lastCell++
		return state.cells[last]
	} else if len(state.cells) > 0 {
		state.lastCell = 0
		return nextCellStoreId()
	}
	return 0
}

func upload(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	file, handler, err := r.FormFile("fileupload")
	chk(err)
	defer file.Close()

	// just copy a given file
	f, err := os.OpenFile("./upload/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	defer f.Close()
	io.Copy(f, file)

	errJson := errorJson{
		Code:  0,
		Descr: "",
	}

	w.WriteHeader(200) // ok
	json.NewEncoder(w).Encode(errJson)
}

func report(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	var tasks []taskJson
	_ = json.NewDecoder(r.Body).Decode(&tasks)
	for _, t := range tasks {
		fmt.Println("Uploaded task id:", t.Id)
	}
	json.NewEncoder(w).Encode(tasks)
}

func attachments(w http.ResponseWriter, r *http.Request) {
	if err := checkCode(w, r); err != nil {
		log.Println(err)
		return
	}
	var attaches []attachmentJson
	_ = json.NewDecoder(r.Body).Decode(&attaches)
	for _, t := range attaches {
		fmt.Println("Uploaded an attachment id:", t.Id)
	}
	json.NewEncoder(w).Encode(attaches)
}
