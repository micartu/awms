package main

import (
	"fmt"
	"github.com/RangelReale/osin"
	"github.com/gorilla/mux"
	"github.com/sasbury/mini"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
)

type srv struct {
	tstorage *TokenStorage
	server   *osin.Server
	user     userJson
	caps     []capabilityJson
	g        *graphJson
	coords   []coordinateJson
	items    []itemJson
	tasks    []taskJson
	cells    []int
	lastCell int
	login    string
}

type errorJson struct {
	Code  int    `json:"err_code,omitempty"`
	Descr string `json:"description,omitempty"`
}

type graphJson struct {
	V     int        `json:"v,omitempty"`
	E     int        `json:"e,omitempty"`
	Verts []edgeJson `json:"vertices,omitempty"`
}

type edgeJson struct {
	From   int     `json:"from,omitempty"`
	To     int     `json:"to,omitempty"`
	Weight float64 `json:"weight,omitempty"`
}

type cellJson struct {
	Id        int     `json:"id,omitempty"`
	S         float64 `json:"s,omitempty"`
	H         float64 `json:"h,omitempty"`
	M         float64 `json:"m,omitempty"`
	MCritical float64 `json:"mcrit,omitempty"`
	Vused     float64 `json:"vused,omitempty"`
}

type itemJson struct {
	Id    int     `json:"id,omitempty"`
	S     float64 `json:"s,omitempty"`
	H     float64 `json:"h,omitempty"`
	M     float64 `json:"m,omitempty"`
	Qr    string  `json:"qr,omitempty"`
	Ean   int     `json:"ean,omitempty"`
	Img   string  `json:"img,omitempty"`
	Title string  `json:"title,omitempty"`
	Descr string  `json:"descr,omitempty"`
	// optional
	CellId int        `json:"cellid,omitempty"`
	Path   []edgeJson `json:"path,omitempty"`
}

type taskDescrJson struct {
	PointACoord string `json:"a_coord,omitempty"`
	PointADescr string `json:"a_descr,omitempty"`
	PointBCoord string `json:"b_coord,omitempty"`
	PointBDescr string `json:"b_descr,omitempty"`
	Descr       string `json:"descr,omitempty"`
}

type taskJson struct {
	Id        int           `json:"id,omitempty"`
	Type      string        `json:"type,omitempty"`
	State     string        `json:"state,omitempty"`
	Map       string        `json:"map,omitempty"`
	StartNode int           `json:"start_node,omitempty"`
	Descr     taskDescrJson `json:"descr,omitempty"`
	Items     []itemJson    `json:"items,omitempty"`
}

type userJson struct {
	Login        string           `json:"login,omitempty"`
	Name         string           `json:"name,omitempty"`
	Avatar       string           `json:"avatar,omitempty"`
	Capabilities []capabilityJson `json:"capabilities,omitempty"`
}

type capabilityJson struct {
	Id int     `json:"id,omitempty"`
	S  float64 `json:"s,omitempty"`
	H  float64 `json:"h,omitempty"`
	M  float64 `json:"m,omitempty"`
}

type coordinateJson struct {
	Id int     `json:"id,omitempty"`
	X  float64 `json:"x,omitempty"`
	Y  float64 `json:"y,omitempty"`
}

type attachmentJson struct {
	Id   int    `json:"id,omitempty"`
	Img  string `json:"img,omitempty"`
	Msg  string `json:"msg,omitempty"`
	Date string `json:"date,omitempty"`
}

var state *srv

func token(w http.ResponseWriter, r *http.Request) {
	resp := state.server.NewResponse()
	defer resp.Close()

	if ar := state.server.HandleAccessRequest(resp, r); ar != nil {
		ar.Authorized = false
		switch ar.Type {
		case osin.REFRESH_TOKEN:
		case osin.PASSWORD:
			if ar.Username == "test" || ar.Username == state.user.Login {
				ar.Authorized = true
				state.login = ar.Username
				fmt.Printf("Authorized!")
			}
		}
		state.server.FinishAccessRequest(resp, r, ar)
	}
	if resp.IsError && resp.InternalError != nil {
		fmt.Printf("ERROR: %s\n", resp.InternalError)
	}
	osin.OutputJSON(resp, w, r)
}

func fatal(v interface{}) {
	log.Fatal(v)
	os.Exit(1)
}

func chk(err error) {
	if err != nil {
		fatal(err)
	}
}

func params() (string, *mini.Config) {
	u, err := user.Current()
	chk(err)

	curdir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	chk(err)

	cfg, err := mini.LoadConfiguration(curdir + "/.awmsrc")
	chk(err)

	// postgresql settings
	info := fmt.Sprintf("host=%s port=%s dbname=%s "+
		"sslmode=%s user=%s password=%s "+
		"coordfile=%s",
		cfg.String("host", "127.0.0.1"),
		cfg.String("port", "5432"),
		cfg.String("dbname", u.Username),
		cfg.String("sslmode", "disable"),
		cfg.String("user", u.Username),
		cfg.String("pass", ""),
	)
	return info, cfg
}

func readUserInfo(cfg *mini.Config) {
	filename := cfg.String("userfile", "data/user.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	cnt := strings.Split(string(content), "|")
	if len(cnt) < 3 {
		return
	}
	u := userJson{Login: "john", Name: "John", Avatar: "avatar", Capabilities: state.caps}
	u.Login = cnt[0]
	u.Name = cnt[1]
	u.Avatar = cnt[2]
	state.user = u
}

func readGraph(cfg *mini.Config) {
	filename := cfg.String("graphfile", "data/graph.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	g := &graphJson{
		V:     0,
		E:     0,
		Verts: []edgeJson{},
	}
	count := 0
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " \t")
		if len(data) < 3 {
			continue
		}
		from, _ := strconv.Atoi(data[0])
		to, _ := strconv.Atoi(data[1])
		weight, _ := strconv.ParseFloat(data[2], 64)
		e := edgeJson{
			From:   from,
			To:     to,
			Weight: weight,
		}
		g.Verts = append(g.Verts, e)
		g.V++
		g.E++
		count++
	}
	if count > 0 {
		state.g = g
	}
}

func readCoordinates(cfg *mini.Config) {
	filename := cfg.String("coordfile", "data/coord.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " \t")
		if len(data) < 3 {
			continue
		}
		index, _ := strconv.Atoi(data[0])
		x, _ := strconv.ParseFloat(data[1], 64)
		y, _ := strconv.ParseFloat(data[2], 64)
		coor := coordinateJson{Id: index, X: x, Y: y}
		state.coords = append(state.coords, coor)
	}
}

func readItemAttribute(filename string, item *itemJson) {
	content, err := ioutil.ReadFile(filename)
	chk(err)
	str := strings.Replace(string(content), "\n", "", -1)
	data := strings.Split(str, "|")
	if len(data) < 3 {
		return
	}
	item.Title = data[0]
	item.Descr = data[1]
	item.Img = data[2]
}

func readItems(cfg *mini.Config) {
	filename := cfg.String("itemsfile", "data/items.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " \t")
		if len(data) < 8 {
			continue
		}
		id, _ := strconv.Atoi(data[0])
		s, _ := strconv.ParseFloat(data[1], 64)
		h, _ := strconv.ParseFloat(data[2], 64)
		m, _ := strconv.ParseFloat(data[3], 64)
		qr := data[4]
		ean, _ := strconv.Atoi(data[5])
		cell, _ := strconv.Atoi(data[6])
		desfile := data[7]
		item := itemJson{Id: id,
			S:      s,
			H:      h,
			M:      m,
			Qr:     qr,
			Ean:    ean,
			Img:    "",
			Title:  "",
			Descr:  "",
			CellId: cell,
			Path:   nil}
		if i := strings.LastIndex(filename, "/"); i != -1 {
			desfile = filename[:i+1] + desfile
		}
		readItemAttribute(desfile, &item)
		state.items = append(state.items, item)
	}
}

func readTaskDescription(filename string, t *taskJson) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	cnt := strings.Split(string(content), "|")
	if len(cnt) < 5 {
		return
	}
	t.Descr.PointACoord = cnt[0]
	t.Descr.PointADescr = cnt[1]
	t.Descr.PointBCoord = cnt[2]
	t.Descr.PointBDescr = cnt[3]
	t.Descr.Descr = cnt[4]
}

func readTasks(cfg *mini.Config) {
	filename := cfg.String("taskfile", "data/tasks.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " \t")
		if len(data) < 6 {
			continue
		}
		id, _ := strconv.Atoi(data[0])
		ttype := data[1]
		tstate := data[2]
		tmap := data[3]
		start, _ := strconv.Atoi(data[4])
		task := taskJson{Id: id,
			Type:      ttype,
			State:     tstate,
			Map:       tmap,
			StartNode: start,
			Descr:     taskDescrJson{},
			Items:     []itemJson{},
		}
		desfile := data[5]
		if i := strings.LastIndex(filename, "/"); i != -1 {
			desfile = filename[:i+1] + desfile
		}
		readTaskDescription(desfile, &task)
		for i := 6; i < len(data); i++ {
			iid, _ := strconv.Atoi(data[i])
			item := findItem(iid)
			if item != nil {
				task.Items = append(task.Items, *item)
			}
		}
		state.tasks = append(state.tasks, task)
	}
}

func readCells(cfg *mini.Config) {
	filename := cfg.String("cellfile", "data/cells.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " ")
		for _, c := range data {
			cellid, parseErr := strconv.Atoi(c)
			if parseErr != nil {
				continue
			}
			state.cells = append(state.cells, cellid)
		}
	}
}

func readCapabilities(cfg *mini.Config) {
	filename := cfg.String("capfile", "data/capabilities.txt")
	content, err := ioutil.ReadFile(filename)
	chk(err)
	lines := strings.Split(string(content), "\n")
	for _, d := range lines {
		if len(d) > 0 && d[0] == '#' {
			continue
		}
		data := strings.Split(d, " \t")
		if len(data) < 4 {
			continue
		}
		id, _ := strconv.Atoi(data[0])
		s, _ := strconv.ParseFloat(data[1], 64)
		h, _ := strconv.ParseFloat(data[2], 64)
		m, _ := strconv.ParseFloat(data[3], 64)
		capability := capabilityJson{Id: id, S: s, H: h, M: m}
		state.caps = append(state.caps, capability)
	}
}

func findItem(id int) *itemJson {
	for _, item := range state.items {
		if item.Id == id {
			return &item
		}
	}
	return nil
}

func main() {
	state = &srv{lastCell: 0}
	_, cfg := params()

	readGraph(cfg)
	readCapabilities(cfg)
	readCoordinates(cfg)
	readItems(cfg)
	readTasks(cfg)
	readCells(cfg)
	readUserInfo(cfg)

	state.tstorage = NewTokenStorage(cfg.String("client_id", "1234"),
		cfg.String("secret", "aabbcc"),
		cfg.String("redirect", ""))

	ocfg := osin.NewServerConfig()
	ocfg.AllowedAuthorizeTypes = osin.AllowedAuthorizeType{osin.CODE, osin.TOKEN}
	ocfg.AllowedAccessTypes = osin.AllowedAccessType{osin.REFRESH_TOKEN, osin.PASSWORD}
	ocfg.AllowGetAccessRequest = true
	ocfg.AllowClientSecretInParams = true

	state.server = osin.NewServer(ocfg, state.tstorage)

	router := mux.NewRouter()

	// Authorization part
	router.HandleFunc("/token", token)

	// functions part
	router.HandleFunc("/graph", printGraph).Methods("GET")
	router.HandleFunc("/coordinates", printCoordinates).Methods("GET")
	router.HandleFunc("/tasks", showTasks).Methods("GET")
	router.HandleFunc("/capabilities", showCapabilities).Methods("GET")
	router.HandleFunc("/user", showUser).Methods("GET")
	router.HandleFunc("/img/{item}", pngHandler).Methods("GET")

	router.HandleFunc("/item/add", addItems).Methods("POST")

	router.HandleFunc("/upload", upload).Methods("POST")
	router.HandleFunc("/report_tasks", report).Methods("POST")
	router.HandleFunc("/attachments", attachments).Methods("POST")

	fmt.Println("awms mock server is ready to serve..")
	if cert := cfg.String("cert", ""); len(cert) > 0 {
		key := cfg.String("key", "")
		log.Fatal(http.ListenAndServeTLS(":8000", cert, key, router))
	} else {
		log.Fatal(http.ListenAndServe(":8000", router))
	}
}
