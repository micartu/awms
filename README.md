#Warehouse Management System
Demonstration of a wms system for small warehouses

Works with Linea Pro scanners and thermal printers from Datecs (http://www.datecs.bg/)
It's a client part (iOS), server part is emulated through a mock server (could be found under the directory mock_http - in order to build it just write in bash in that dir: go build and then execute ./mock_http)

In order to test the application you need to build it first. It consists of git submodules and cocoa pods subprojects. So you have to download those:
1. submodules: git submodule init && git submodule update
2. then cocoapods: pod install

after building the app you should be able to login into system with user under login "1" and any not empty password
