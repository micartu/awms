//
//  TruckDeliveryBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 31.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol TruckDeliveryDependency: CommonDependency {
    var scanner: ScannerProtocol { get }
}

final class TruckDeliveryComponent: CommonComponent<TruckDeliveryDependency> {
    let task: TaskTruckDelivery
    init(dependency: TruckDeliveryDependency, task: TaskTruckDelivery) {
        self.task = task
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol TruckDeliveryBuildable: Buildable {
    func build(withListener listener: TruckDeliveryListener, task: TaskTruckDelivery) -> TruckDeliveryRouting
}

final class TruckDeliveryBuilder: Builder<TruckDeliveryDependency>, TruckDeliveryBuildable {

    override init(dependency: TruckDeliveryDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: TruckDeliveryListener, task: TaskTruckDelivery) -> TruckDeliveryRouting {
        let c = TruckDeliveryComponent(dependency: dependency, task: task)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "truckDelivery") as! TruckDeliveryViewController
        let interactor = TruckDeliveryInteractor(presenter: viewController, component: c)
        interactor.listener = listener
        return TruckDeliveryRouter(interactor: interactor, viewController: viewController)
    }
}
