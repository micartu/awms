//
//  TruckDeliveryViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 31.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol TruckDeliveryPresentableListener: CommonPresentableListener {
    func showOnMap()
}

final class TruckDeliveryViewController: CommonVC, TruckDeliveryPresentable, TruckDeliveryViewControllable {
    weak var listener: TruckDeliveryPresentableListener? {
        didSet {
            commonListener = listener
        }
    }

    @IBOutlet weak var btnShowOnMap: CornedButton!
    @IBOutlet weak var labelADescr: UILabel!
    @IBOutlet weak var labelBDescr: UILabel!
    @IBOutlet weak var labelDescr: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Truck Delivery".localized
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        btnShowOnMap.apply(theme: theme)
    }

    // MARK: - TruckDeliveryPresentable

    func show(descriptionA: String, descriptionB: String) {
        labelADescr.text = descriptionA
        labelBDescr.text = descriptionB
    }

    func show(description: String) {
        labelDescr.text = description
    }

    // MARK: - Actions

    @IBAction func showOnMapAction() {
        listener?.showOnMap()
    }
}
