//
//  TruckDeliveryRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 31.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol TruckDeliveryInteractable: Interactable {
    weak var router: TruckDeliveryRouting? { get set }
    weak var listener: TruckDeliveryListener? { get set }
}

protocol TruckDeliveryViewControllable: CommonViewControllable, MenuProtocol {
}

final class TruckDeliveryRouter: CommonRouter<TruckDeliveryInteractable, TruckDeliveryViewControllable>, TruckDeliveryRouting {

    override init(interactor: TruckDeliveryInteractable, viewController: TruckDeliveryViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }

    // MARK: - TruckDeliveryRouting

    func showOnMap(coords: [CGPoint]) -> Bool {
        if let u = prepareURLfrom(coords: coords, type: .app) {
            if UIApplication.shared.canOpenURL(u) {
                UIApplication.shared.open(u)
            } else {
                let inBrowser = prepareURLfrom(coords: coords, type: .browser)!
                UIApplication.shared.open(inBrowser)
            }
            return true
        }
        return false
    }

    // MARK: - Private

    private enum mapType {
        case app
        case browser
    }

    private func prepareURLfrom(coords: [CGPoint], type: mapType) -> URL? {
        if coords.count < 2 {
            return nil
        }
        let a = coords[0]
        let b = coords[1]
        let coord = "?rtext=\(a.x),\(a.y)~\(b.x),\(b.y)&rtt=auto"
        if type == .app {
            return URL(string: "yandexmaps://maps.yandex.ru/" + coord)
        } else {
            return URL(string: "https://yandex.ru/maps" + coord)
        }
    }
}
