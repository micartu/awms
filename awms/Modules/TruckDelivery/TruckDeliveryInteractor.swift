//
//  TruckDeliveryInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 31.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol TruckDeliveryRouting: CommonViewableRouting {
    func showOnMap(coords: [CGPoint]) -> Bool
}

protocol TruckDeliveryPresentable: CommonPresentable {
    weak var listener: TruckDeliveryPresentableListener? { get set }
    func show(descriptionA: String, descriptionB: String)
    func show(description: String)
}

protocol TruckDeliveryListener: CommonChildListener {
}

final class TruckDeliveryInteractor: CommonInteractor<TruckDeliveryPresentable>, TruckDeliveryInteractable, TruckDeliveryPresentableListener {

    weak var router: TruckDeliveryRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: TruckDeliveryListener? {
        didSet {
            commonListener = listener
        }
    }
    let task: TaskTruckDelivery
    let scanner: ScannerProtocol

    init(presenter: TruckDeliveryPresentable, component c: TruckDeliveryComponent) {
        task = c.task
        scanner = c.dependency.scanner
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        scanner.bindTo(delegate: self)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.show(descriptionA: task.pointsDescr[0],
                       descriptionB: task.pointsDescr[1])
        presenter.show(description: task.descr)
    }

    // MARK: - TruckDeliveryPresentableListener

    func showOnMap() {
        if let opened = router?.showOnMap(coords: task.pointsCoord), !opened {
            presenter.show(title: "Error".localized,
                           error: "Cannot show route".localized)
        }
    }

    // MARK: - CommonPresentableListener

    override func moveBackInNavigation() {
        super.moveBackInNavigation()
        scanner.unbind(delegate: self)
    }
}

