//
//  TruckDeliveryInteractor+Scanner.swift
//  awms
//
//  Created by Michael Artuerhof on 02.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension TruckDeliveryInteractor: ScannerDelegate {
    func itemScanned(type: ScanItemTypes, data: String) {
        switch type {
        case .QR:
            let d = data.split(separator: "|")
            if d.count > 1 && d[0] == "task" {
                let id = String(d[1]).toInt
                if id == task.id {
                    listener?.taskAcomplished(id: id)
                    presenter.showYesNO(title: "Task acomplished".localized,
                                        message: "Return back to TaskBoard?".localized,
                                        actionYes: { [unowned self] (action) in
                                            self.router?.navigationMoveBack()
                        }, actionNo: nil)
                }
            }
        default:
            print("unsupported type")
        }
    }
}
