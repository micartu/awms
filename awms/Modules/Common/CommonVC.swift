//
//  CommonVC.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import SVProgressHUD
import STPopup

class CommonVC: UIViewController, AlertProtocol, Themeble, CommonViewControllable, MenuProtocol, CommonMenuActions {
    // MARK: - AlertProtocol

    func show(title: String, error: String, action: ((UIAlertAction) -> Void)?) {
        show(title: title, message: error, action: nil)
    }

    func show(title: String, error: String) {
        show(title: title, error: error, action: nil)
    }

    func show(title: String, message: String, action: ((UIAlertAction) -> Void)?) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized, style: .default, handler: action)
        ac.addAction(okAction)
        checkBusy()
        runOnMainThread {
            self.present(ac, animated: true, completion: nil)
        }
    }

    func showYesNO(title: String,
                   message: String,
                   actionYes: ((UIAlertAction) -> Void)?,
                   actionNo: ((UIAlertAction) -> Void)?) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes".localized, style: .default, handler: actionYes)
        ac.addAction(yesAction)
        let noAction = UIAlertAction(title: "No".localized, style: .default, handler: actionNo)
        ac.addAction(noAction)
        checkBusy()
        runOnMainThread {
            self.present(ac, animated: true, completion: nil)
        }
    }

    func showInputDialog(title: String,
                         message: String,
                         okAction:((String) -> Void)?) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addTextField(configurationHandler: nil)
        let yesAction = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
            if let act = okAction {
                let textfields = ac.textFields!
                act(textfields.first!.text!)
            }
        })
        ac.addAction(yesAction)
        let noAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
        ac.addAction(noAction)
        checkBusy()
        runOnMainThread {
            self.present(ac, animated: true, completion: nil)
        }
    }

    func showBusyIndicator() {
        busy = true
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setOffsetFromCenter(UIOffset(horizontal: 0, vertical: 150))
        runOnMainThread {
            SVProgressHUD.show()
        }
    }

    func hideBusyIndicator() {
        busy = false
        runOnMainThread {
            SVProgressHUD.dismiss()
        }
    }

    // MARK: - Themeble

    func apply(theme: Theme) {
        if let nav = self.navigationController {
            let navbarAttributes = [NSAttributedStringKey.font: theme.topFont,
                                    NSAttributedStringKey.foregroundColor: theme.tintColor]
            runOnMainThread {
                nav.navigationBar.titleTextAttributes = navbarAttributes
                nav.navigationBar.tintColor = theme.tintColor
            }
        }
    }

    func addMenu() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(commonMenuAction))
    }

    @objc func commonMenuAction() {
        commonListener?.menuAction()
    }

    // MARK: - Life cycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        if let listener = commonListener as? CommonCycleEvents {
            listener.viewDidLoad()
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if busy {
            showBusyIndicator()
        }
    }

    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            commonListener?.moveBackInNavigation()
        }
    }

    func navController() -> UINavigationController? {
        return self.navigationController
    }

    func navMoveBack() {
        runOnMainThread { [weak self] in
            self?.navController()?.popViewController(animated: true)
        }
    }

    // MARK: - MenuProtocol

    func showMenu() {
        self.uiviewController.showLeftViewAnimated(self)
    }

    func hideMenu() {
        self.uiviewController.hideLeftViewAnimated(self)
    }

    // MARK: - CommonMenuActions

    func chooserPrinter() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "choosePrinter") as! ChoosePrinterVC
        let popUp = generatePopUp(for: vc)
        runOnMainThread {
            popUp.present(in: self)
        }
    }

    // MARK: - Private
    private func checkBusy() {
        if busy {
            hideBusyIndicator()
        }
    }
    weak var commonListener: CommonPresentableListener?
    private var busy = false
}

func generatePopUp(for vc: UIViewController) -> STPopupController {
    let popUp = STPopupController(rootViewController: vc)
    popUp.style = .formSheet
    popUp.transitionStyle = .fade
    let sz = UIScreen.main.bounds.size
    let xfactor: CGFloat = 0.8
    let yfactor: CGFloat = 0.8
    popUp.containerView.layer.cornerRadius = 8
    vc.contentSizeInPopup = CGSize(width: sz.width * xfactor,
                                   height: sz.height * yfactor)
    vc.landscapeContentSizeInPopup = CGSize(width: sz.height * xfactor,
                                            height: sz.width * yfactor)
    return popUp
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func runOnMainThread(_ closure: @escaping ()->()) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async {
            closure()
        }
    }
}
