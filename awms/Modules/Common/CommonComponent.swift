//
//  CommonComponent.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

open class CommonComponent<DependencyType>: Component<DependencyType>, CommonDependency {
    var storyboard: UIStoryboard {
        let dep = self.dependency as! CommonDependency
        return dep.storyboard
    }

    var network: NetworkProtocol {
        let dep = self.dependency as! CommonDependency
        return dep.network
    }

    var themeManager: ThemeManagerProtocol {
        let dep = self.dependency as! CommonDependency
        return dep.themeManager
    }
}
