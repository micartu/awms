//
//  Protocols.swift
//  awms
//
//  Created by Michael Artuerhof on 24.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import RIBs

protocol MenuProtocol: class {
    func showMenu()
    func hideMenu()
}

protocol CommonPresentableListener: class {
    func menuAction()
    func moveBackInNavigation()
}

protocol CommonChildListener: class {
    func backAction()
    func controllerRemoved()
    func taskAcomplished(id: Int)
}

protocol CommonCycleEvents: class {
    func viewDidLoad()
}

protocol CommonMenuActions: class {
    func chooserPrinter()
}

protocol CommonViewableRouting: ViewableRouting, ThemebleRouting, MenuProtocol {
    func navigationMoveBack()
}

protocol CommonViewControllable: ViewControllable, AlertProtocol, Themeble  {
    func navController() -> UINavigationController?
    func navMoveBack()
}

protocol CommonPresentable: Presentable, AlertProtocol {
}

protocol CommonDependency: Dependency {
    var storyboard: UIStoryboard { get }
    var network: NetworkProtocol { get }
    var themeManager: ThemeManagerProtocol { get }
}
