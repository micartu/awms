//
//  tableView+roundCells.swift
//  awms
//
//  Created by Michael Artuerhof on 03.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension UITableView {
    func applyRoundStyle(to cell: UITableViewCell, with index: IndexPath) {
        let xOffset: CGFloat = 10
        let kRadius: CGFloat = 8
        let top = (index.row == 0)
        let bottom = (index.row == numberOfRows(inSection: index.section) - 1)
        var maskPath: UIBezierPath
        let maskLayer = CAShapeLayer()
        let rc = cell.bounds.insetBy(dx: xOffset, dy: 0)
        if top || bottom {
            var corners = UIRectCorner(rawValue: 0)
            if top {
                corners = corners.union(.topLeft).union(.topRight)
            }
            if bottom {
                corners = corners.union(.bottomLeft).union(.bottomRight)
            }
            maskPath = UIBezierPath(roundedRect: rc,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: kRadius, height: kRadius))
        } else {
            maskPath = UIBezierPath(rect: rc)
        }
        maskLayer.frame = cell.bounds
        maskLayer.path = maskPath.cgPath
        cell.layer.mask = maskLayer
        cell.layer.masksToBounds = true
        cell.separatorInset = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsets.zero
    }
}
