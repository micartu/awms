//
//  CommonRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import RIBs

open class CommonRouter<InteractorType, ViewControllerType>: ViewableRouter<InteractorType, ViewControllerType>, CommonViewableRouting {
    func change(theme: Theme) {
        if let v = viewController as? Themeble {
            v.apply(theme: theme)
        }
    }

    // MARK: - CommonChildRouter

    func navigationMoveBack() {
        (viewController as? CommonViewControllable)?.navController()?.popViewController(animated: true)
    }

    // MARK: - MenuProtocol

    func showMenu() {
        (viewController as! MenuProtocol).showMenu()
    }

    func hideMenu() {
        (viewController as! MenuProtocol).hideMenu()
    }
}
