//
//  CommonInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import RIBs

open class CommonInteractor<PresenterType>: PresentableInteractor<PresenterType>, CommonCycleEvents, CommonPresentableListener, LeftMenuListener {

    /// controls themes of all view controllers
    var themeManager: ThemeManagerProtocol

    /// link to router with common parameters
    /// actually here we should have something like weak var router: RoutingType?
    /// and move RoutingType into the header as generics parameter
    /// but generics in swift didn't allow me it (there were a lot of errors)
    weak var commonRouter: MenuProtocol?
    weak var commonListener: CommonChildListener?

    init(presenter: PresenterType, themeManager: ThemeManagerProtocol) {
        self.themeManager = themeManager
        super.init(presenter: presenter)
    }

    // MARK: - CommonCycleEvents

    func viewDidLoad() {
        if let p = self.presenter as? Themeble {
            p.apply(theme: themeManager.getCurrentTheme())
        }
    }

    // MARK: - CommonPresentableListener

    func menuAction() {
        commonRouter?.showMenu()
    }

    func moveBackInNavigation() {
        commonListener?.controllerRemoved()
    }

    // MARK: - CommonChildListener

    func backAction() {
    }

    func controllerRemoved() {
    }

    func taskAcomplished(id: Int) {
        // DO NOTHING
    }

    // MARK: - LeftMenuListener

    func selectedMenu(_ index: MenuType) {
        if index == .chooserPrinter {
            if let pres = presenter as? CommonMenuActions {
                commonRouter?.hideMenu()
                delay(0.1) {
                    pres.chooserPrinter()
                }
            }
        }
    }
}

