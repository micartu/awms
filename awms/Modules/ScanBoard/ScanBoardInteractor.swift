//
//  ScanBoardInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol ScanBoardRouting: CommonViewableRouting {
    func showDetails(with router: ItemDetailsRouting)
    func removeDetails()
    func showDelivery(with router: DeliveryRouting)
    func removeDelivery()
    func removeChildren()
    func active() -> Bool
}

protocol ScanBoardPresentable: CommonPresentable {
    weak var listener: ScanBoardPresentableListener? { get set }
    func set(title: String)
    func show(items: [ScanItem])
    func updateHeader(with text: String)
    func updateCountInBag(count: Int)
    func updateValueFor(index: Int, value: Int)
    func update(index: Int, with item: ScanItem)
    func hideLoaderButton()
}

protocol ScanBoardListener: CommonChildListener {
}

final class ScanBoardInteractor: CommonInteractor<ScanBoardPresentable>, ScanBoardInteractable,
ScanBoardPresentableListener, DeliveryListener, ItemDetailsListener {
    weak var router: ScanBoardRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: ScanBoardListener? {
        didSet {
            commonListener = listener
        }
    }
    let menu: LeftMenuRouting
    let component: ScanBoardComponent
    let storage: StorageService
    let order: OrdererService
    let lastScan: String

    init(presenter: ScanBoardPresentable, component c: ScanBoardComponent) {
        menu = c.dependency.menu!
        if c.mode == .scan {
            menu.showMenu(menu: [.backMenu, .orderDataAgain])
        } else { // .print
            menu.showMenu(menu: [.backMenu])
        }
        awaited = c.items
        storage = c.dependency.storage
        order = c.dependency.order
        lastScan = c.dependency.lastScan
        component = c
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        presenter.listener = self
        menu.subscribe(self)
        if c.mode == .print {
            presenter.set(title: "Printing Board".localized)
            presenter.hideLoaderButton()
        } else {
            presenter.set(title: "Scanning Board".localized)
        }
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    // MARK: - LeftMenuListener

    override func selectedMenu(_ index: MenuType) {
        super.selectedMenu(index)
        if index == .backMenu {
            router?.hideMenu()
            moveBack()
        } else if index == .orderDataAgain {
            router?.hideMenu()
            var items = [Item]()
            for s in self.shown {
                let i = s.item
                if i.scanned > 0 && i.storedIn < 0 {
                    items.append(i)
                }
            }
            if items.count == 0 {
                presenter.show(title: "Warning".localized,
                               error: "All elements were ordered already".localized)
            } else {
                presenter.showYesNO(title: "Connection to server".localized,
                                    message: "Try to order data on the server again?".localized,
                                    actionYes: { [unowned self] action in
                                        self.order.order(items: items) { items, err in
                                            if let e = err {
                                                self.presenter.show(title: "Error".localized,
                                                                    error: e.localizedDescription)
                                            } else {
                                                self.updateAwaited()
                                                self.gatherItems()
                                            }
                                        }
                    }, actionNo: nil)
            }
        }
    }

    // MARK: - ScanBoardPresentableListener
    func longPress(on cell:Int) {
        let c = shown[cell].item
        let detailsBuilder = ItemDetailsBuilder(dependency: self.component)
        let r = detailsBuilder.build(withListener: self, item: c)
        self.router?.showDetails(with: r)
    }

    func loaderAction() {
        // load Delivery module if there are some items to be delivered only
        if let items = storage.getBagItems() {
            let tasks = storage.getTasksForCurrentUser()!
            for t in tasks {
                switch t {
                case is TaskDelivery:
                    if items.count > 0 {
                        let deliveryb = DeliveryBuilder(dependency: self.component)
                        let r = deliveryb.build(withListener: self, task: t as! TaskDelivery)
                        deliveryId = t.id
                        router?.showDelivery(with: r)
                    }
                    return
                default:
                    continue
                }
            }
        } else {
            presenter.show(title: "No items".localized,
                           error: "Please select and upload to server items to be delivered!".localized)
        }
    }

    func changed(itemIndex: Int, withScanned count: Int) {
        if component.mode == .scan {
            // we can change scanned values manually only in scan mode
            let i = shown[itemIndex].item
            putIntoBag(item: i, with: count) { (changed) in
                if changed {
                    self.shown[itemIndex].scanned = count
                    self.presenter.updateCountInBag(count: self.count)
                } else {
                    self.presenter.updateValueFor(index: itemIndex, value: i.scanned)
                }
            }
        }
    }

    func viewDidAppear() {
        presenter.updateCountInBag(count: count)
        checkIfTaskIsOver()
    }

    func manualEANinput() {
        presenter.showInputDialog(title: "Manual EAN input".localized,
                                  message: "Please enter EAN of item manually".localized,
                                  okAction: { [unowned self] text in
                                    self.itemScanned(type: .EAN13, data: text)
        })
    }

    // MARK: - CommonPresentableListener
    override func viewDidLoad() {
        super.viewDidLoad()
        if component.mode == .scan {
            presenter.updateHeader(with: "Please scan all items\nwhich you need to deliver".localized)
        } else { // .print
            presenter.updateHeader(with: "Please print all items' labels\nthen scan those labels".localized)
        }
        gatherItems()
        // check if user scanned something in TaskBoard and it came to us here:
        let data = lastScan.split(separator: "|")
        if data.count > 1 && data[0] == "task" {
            if String(data[1]).toInt == component.id {
                self.itemScanned(type: .QR, data: lastScan)
            }
        }
    }

    // MARK: - CommonChildListener

    override func backAction() {
        router?.removeChildren()
    }

    override func controllerRemoved() {
        super.controllerRemoved()
        router?.removeChildren()
        updateAwaited()
        gatherItems()
    }

    // MARK: - internal

    internal func updateAwaited() {
        if let task = storage.get(task: component.id) as? TaskScan {
            awaited = task.items
        }
    }

    internal func checkIfTaskIsOver() {
        var acomplished = true
        if let items = awaited {
            if component.mode == .scan {
                for i in items {
                    if i.status != .delivered {
                        acomplished = false
                        break
                    }
                }
            }
            else { // .print
                for i in items {
                    if i.scanned == 0 {
                        acomplished = false
                        break
                    }
                }
            }
        }
        if acomplished {
            listener?.taskAcomplished(id: component.id)
            // finish delivery task because it's connected to ours
            if deliveryId > -1 {
                listener?.taskAcomplished(id: deliveryId)
            }
            presenter.showYesNO(title: "Finish task?".localized,
                                message: "Task is finished, would you like to exit?".localized,
                                actionYes: { [unowned self] action in
                                    self.moveBack()
                }, actionNo: nil)
        }
    }

    internal func gatherItems() {
        if component.mode == .scan {
            gatherScannedItems()
        } else { // .print
            // show all items
            if let awaited = awaited {
                var i2s = [ScanItem]()
                for i in awaited {
                    let s = createPrintItem(item: i)
                    i2s.append(s)
                }
                shown = i2s
                presenter.show(items: i2s)
            }
        }
    }

    internal func gatherScannedItems() {
        if let items = storage.getBagItems() {
            var i2s = [ScanItem]()
            count = 0
            for i in items {
                if i.status == .delivered {
                    storage.deleteFromBox(item: i) { }
                    continue
                }
                let s = createScanItem(item: i)
                i2s.append(s)
                count += i.scanned
            }
            shown = i2s
            presenter.show(items: i2s)
        }
    }

    internal func createItem(item: Item, type: ScanItemType) -> ScanItem {
        let theme = self.themeManager.getCurrentTheme()
        return ScanItem(type: type,
                        theme: theme,
                        item: item,
                        scanned: item.scanned)
    }

    internal func createScanItem(item: Item) -> ScanItem {
        return createItem(item: item, type: .toScan)
    }

    internal func createPrintItem(item: Item) -> ScanItem {
        return createItem(item: item, type: .toPrint)
    }

    internal func putIntoBag(item: Item, with count: Int, result: @escaping ((Bool) -> Void)) {
        let diff = count - item.scanned
        var i = item
        i.scanned = diff
        if storage.canPutIntoBag(item: i) {
            i.scanned = count
            storage.putIntoBag(item: i) { [unowned self] in
                // update count of elements in the bag
                self.count += diff
                self.presenter.updateCountInBag(count: self.count)
                self.storage.update(item: i) {
                    result(true)
                }
            }
        } else {
            // cannot put item into the bag
            presenter.show(title: "Error".localized, error: "Can't put item into the bag".localized)
            result(false)
        }
    }

    internal func moveBack() {
        menu.unsubscribe(self)
        router?.navigationMoveBack()
        listener?.backAction()
        component.scanner.unbind(delegate: self)
    }

    internal var awaited: [Item]?
    internal var shown: [ScanItem]! = nil
    /// count of items in the bag
    internal var count = 0
    internal var deliveryId = -1
}
