//
//  ScanBoardInteractor+Scanner.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension ScanBoardInteractor: ScannerDelegate {
    func itemScanned(type: ScanItemTypes, data: String) {
        if !(router?.active() ?? true) {
            // do not read scanned items if we're not active now
            return
        }
        switch type {
        case .EAN13:
            let ean = data.toInt
            if let i = storage.getItemBy(ean: ean) {
                switch component.mode {
                case .scan:
                    if i.status == .delivered {
                        presenter.show(title: "Error".localized,
                                       error: "Item with EAN: \(i.ean) was already scanned and delivered!")
                        return
                    }
                    tryPutIntoBag(item: i)
                case .print:
                    var cItem = i
                    cItem.scanned = i.scanned + 1
                    update(item: cItem)
                }
            }
        case .QR:
            let d = data.split(separator: "|")
            if d.count > 2 {
                itemScanned(type: .EAN13, data: String(d[2]))
            }
        }
    }

    func tryPutIntoBag(item i: Item) {
        putIntoBag(item: i, with: i.scanned + 1) { [unowned self] (changed) in
            if changed {
                var cItem = i
                cItem.scanned = i.scanned + 1
                self.gatherScannedItems()
                self.order.order(items: [cItem]) { (ordered, err) in
                    if let ordered = ordered {
                        if let io = ordered.first {
                            // save ordered position of the item:
                            cItem.storedIn = io.storedIn
                            // change its status to to be delivered:
                            cItem.status = .toDeliver
                            self.update(item: cItem)
                        }
                    }
                    if let e = err {
                        self.presenter.show(title: "Error".localized,
                                            error: e.localizedDescription)
                    }
                }
            } else {
                self.presenter.show(title: "No storage".localized,
                                    error: "Item cannot be put anywhere".localized)
            }
        }
    }

    func update(item cItem: Item) {
        storage.update(item: cItem) { [unowned self] in
            // update local item
            for ind in self.shown!.indices {
                if self.shown![ind].item.ean == cItem.ean {
                    let i: ScanItem
                    switch self.component.mode {
                    case .scan:
                        i = self.createScanItem(item: cItem)
                    case .print:
                        i = self.createPrintItem(item: cItem)
                    }
                    self.shown![ind] = i
                    self.updateAwaited()
                    self.checkIfTaskIsOver()
                    // update data
                    self.presenter.update(index: ind, with: i)
                }
            }
        }
    }
}
