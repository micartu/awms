//
//  ScanBoardViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol ScanBoardPresentableListener: CommonPresentableListener {
    func longPress(on cell:Int)
    func loaderAction()
    func changed(itemIndex: Int, withScanned count: Int)
    func viewDidAppear()
    func manualEANinput()
}

final class ScanBoardViewController: CommonVC, ScanBoardPresentable, ScanBoardViewControllable {
    weak var listener: ScanBoardPresentableListener? {
        didSet {
            commonListener = listener
        }
    }
    @IBOutlet weak var tableView: UITableView!
    private var items: [ScanItem]? = nil
    private var theme: Theme! = nil
    private var headerText: String = ""
    private let pressDuration = 0.3
    private var showLoaderButton = true
    private let sections: [ScanSectionType] = [.header, .item]

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80

        addMenu()

        let lpr = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        lpr.minimumPressDuration = pressDuration
        tableView.addGestureRecognizer(lpr)

        // add loader icon with badge
        if !showLoaderButton {
            return
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "loader"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(loaderAction))
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        // we need to readraw our rounded cells, so call reload:
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        listener?.viewDidAppear()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        self.theme = theme
        tableView.tintColor = theme.tintColor
    }

    func show(items: [ScanItem]) {
        self.items = items
        tableView.reloadData()
    }

    func set(title: String) {
        self.title = title
    }

    func hideLoaderButton() {
        showLoaderButton = false
    }

    func updateHeader(with text: String) {
        let s = section(for: .header)
        headerText = text

        // is it the first time we update the header?
        if items != nil && s > -1 {
            tableView.beginUpdates()
            tableView.reloadRows(at: [IndexPath(row: 0, section: s)], with: .fade)
            tableView.endUpdates()
        }
    }

    func updateCountInBag(count: Int) {
        runOnMainThread { [weak self] in
            self?.navigationItem.rightBarButtonItem?.updateBadge(number: count)
        }
    }

    func section(for type: ScanSectionType) -> Int {
        for i in sections.indices {
            let s = sections[i]
            if s == type {
                return i
            }
        }
        return -1
    }

    func update(index: Int, with item: ScanItem) {
        let s = section(for: .item)
        items![index] = item
        if s > -1 {
            tableView.beginUpdates()
            tableView.reloadRows(at: [IndexPath(row: index, section: s)], with: .right)
            tableView.endUpdates()
        }
    }

    func updateValueFor(index: Int, value: Int) {
        let s = section(for: .item)
        items![index].scanned = value
        if s > -1 {
            tableView.beginUpdates()
            tableView.reloadRows(at: [IndexPath(row: index, section: s)], with: .left)
            tableView.endUpdates()
        }
    }

    // MARK: - Actions

    @objc func longPressAction(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            let p = gestureRecognizer.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: p) {
                listener?.longPress(on: indexPath.row)
            }
        }
    }

    @objc func loaderAction() {
        listener?.loaderAction()
    }

    @IBAction func manualEANInputAction() {
        listener?.manualEANinput()
    }
}

extension ScanBoardViewController: ScanedItemCellDelegate {
    func valueChanged(_ value: Int, forItem: Int) {
        listener?.changed(itemIndex: forItem, withScanned: value)
    }
}

extension ScanBoardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .header:
            return 1
        case .item:
            if let c = items?.count {
                return c
            }
            return 0
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch sections[indexPath.section] {
        case .header:
            let model = ScanItemHeader(descr: headerText)
            cell = tableView.dequeueReusableCell(withModel: model, for: indexPath)
        case .item:
            let model = items![indexPath.row]
            let c = tableView.dequeueReusableCell(withModel: model, for: indexPath) as! ScanedItemCell
            c.delegate = self
            cell = c
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.applyRoundStyle(to: cell, with: indexPath)
    }
}

extension ScanBoardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
}
