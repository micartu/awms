//
//  ScanBoardBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

enum ScanBoardMode {
    case scan
    case print
}

protocol ScanBoardDependency: CommonDependency {
    var menu: LeftMenuRouting? { get }
    var scanner: ScannerProtocol { get }
    var storage: StorageService { get }
    var saver: FileService { get }
    var order: OrdererService { get }
    var mapLoader: MapService { get }
    var lastScan: String { get }
}

final class ScanBoardComponent: CommonComponent<ScanBoardDependency>, ItemDetailsDependency, DeliveryDependency {
    var items: [Item]?
    var storage: StorageService {
        get {
            return dependency.storage
        }
    }
    var mapLoader: MapService {
        get {
            return dependency.mapLoader
        }
    }
    var saver: FileService {
        get {
            return dependency.saver
        }
    }
    var scanner: ScannerProtocol {
        get {
            return dependency.scanner
        }
    }
    var printer: PrinterService {
        get {
            return ServicesAssembler.shared().printer
        }
    }
    let mode: ScanBoardMode
    let id: Int
    init(dependency: ScanBoardDependency, mode: ScanBoardMode, id: Int,
         awaited items: [Item]?) {
        self.items = items
        self.id = id
        self.mode = mode
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol ScanBoardBuildable: Buildable {
    func build(withListener listener: ScanBoardListener, with task: TaskScan) -> ScanBoardRouting
}

final class ScanBoardBuilder: Builder<ScanBoardDependency>, ScanBoardBuildable {

    override init(dependency: ScanBoardDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: ScanBoardListener, with task: TaskScan) -> ScanBoardRouting {
        var mode: ScanBoardMode
        if task is TaskPrint {
            mode = .print
        } else {
            mode = .scan
        }
        let c = ScanBoardComponent(dependency: dependency, mode: mode, id: task.id, awaited: task.items)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "scanboard") as! ScanBoardViewController
        let interactor = ScanBoardInteractor(presenter: viewController, component: c)
        interactor.listener = listener
        dependency.scanner.bindTo(delegate: interactor)
        return ScanBoardRouter(interactor: interactor, viewController: viewController)
    }
}
