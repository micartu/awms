//
//  ScanBoardRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol ScanBoardInteractable: Interactable {
    weak var router: ScanBoardRouting? { get set }
    weak var listener: ScanBoardListener? { get set }
}

protocol ScanBoardViewControllable: CommonViewControllable, MenuProtocol {
}

final class ScanBoardRouter: CommonRouter<ScanBoardInteractable, ScanBoardViewControllable>, ScanBoardRouting {

    override init(interactor: ScanBoardInteractable, viewController: ScanBoardViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }

    func showDetails(with router: ItemDetailsRouting) {
        detailsRouter = router
        attachChild(router)
        viewController.navController()?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }

    func removeDetails() {
        if let r = detailsRouter {
            detachChild(r)
            detailsRouter = nil
        }
    }

    func showDelivery(with router: DeliveryRouting) {
        deliveryRouter = router
        attachChild(router)
        viewController.navController()?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }

    func removeDelivery() {
        if let r = deliveryRouter {
            detachChild(r)
            deliveryRouter = nil
        }
    }

    func removeChildren() {
        if deliveryRouter != nil {
            removeDelivery()
        }
        if detailsRouter != nil {
            removeDetails()
        }
    }

    func active() -> Bool {
        return detailsRouter == nil && deliveryRouter == nil
    }

    // MARK: - Private

    private var detailsRouter: ItemDetailsRouting? = nil
    private var deliveryRouter: DeliveryRouting? = nil
}
