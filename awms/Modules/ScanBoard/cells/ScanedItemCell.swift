//
//  ScanedItemCell.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

protocol ScanedItemCellDelegate: class {
    func valueChanged(_ value: Int, forItem: Int)
}

class ScanedItemCell: UITableViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var stepper: Stepper!
    weak var delegate: ScanedItemCellDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        stepper.delegate = self
    }

    func hideStepper() {
        stepper.isHidden = true
    }
}

extension ScanedItemCell: StepperDelegate {
    func valueChanged(_ value: Int) {
        let tableview = superview as! UITableView
        let index = tableview.indexPath(for: self)
        delegate?.valueChanged(value, forItem: index!.row)
    }

    var value: Int {
        get {
            return stepper.value
        }
        set {
            stepper.value = newValue
        }
    }
}

extension ScanedItemCell: Themeble {
    func apply(theme: Theme) {
        stepper.apply(theme: theme)
    }
}
