//
//  ScanedHeaderCell.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class ScanedHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
