//
//  ScanItem.swift
//  awms
//
//  Created by Michael Artuerhof on 03.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

enum ScanSectionType {
    case header
    case item
}

protocol ScanItemAnyModel {
    static var CellAnyType: UIView.Type { get }
    func setupAny(cell: UIView)
}

extension ScanItemModel {
    static var CellAnyType: UIView.Type {
        return CellType.self
    }
    func setupAny(cell: UIView) {
        if let c = cell as? CellType {
            setup(cell: c)
        }
    }
}

protocol ScanItemModel: ScanItemAnyModel {
    associatedtype CellType: UIView
    func setup(cell: CellType)
}

struct ScanItemHeader: ScanItemModel {
    let descr: String
    func setup(cell: ScanedHeaderCell) {
        cell.titleLabel?.text = descr
    }
}

enum ScanItemType {
    case toScan
    case toPrint
}

struct ScanItem: ScanItemModel {
    let type: ScanItemType
    let theme: Theme
    let item: Item
    var scanned: Int
    func setup(cell: ScanedItemCell) {
        cell.itemImage.image = item.img!
        cell.titleLabel?.text = item.title
        cell.descrLabel?.text = item.descr + ": \(item.ean)"
        cell.apply(theme: theme)
        cell.value = scanned
        switch type {
        case .toScan:
            if item.storedIn > -1 {
                cell.backgroundColor = theme.cellScannedOk
            } else {
                cell.backgroundColor = theme.cellScannedError
            }
        case .toPrint:
            cell.hideStepper()
            if scanned > 0 {
                cell.backgroundColor = theme.cellScannedOk
            } else {
                cell.backgroundColor = UIColor.white
            }
        }
    }
}

extension UITableView {
    func dequeueReusableCell(withModel model: ScanItemAnyModel, for indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: type(of: model).CellAnyType)
        let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        model.setupAny(cell: cell)
        return cell
    }
}


