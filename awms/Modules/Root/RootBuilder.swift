//
//  RootBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol RootDependency: CommonDependency {
}

final class RootComponent: CommonComponent<RootDependency>, LoginDependency, TaskBoardDependency {
    var root: RootRouting?
    var scanner: ScannerProtocol
    var saver: FileService
    var cleaner: CleanerService
    var taskLoader: TaskService
    var imgLoader: ImageLoaderService
    var mapLoader: MapService
    var storage: StorageService
    var uploader: UploaderService
    var login: String

    override init(dependency: RootDependency) {
        self.login = ""
        self.scanner = ServicesAssembler.shared().scanner
        self.storage = ServicesAssembler.shared().cache
        self.saver = ServicesAssembler.shared().filer
        self.cleaner = ServicesAssembler.shared().cleaner
        self.uploader = ServicesAssembler.shared().uploader
        self.imgLoader = ServicesAssembler.shared().imgLoader
        self.mapLoader = ServicesAssembler.shared().mapLoader
        self.taskLoader = TaskLoader(network: dependency.network,
                                     cache: self.storage,
                                     imgLoader: self.imgLoader,
                                     mapLoader: self.mapLoader,
                                     saver: self.saver)
        self.root = nil // it must be set afterwards
        super.init(dependency: dependency)
    }

    func changeLogin(name: String) {
        login = name
        storage.reload(for: name)
        taskLoader.change(cache: storage)
        taskLoader.changeLogin(name: name)
    }
}

// MARK: - Builder

protocol RootBuildable: Buildable {
    func build() -> LaunchRouting
}

final class RootBuilder: Builder<RootDependency>, RootBuildable {

    override init(dependency: RootDependency) {
        super.init(dependency: dependency)
    }

    func build() -> LaunchRouting {
        let c = RootComponent(dependency: dependency)
        let viewController = RootViewController()
        let interactor = RootInteractor(presenter: viewController, component: c)
        let leftMenuBuilder = LeftMenuBuilder(dependency: dependency)
        let loginBuild = LoginBuilder(dependency: c)
        let root = RootRouter(interactor: interactor,
                              viewController: viewController,
                              menuBuilder: leftMenuBuilder,
                              loginBuilder: loginBuild)
        c.root = root
        return root
    }
}
