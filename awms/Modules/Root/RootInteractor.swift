//
//  RootInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol RootRouting: ViewableRouting, MenuProtocol {
    func loadTaskBoard(router: TaskBoardRouting)
    func logout()
    func menuRouter() -> LeftMenuRouting
    func showAbout()
}

protocol RootPresentable: Presentable {
    weak var listener: RootPresentableListener? { get set }
}

protocol RootListener: class {
}

final class RootInteractor: CommonInteractor<RootPresentable>,
    RootInteractable, RootPresentableListener,
LoginListener, TaskBoardListener {

    weak var router: RootRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: RootListener?
    let component: RootComponent
    var storyboard: UIStoryboard {
        get {
            return component.dependency.storyboard
        }
    }

    init(presenter: RootPresentable, component: RootComponent) {
        self.component = component
        super.init(presenter: presenter, themeManager: component.dependency.themeManager)
        presenter.listener = self
    }

    // MARK: - RootInteractable

    // MARK: - LeftMenuListener

    override func selectedMenu(_ index: MenuType) {
        super.selectedMenu(index)
        switch index {
        case .aboutMenu:
            router?.showAbout()
        case .logoutMenu:
            router?.logout()
        default:
            break // I'll add it later
        }
    }

    // MARK: - LoginListener

    func successfulLogin(_ name: String) {
        component.changeLogin(name: name)
        let boardBuilder = TaskBoardBuilder(dependency: component)
        let router = boardBuilder.build(withListener: self)
        self.router?.loadTaskBoard(router: router)
    }

    // MARK: - CommonPresentableListener

    override func menuAction() {
        router?.showMenu()
    }

    override func moveBackInNavigation() {
        // DO NOTHING
    }

    // MARK: - RootInteractable
}
