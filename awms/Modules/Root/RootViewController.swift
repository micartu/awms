//
//  RootViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import LGSideMenuController

protocol RootPresentableListener: class {
}

final class RootViewController: LGSideMenuController, RootPresentable, RootViewControllable, CommonMenuActions {

    weak var listener: RootPresentableListener?

    // MARK: - RootViewControllable

    func setLeftViewController(_ vc: UIViewController) {
        self.leftViewController = vc
    }

    func setRootViewController(_ vc: UIViewController) {
        self.rootViewController = vc
    }

    func showLeftMenu() {
        showLeftViewAnimated()
    }

    func hideLeftMenu() {
        hideLeftViewAnimated()
    }

    func initializeMenu() {
        self.leftViewWidth = 290.0
        self.leftViewPresentationStyle = .slideBelow
    }

    func chooserPrinter() {
        if let nav = self.rootViewController as? UINavigationController {
            if let root = nav.topViewController as? CommonMenuActions {
                root.chooserPrinter()
            }
        }
    }
}
