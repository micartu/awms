//
//  RootRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

enum RootState {
    case StateLogin
    case StateTaskBoard
}

protocol RootInteractable: Interactable, LeftMenuListener, LoginListener  {
    weak var router: RootRouting? { get set }
    weak var listener: RootListener? { get set }
    var storyboard: UIStoryboard { get }
}

protocol RootViewControllable: ViewControllable {
    func setLeftViewController(_ vc: UIViewController)
    func setRootViewController(_ vc: UIViewController)
    func showLeftMenu()
    func hideLeftMenu()
    func initializeMenu()
}

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {

    init(interactor: RootInteractable,
                  viewController: RootViewControllable,
                  menuBuilder: LeftMenuBuildable,
                  loginBuilder: LoginBuildable) {
        let leftMenuRoute = menuBuilder.build(withListener: interactor)
        let login = loginBuilder.build(withListener: interactor)

        // save pointers for ARC (or some objects would be deallocated)
        self.leftMenuRoute = leftMenuRoute
        loginRouter = login
        let navigationcnt = UINavigationController(rootViewController: login.viewControllable.uiviewController)

        viewController.setRootViewController(navigationcnt)
        viewController.setLeftViewController(leftMenuRoute.viewControllable.uiviewController)

        super.init(interactor: interactor,
                   viewController: viewController)
        interactor.router = self

        nav = navigationcnt

        viewController.initializeMenu()

        attachChild(login)
        attachChild(leftMenuRoute)

        menuForLogin()
    }

    // MARK: - RootRouting

    func showMenu() {
        viewController.showLeftMenu()
    }

    func hideMenu() {
        viewController.hideLeftMenu()
    }

    func loadTaskBoard(router: TaskBoardRouting) {
        boardRouter = router
        nav?.show(router.viewControllable.uiviewController, sender: self)
        attachChild(router)
    }

    func logout() {
        viewController.hideLeftMenu()
        detachChild(self.boardRouter!)
        nav?.popViewController(animated: true)
        boardRouter = nil
        menuForLogin()
    }

    func showAbout() {
        let vc = interactor.storyboard.instantiateViewController(withIdentifier: "about")
        leftMenuRoute?.resetSelectedMenu()
        nav?.present(vc, animated: true)
        hideMenu()
    }

    func menuRouter() -> LeftMenuRouting {
        return self.leftMenuRoute!
    }

    // MARK: - Private

    func menuForLogin() {
        leftMenuRoute?.showMenu(menu: [.chooserPrinter,
                                       .aboutMenu])
    }

    private let leftMenuRoute: LeftMenuRouting?
    private let loginRouter: LoginRouting?
    private var boardRouter: TaskBoardRouting?
    private var nav: UINavigationController?
}
