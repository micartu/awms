//
//  LeftMenuInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol LeftMenuRouting: ViewableRouting {
    func showMenu(menu: [MenuType])
    func subscribe(_ me: LeftMenuListener)
    func unsubscribe(_ me: LeftMenuListener)
    func resetSelectedMenu()
}

protocol LeftMenuPresentable: Presentable {
    weak var listener: LeftMenuPresentableListener? { get set }
}

protocol LeftMenuListener: class {
    func selectedMenu(_ index: MenuType)
}

final class LeftMenuInteractor: PresentableInteractor<LeftMenuPresentable>, LeftMenuInteractable, LeftMenuPresentableListener {


    weak var router: LeftMenuRouting?
    weak var listener: LeftMenuListener?

    override init(presenter: LeftMenuPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    // MARK: - LeftMenuInteractable

    func menuModel(_ menu: [MenuType]) {
        self.model = menu
    }

    func subscribe(_ me: LeftMenuListener) {
        subscribers.append(me)
    }

    func unsubscribe(_ me: LeftMenuListener) {
        subscribers.removeElementByReference(me)
    }

    // MARK: - LeftMenuPresentableListener

    func menu(selected: Int) {
        if let menu = model?[selected] {
            if subscribers.count == 0 {
                // root is in charge - translate commands to him
                listener?.selectedMenu(menu)
            } else {
                // translate event to all subscribers
                for s in subscribers {
                    s.selectedMenu(menu)
                }
            }
            return
        }
        fatalError("selected menu's index is out of range")
    }

    // MARK: - Private

    private var model: [MenuType]?
    private var subscribers = [LeftMenuListener]()
}
