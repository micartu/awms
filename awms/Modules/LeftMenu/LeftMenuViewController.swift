//
//  LeftMenuViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol LeftMenuPresentableListener: class {
    func menu(selected: Int)
}

final class LeftMenuViewController: CommonVC, LeftMenuPresentable, LeftMenuViewControllable {

    weak var listener: LeftMenuPresentableListener?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var appTitleLabel: UILabel!
    var menus: [String]?

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
    }

    // MARK: - CommonVC

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        appTitleLabel.font = theme.topFont
    }

    // MARK: - LeftMenuViewControllable

    func prepareMenu(menu:[String]) {
        menus = menu
        tableView.reloadData()
    }

    func removeSelection() {
        if let c = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: c, animated: false)
        }
    }
}

extension LeftMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cnt = menus?.count {
            return cnt
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell")!
        cell.textLabel?.text = menus![indexPath.row]
        return cell
    }
}

extension LeftMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        listener?.menu(selected: indexPath.row)
    }
}
