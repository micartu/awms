//
//  LeftMenuRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

enum MenuType {
    case aboutMenu
    case logoutMenu
    case backMenu
    case chooserPrinter
    case orderDataAgain
}

protocol LeftMenuInteractable: Interactable {
    weak var router: LeftMenuRouting? { get set }
    weak var listener: LeftMenuListener? { get set }
    func menuModel(_ menu: [MenuType])
    func subscribe(_ subscriber: LeftMenuListener)
    func unsubscribe(_ subscriber: LeftMenuListener)
}

protocol LeftMenuViewControllable: ViewControllable {
    func prepareMenu(menu:[String])
    func removeSelection()
}

final class LeftMenuRouter: ViewableRouter<LeftMenuInteractable, LeftMenuViewControllable>, LeftMenuRouting {

    public var viewcontroller: LeftMenuViewControllable

    override init(interactor: LeftMenuInteractable, viewController: LeftMenuViewControllable) {
        self.viewcontroller = viewController
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }

    func showMenu(menu: [MenuType]) {
        var menus = [String]()
        for m in menu {
            switch m {
            case .aboutMenu:
                menus.append("About".localized)
            case .logoutMenu:
                menus.append("Logout".localized)
            case .backMenu:
                menus.append("Back".localized)
            case .chooserPrinter:
                menus.append("Change Printer".localized)
            case .orderDataAgain:
                menus.append("Order Data".localized)
            }
        }
        self.interactor.menuModel(menu)
        self.viewcontroller.prepareMenu(menu: menus)
    }

    func subscribe(_ me: LeftMenuListener) {
        interactor.subscribe(me)
    }

    func unsubscribe(_ me: LeftMenuListener) {
        interactor.unsubscribe(me)
    }

    func resetSelectedMenu() {
        viewcontroller.removeSelection()
    }
}
