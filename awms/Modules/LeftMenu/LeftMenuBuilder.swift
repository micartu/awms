//
//  LeftMenuBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

final class LeftMenuComponent: Component<RootDependency> {
}

// MARK: - Builder

protocol LeftMenuBuildable: Buildable {
    func build(withListener listener: LeftMenuListener) -> LeftMenuRouting
}

final class LeftMenuBuilder: Builder<RootDependency>, LeftMenuBuildable {

    override init(dependency: RootDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LeftMenuListener) -> LeftMenuRouting {
        let _ = LeftMenuComponent(dependency: dependency)
        let storyboard = dependency.storyboard
        let viewController = storyboard.instantiateViewController(withIdentifier: "leftMenu") as! LeftMenuViewController
        let interactor = LeftMenuInteractor(presenter: viewController)
        interactor.listener = listener
        return LeftMenuRouter(interactor: interactor, viewController: viewController)
    }
}
