//
//  DeliveryBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol DeliveryDependency: CommonDependency {
    var scanner: ScannerProtocol { get }
    var saver: FileService { get }
    var mapLoader: MapService { get }
    var storage: StorageService { get }
}

final class DeliveryComponent: CommonComponent<DeliveryDependency> {
    var map: String
    var startPoint: Int
    var id: Int

    init(dependency: DeliveryDependency, map: String, id: Int, startPoint: Int) {
        self.map = map
        self.startPoint = startPoint
        self.id = id
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol DeliveryBuildable: Buildable {
    func build(withListener listener: DeliveryListener, task: TaskDelivery) -> DeliveryRouting
}

final class DeliveryBuilder: Builder<DeliveryDependency>, DeliveryBuildable {

    override init(dependency: DeliveryDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: DeliveryListener, task: TaskDelivery) -> DeliveryRouting {
        let c = DeliveryComponent(dependency: dependency,
                                  map: task.map,
                                  id: task.id,
                                  startPoint: task.startNode)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "delivery") as! DeliveryViewController
        let interactor = DeliveryInteractor(presenter: viewController, component: c)
        let storageView = StorageMapView(map: task.map, saver: dependency.saver)
        interactor.listener = listener
        viewController.construct(with: storageView)
        return DeliveryRouter(interactor: interactor, viewController: viewController)
    }
}
