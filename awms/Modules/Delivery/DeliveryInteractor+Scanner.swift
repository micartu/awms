//
//  DeliveryInteractor+Scanner.swift
//  awms
//
//  Created by Michael Artuerhof on 08.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import Repeat

extension DeliveryInteractor: ScannerDelegate {
    func itemScanned(type: ScanItemTypes, data: String) {
        let d = data.split(separator: "|")
        switch type {
        case .EAN13:
            if nodeScanned > -1 && d.count > 0 {
                let ean = String(d[0]).toInt
                if !scannedItem(withEAN: ean, forNode: nodeScanned) {
                    let str = String.localizedStringWithFormat("Node with id: %d doesn't contain the item with EAN: %d".localized, nodeScanned, ean)
                    presenter.show(help: str)
                } else {
                    presenter.show(help: "Scan next item and its node".localized)
                }
                nodeScanned = -1
            }
        case .QR:
            if d.count > 1 {
                if d[0] == "node" {
                    nodeScanned = String(d[1]).toInt
                    let str = String.localizedStringWithFormat("Scanned node: %d, now scan the item you want to deliver".localized, nodeScanned)
                    presenter.show(help: str)
                    Repeater.once(after: .seconds(30), { [unowned self] _ in
                        self.nodeScanned = -1
                    })
                }
                if d[0] == "task" && d.count > 2 {
                    itemScanned(type: .EAN13, data: String(d[2]))
                }
            }
        }
    }
}
