//
//  DeliveryRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol DeliveryInteractable: Interactable {
    weak var router: DeliveryRouting? { get set }
    weak var listener: DeliveryListener? { get set }
}

protocol DeliveryViewControllable: CommonViewControllable {
}

final class DeliveryRouter: CommonRouter<DeliveryInteractable, DeliveryViewControllable>, DeliveryRouting {

    override init(interactor: DeliveryInteractable, viewController: DeliveryViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
