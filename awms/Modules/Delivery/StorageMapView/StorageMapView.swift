//
//  StorageMapView.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import PocketSVG

class StorageMapView: UIScrollView {
    var theme: Theme? {
        didSet {
            if let mc = self.mc {
                mc.theme = theme
            }
        }
    }
    weak var canvasDelegate: MapCanvasDelegate? = nil

    init(map name: String, saver: FileService, frame: CGRect = CGRect.zero) {
        super.init(frame: frame)
        let contents = saver.contentsOfMap(name)
        let storageView = SVGImageView.init(svgSource: contents)
        let sz = storageView.intrinsicContentSize
        translatesAutoresizingMaskIntoConstraints = false
        add(view: storageView, size: sz)
        contentSize = sz
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func removeMapElements() {
        if let m = self.mc {
            m.removeFromSuperview()
            m.destroy()
            self.mc = nil
        }
    }

    func addMapElements(_ elements: [MapElement]) {
        removeMapElements()
        let sz = contentSize
        let frame = CGRect(x: 0, y: 0,
                           width: sz.width, height: sz.height)
        let mc = MapCanvas(frame: frame)
        mc.delegate = canvasDelegate
        add(view: mc, size: sz)
        mc.items = elements
        self.mc = mc
    }

    // MARK: - Private

    private func add(view: UIView, size: CGSize) {
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        view.heightAnchor.constraint(equalToConstant: size.height).isActive = true
    }

    private var mc: MapCanvas? = nil

    deinit {
        print("deinit of StorageMapView")
        removeMapElements()
    }
}
