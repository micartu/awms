//
//  MapPoint.swift
//  awms
//
//  Created by michael on 22.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import Macaw

class MapPoint: Group {
    var animation: Animation? = nil
    init(point: MapElementPoint, theme: Theme, canvas: MapCanvas) {
        let p = point.point
        let current = (point.type == .currentPoint)
        let circle = Circle(r: 10)
        let color = current ? theme.currentMapTipColor : theme.additionalMapTipColor
        let s = Shape(form: circle,
                      fill: color.toMacaw(),
                      place: Transform.move(dx: Double(p.x), dy: Double(p.y)))
        super.init(contents: [s])
        /*
         // TODO: I cannot release animation here for current element,
         // that's why it was commented
        if current {
            let a = contentsVar.animation({ t in
                let color = theme.animatedMapTipColor.toMacaw().with(a: 1 - t)
                let floating = Circle(r: 10 + t * 10).stroke(fill: color, width: 3)
                floating.place = Transform.move(dx: Double(p.x), dy: Double(p.y))
                return [s, floating]
            }, during: 2)
            //a.easing(.easeInOut).autoreversed().cycle().play()
            animation = a
        } else {
            animation = s.strokeVar.animation(to: Stroke(fill: theme.animatedMapTipColor.toMacaw(),
                                                         width: 6.0,  dashes: [15, 5]))
        }
         */
        animation = s.strokeVar.animation(to: Stroke(fill: theme.animatedMapTipColor.toMacaw(),
                                                     width: 6.0,  dashes: [15, 5]))
        if current {
            animation?.autoreversed().cycle().play()
        }
        self.onTap { [unowned self] event in
            canvas.tapped(point: point)
            if self.animation?.state() != .running {
                canvas.removeAnimations()
                self.animation?.autoreversed().cycle().play()
            }
        }
    }
    deinit {
        animation = nil
    }
}
