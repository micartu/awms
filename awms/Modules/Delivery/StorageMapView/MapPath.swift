//
//  MapPath.swift
//  awms
//
//  Created by michael on 22.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import Macaw

class MapPath: Group {
    init?(path e: MapElementPath, theme: Theme, canvas: MapCanvas) {
        if e.path.count < 2 { return nil }
        var cur = e.path.first!
        var lines = [Shape]()
        for index in 1..<e.path.count {
            let next = e.path[index]
            let stroke = Stroke(fill: theme.currentMapTipColor.toMacaw(),
                                width: 4,
                                cap: .round,
                                join: .round)
            let l = Shape(form: Line(x1: Double(cur.x), y1: Double(cur.y),
                                     x2: Double(next.x), y2: Double(next.y)),
                          fill: theme.currentMapTipColor.toMacaw(),
                          stroke: stroke)
            lines.append(l)
            cur = next
        }
        super.init(contents: lines)
    }
}
