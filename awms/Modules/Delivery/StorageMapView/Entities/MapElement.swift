//
//  MapElement.swift
//  awms
//
//  Created by michael on 15.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

enum MapElementType {
    case currentPoint
    case point
    case path
}

class MapElement {
    let id: String
    let type: MapElementType
    init(id: String, type: MapElementType) {
        self.id = id
        self.type = type
    }
}

class MapElementPoint: MapElement {
    let point: CGPoint

    init(id: String, point: CGPoint, type: MapElementType) {
        self.point = point
        super.init(id: id, type: type)
    }

    convenience init(id: String, point: CGPoint) {
        self.init(id: id, point: point, type: .point)
    }
}

class MapElementPath: MapElement {
    let path: [CGPoint]
    init(id: String, path: [CGPoint]) {
        self.path = path
        super.init(id: id, type: .path)
    }
}
