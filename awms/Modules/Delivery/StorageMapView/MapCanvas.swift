//
//  MapCanvas.swift
//  awms
//
//  Created by michael on 15.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import Macaw

protocol MapCanvasDelegate: class {
    func tapped(point: Int)
}

class MapCanvas: MacawView {
    weak var delegate: MapCanvasDelegate? = nil
    var scene: MapScene?
    var points = [MapPoint]()
    var items: [MapElement] = [] {
        didSet {
            updateNode()
        }
    }
    var theme: Theme? {
        didSet {
            if theme != nil {
                updateNode()
            }
        }
    }

    func updateNode() {
        points.removeAll()
        if let t = self.theme {
            let scene = MapScene(items: items, theme: t, canvas: self)
            self.scene = scene
            self.node = scene.node
            self.backgroundColor = UIColor.clear
        }
    }

    func addLinkTo(point: MapPoint) {
        points.append(point)
    }

    func removeAnimations() {
        for p in points {
            if p.animation?.state() == .running {
                p.animation?.stop()
            }
        }
    }

    func tapped(point: MapElementPoint) {
        let id = point.id.toInt
        delegate?.tapped(point: id)
    }

    func destroy() {
        removeAnimations()
        points.removeAll()
        scene = nil
    }

    deinit {
        destroy()
    }
}

class MapScene {
    let node: Group

    init(items: [MapElement], theme: Theme, canvas: MapCanvas) {
        var nodes = [Group]()
        for i in items {
            switch i {
            case is MapElementPoint:
                let pt = MapPoint(point: i as! MapElementPoint,
                                  theme: theme,
                                  canvas: canvas)
                canvas.addLinkTo(point: pt)
                nodes.append(pt)
            case is MapElementPath:
                if let path = MapPath(path: i as! MapElementPath,
                                      theme: theme,
                                      canvas: canvas) {
                    nodes.append(path)
                }
            default:
                print("Element's type: \(i.type) isn't supported yet")
            }
        }
        node = nodes.group()
    }
}
