//
//  DeliveryInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol DeliveryRouting: CommonViewableRouting {
}

enum DeliverState {
    case error
    case finished
}

protocol DeliveryPresentable: CommonPresentable {
    weak var listener: DeliveryPresentableListener? { get set }
    func show(canvas: [MapElement])
    func show(state: DeliverState)
    func moveTo(point: CGPoint)
    func show(help: String)
    func removePopUp()
}

protocol DeliveryListener: CommonChildListener {
}

enum DeliveryInteractorStatus: Int {
    case initData = 0
    case initDataViewAvailable
    case initDataFinished
    case calcDistances
    case idle
    case error
}

final class DeliveryInteractor: CommonInteractor<DeliveryPresentable>, DeliveryInteractable,
DeliveryPresentableListener {

    weak var router: DeliveryRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: DeliveryListener? {
        didSet {
            commonListener = listener
        }
    }
    internal var nodeScanned = -1
    let component: DeliveryComponent
    let mapLoader: MapService
    var storage: StorageService
    var state: DeliveryInteractorStatus {
        get {
            lock.lock(); defer { lock.unlock() }
            return _state
        }
        set {
            lock.lock()
            _state = newValue
            lock.unlock()
            DTraceDeliveryStateChanged(to: newValue)
        }
    }

    init(presenter: DeliveryPresentable, component c: DeliveryComponent) {
        self.mapLoader = c.dependency.mapLoader
        self.storage = c.dependency.storage
        self.curNode = c.startPoint
        component = c
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        self.component.dependency.scanner.bindTo(delegate: self)
        presenter.listener = self
        self.items = storage.getBagItems()
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    func checkIfTaskIsOver() {
        var over = true
        for i in self.items {
            if i.status != .delivered {
                over = false
                break
            }
        }
        if over {
            presenter.removePopUp()
            listener?.taskAcomplished(id: component.id)
            presenter.showYesNO(title: "Finish task?".localized,
                                message: "Task is finished, would you like to exit?".localized,
                                actionYes: { [unowned self] action in
                                    self.router?.navigationMoveBack()
                }, actionNo: nil)
        }
    }

    // MARK: - CommonPresentableListener

    override func moveBackInNavigation() {
        super.moveBackInNavigation()
        component.dependency.scanner.unbind(delegate: self)
    }

    // MARK: - DeliveryPresentableListener

    func viewWillAppear() {
        if state != .idle { // first time load
            getInitialData()
        }
        checkIfTaskIsOver()
    }

    func scannedItem(withEAN ean:Int, forNode node: Int) -> Bool {
        for i in self.items {
            if i.ean == ean && i.storedIn == node {
                var item = i
                item.status = .delivered
                storage.deleteFromBox(item: item) { [unowned self] in
                    self.storage.update(item: item) {
                        self.state = .calcDistances
                        self.pathCalc.calculateDistancesFrom(startPoint: node) {
                            self.items = self.storage.getBagItems()
                            self.findMinimumDistance()
                            self.checkIfTaskIsOver()
                        }
                    }
                }
                return true
            }
        }
        return false
    }

    // MARK: - MapCanvasDelegate

    func tapped(point: Int) {
        var selected = [Item]()
        for i in self.items {
            if i.storedIn == point {
                selected.append(i)
            }
        }
        if selected.count > 0 {
            var msg = "Item(s) to be delivered: ".localized
            for i in selected.indices {
                let desc = selected[i].title + ": \(selected[i].ean)"
                if i != selected.count - 1 {
                    msg += desc + ","
                } else {
                    msg += desc
                }
            }
            if msg.count > 0 {
                presenter.show(help: msg)
            }
        }
    }

    // MARK: - Private
    private func getInitialData() {
        let loader = DispatchGroup()
        presenter.showBusyIndicator()
        loader.enter()
        mapLoader.get(graph: component.map) { [unowned self] (graph, err) in
            if let g = graph {
                self.graph = g
                self.pathCalc = Dijkstra(graph: g)
                self.pathCalc.calculateDistancesFrom(startPoint: self.curNode) {
                    loader.leave()
                }
            } else {
                loader.leave()
                if let e  = err {
                    self.presenter.show(title: "Error".localized,
                                        error: e.localizedDescription)
                } else {
                    self.presenter.show(title: "Error".localized,
                                        error: "Cannot load graph structure for map".localized)
                }
                self.state = .error
            }
        }
        loader.enter()
        mapLoader.getMapCoordinatesOf(graph: component.map) { [unowned self] (items, err) in
            if let elms = items {
                self.mapNodes = elms
            } else {
                if let e  = err {
                    self.presenter.show(title: "Error".localized,
                                        error: e.localizedDescription)
                } else {
                    self.presenter.show(title: "Error".localized,
                                        error: "Cannot load graph nodes structure".localized)
                }
                self.state = .error
            }
            loader.leave()
        }
        loader.notify(queue: .main) { [unowned self] in
            if self.state != .error {
                self.state = .calcDistances
                self.findMinimumDistance()
            }
        }
    }

    private func findMinimumDistance() {
        presenter.showBusyIndicator()
        if let items = self.items {
            var minDistance = Double(MAXFLOAT)
            var prioItem: Item? = nil
            var path: [Edge]? = nil
            for i in items {
                if i.status == .toDeliver {
                    let node = i.storedIn
                    if node < 0 { continue }
                    let dist = pathCalc.costTo(finalDestination: node)
                    if dist < minDistance {
                        minDistance = dist
                        prioItem = i
                        path = pathCalc.pathTo(finalDestination: node)
                    }
                }
            }
            if let p = path {
                var elements = [MapElement]()
                var lastEdge: Edge? = nil
                var points = [CGPoint]()
                for e in p {
                    guard let from = find(node: e.From()) else { continue }
                    guard let to = find(node: e.To()) else { continue }
                    if lastEdge == nil { // first point
                        let p1 = CGPoint(x: to.x, y: to.y)
                        let p2 = CGPoint(x: from.x, y: from.y)
                        points.append(p1)
                        points.append(p2)
                    } else {
                        let le = lastEdge!
                        let toP: CGPoint?
                        if le.To() == e.From() || le.From() == e.From() {
                            toP = CGPoint(x: to.x, y: to.y)
                        } else if le.From() == e.To() || le.To() == e.To() {
                            toP = CGPoint(x: from.x, y: from.y)
                        } else {
                            toP = nil
                            // something went wrong...
                            break
                        }
                        if toP != nil {
                            points.append(toP!)
                        }
                    }
                    lastEdge = e
                }
                if points.count > 1 {
                    let curPath = MapElementPath(id: "path", path: points)
                    elements.append(curPath)
                }
                for i in items {
                    if i.storedIn < 0 || i.status != .toDeliver { continue }
                    guard let node = find(node: i.storedIn) else { continue }
                    if i.ean == prioItem!.ean {
                        let pt = CGPoint(x: node.x, y: node.y)
                        let element = MapElementPoint(id: "\(node.Id)",
                            point: pt,
                            type: .currentPoint)
                        elements.append(element)
                        presenter.moveTo(point: pt)
                    } else {
                        let element = MapElementPoint(id: "\(node.Id)",
                            point: CGPoint(x: node.x, y: node.y))
                        elements.append(element)
                    }
                }
                if elements.count > 0 {
                    presenter.show(canvas: elements)
                } else {
                    presenter.show(state: .finished)
                }
            } else {
                presenter.show(state: .finished)
            }
        } else {
            presenter.show(state: .finished)
        }
        presenter.hideBusyIndicator()
        state = .idle
    }

    private func find(node: Int) -> MapNode? {
        if let nodes = mapNodes {
            for n in nodes {
                if n.Id == node {
                    return n
                }
            }
        }
        return nil
    }

    private var items: [Item]!
    private var pathCalc: ShortPathService!
    private var curNode: Int = 0
    private var mapNodes: [MapNode]!
    private var graph: Graph!
    private var _state: DeliveryInteractorStatus = .initData
    private let lock = NSLock()
}
