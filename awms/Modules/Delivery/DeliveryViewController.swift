//
//  DeliveryViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import STPopup

protocol DeliveryPresentableListener: CommonPresentableListener, CommonCycleEvents, MapCanvasDelegate {
    func viewWillAppear()
}

final class DeliveryViewController: CommonVC, DeliveryPresentable, DeliveryViewControllable {

    weak var listener: DeliveryPresentableListener? {
        didSet {
            commonListener = listener
        }
    }

    // MARK: - Life Cycle Events

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Delivery".localized
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listener?.viewWillAppear()
    }

    // MARK: - helping methods for builder

    func construct(with storageView: StorageMapView) {
        storageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(storageView)
        let vs = ["sv": storageView]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[sv]-0-|",
                                                                options: .directionLeftToRight,
                                                                metrics: nil,
                                                                views: vs))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[sv]-0-|",
                                                                options: .directionLeftToRight,
                                                                metrics: nil,
                                                                views: vs))
        storageView.canvasDelegate = listener
        sv = storageView
    }

    // MARK: - DeliveryPresentable

    func show(canvas: [MapElement]) {
        sv.addMapElements(canvas)
        sv.theme = theme
    }

    func show(state: DeliverState) {
        if state == .finished {
            sv.removeMapElements()
        }
    }

    func moveTo(point: CGPoint) {
    }

    func show(help: String) {
        removePopUp()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "helperPopup") as! HelpPopUpVC
        let popUp = STPopupController(rootViewController: vc)
        vc.apply(theme: self.theme)
        vc.show(text: help)
        popUp.style = .bottomSheet
        popUp.backgroundView?.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                          action: #selector(backgroundViewDidTap)))
        popUp.transitionStyle = .slideVertical
        popUp.present(in: self)
        self.popUp = popUp
    }

    func removePopUp() {
        if let p = self.popUp {
            p.dismiss()
            self.popUp = nil
        }
    }

    @objc func backgroundViewDidTap() {
        removePopUp()
    }

    // MARK: - Themeble

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        STPopupNavigationBar.appearance().tintColor = theme.tintColor
        STPopupNavigationBar.appearance().barStyle = .default
        self.theme = theme
    }

    // MARK: - Private
    private var sv: StorageMapView!
    private var theme: Theme!
    private var popUp: STPopupController? = nil
}
