//
//  LoginRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol LoginInteractable: Interactable {
    weak var router: LoginRouting? { get set }
    weak var listener: LoginListener? { get set }
}

protocol LoginViewControllable: ViewControllable, Themeble {
}

final class LoginRouter: CommonRouter<LoginInteractable, LoginViewControllable>, LoginRouting {

    override init(interactor: LoginInteractable, viewController: LoginViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
