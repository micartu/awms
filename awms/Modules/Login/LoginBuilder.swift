//
//  LoginBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol LoginDependency: CommonDependency {
    var scanner: ScannerProtocol { get }
    var taskLoader: TaskService { get }
}

final class LoginComponent: Component<LoginDependency> {
}

// MARK: - Builder

protocol LoginBuildable: Buildable {
    func build(withListener listener: LoginListener) -> LoginRouting
}

final class LoginBuilder: Builder<LoginDependency>, LoginBuildable {

    override init(dependency: LoginDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LoginListener) -> LoginRouting {
        let _ = LoginComponent(dependency: dependency)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
        let interactor = LoginInteractor(presenter: viewController,
                                         themeManager: dependency.themeManager,
                                         taskLoader: dependency.taskLoader,
                                         network: dependency.network)
        interactor.listener = listener
        dependency.scanner.bindTo(delegate: interactor)
        return LoginRouter(interactor: interactor, viewController: viewController)
    }
}
