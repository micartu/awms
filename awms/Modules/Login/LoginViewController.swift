//
//  LoginViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol LoginPresentableListener: CommonPresentableListener, CommonCycleEvents {
    func loginButtonTouched(username: String, password: String)
    func userNameChangedTo(_ name: String)
}

final class LoginViewController: CommonVC, LoginPresentable, LoginViewControllable {

    weak var listener: LoginPresentableListener? {
        didSet {
            commonListener = listener
        }
    }
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: CornedButton!

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        loginButton.apply(theme: theme)
    }

    // MARK: - LoginViewControllable

    func changeLogin(login: String) {
        loginField.text = login
    }

    func changePassword(pass: String) {
        passwordField.text = pass
    }

    // MARK: - LoginPresentable

    func enableLoginButton(enable: Bool) {
        loginButton.enableButton = enable
    }

    // MARK: - Actions

    @objc func loginChanged(_ sender: UITextField) {
        listener?.userNameChangedTo(loginField.text!)
    }

    @IBAction func loginButtonAction(_ sender: Any) {
        listener?.loginButtonTouched(username: loginField.text!, password: passwordField.text!)
    }

    @IBAction func passwordPrAction(_ sender: Any) {
        loginButtonAction(sender)
    }

    @IBAction func loginFieldPrAction(_ sender: Any) {
        passwordField.becomeFirstResponder()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        loginField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Locator".localized
        addMenu()
        loginField.addTarget(self, action: #selector(loginChanged(_:)), for: .editingChanged)
    }
}
