//
//  LoginInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol LoginRouting: CommonViewableRouting {
}

protocol LoginPresentable: CommonPresentable {
    weak var listener: LoginPresentableListener? { get set }
    func enableLoginButton(enable: Bool)
    func changeLogin(login: String)
    func changePassword(pass: String)
}

protocol LoginListener: CommonPresentableListener {
    func successfulLogin(_ name: String)
}

final class LoginInteractor: CommonInteractor<LoginPresentable>, LoginInteractable, LoginPresentableListener {

    weak var router: LoginRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: LoginListener?
    var network: NetworkProtocol
    var taskLoader: TaskService

    init(presenter: LoginPresentable,
         themeManager: ThemeManagerProtocol,
         taskLoader: TaskService,
         network: NetworkProtocol) {

        self.taskLoader = taskLoader
        self.network = network

        super.init(presenter: presenter, themeManager: themeManager)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.enableLoginButton(enable: false)
    }

    // MARK: - LoginPresentableListener
    func loginButtonTouched(username: String, password: String) {
        presenter.showBusyIndicator()
        network.changeCredentials(user: username, pass: password, success: { [unowned self] response in
            self.presenter.hideBusyIndicator()
            self.resetUserData()
            // moving to the next module will be done from the root module's interactor
            self.listener?.successfulLogin(username)
        }, failure: { [unowned self] err in
            self.presenter.hideBusyIndicator()
            if self.taskLoader.countOfTasksFor(user: username) == 0 {
                self.presenter.show(title: "Network error".localized, error: err.localizedDescription)
            }
            else {
                self.resetUserData()
                // all data will be loaded from cache
                self.listener?.successfulLogin(username)
            }
        })
    }

    func userNameChangedTo(_ name: String) {
        presenter.enableLoginButton(enable: (name.count > 0))
    }

    // MARK: - Private

    private func resetUserData() {
        presenter.changeLogin(login: "")
        presenter.changePassword(pass: "")
        presenter.enableLoginButton(enable: false)
    }
}

extension LoginInteractor: ScannerDelegate {
    func itemScanned(type: ScanItemTypes, data: String) {
        if (type == .QR) {
            let d = data.split(separator: "|")
            if d.count > 1 {
                let login = String(d[0])
                let pass = String(d[1])
                if d.count > 2 && d[2] == "go" {
                    // enter credentials + try to login
                    loginButtonTouched(username: login,
                                       password: pass)
                } else {
                    // just change login + password
                    presenter.changeLogin(login: login)
                    presenter.changePassword(pass: pass)
                }
            }
        }
    }
}
