//
//  AttacherRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol AttacherInteractable: Interactable {
    weak var router: AttacherRouting? { get set }
    weak var listener: AttacherListener? { get set }
}

protocol AttacherViewControllable: CommonViewControllable {
}

final class AttacherRouter: CommonRouter<AttacherInteractable, AttacherViewControllable>, AttacherRouting {

    override init(interactor: AttacherInteractable, viewController: AttacherViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
