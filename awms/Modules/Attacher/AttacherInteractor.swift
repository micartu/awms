//
//  AttacherInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import CocoaLumberjack
import AVFoundation

protocol AttacherRouting: CommonViewableRouting {
}

protocol AttacherPresentable: CommonPresentable {
    weak var listener: AttacherPresentableListener? { get set }
    func set(type: String)
    func show(attaches: [Attachment])
    func set(imgLoader: ImageLoaderService)
    func takePhoto(with picker: UIImagePickerController)
}

protocol AttacherListener: CommonPresentableListener, CommonChildListener {
}

final class AttacherInteractor: CommonInteractor<AttacherPresentable> {

    weak var router: AttacherRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: AttacherListener? {
        didSet {
            commonListener = listener
        }
    }
    var storage: StorageService
    var saver: FileService
    var component: AttacherComponent

    init(presenter: AttacherPresentable, component c: AttacherComponent) {
        self.component = c
        self.storage = c.dependency.storage
        self.saver = c.dependency.saver
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        var type = ""
        if component.type == .image {
            type = "an image".localized
        } else {
            type = "a comment".localized
        }
        presenter.set(type: type)
        presenter.set(imgLoader: component.dependency.imgLoader)
    }

    override func willResignActive() {
        super.willResignActive()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }

    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            // The user has previously granted access to the camera.
            completionHandler(true)
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { success in
                completionHandler(success)
            })
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }

    func defaultDevice() -> AVCaptureDevice {
        if let device = AVCaptureDevice.default(.builtInDuoCamera,
                                                for: AVMediaType.video,
                                                position: .back) {
            return device // use dual camera on supported devices
        } else if let device = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                       for: AVMediaType.video,
                                                       position: .back) {
            return device // use default back facing camera otherwise
        } else {
            let err = "All supported devices are expected to have at least one of the queried capture devices."
            DDLogError(err)
            fatalError(err)
        }
    }

    func updateData() {
        if let attaches = storage.getAttachments(of: component.type) {
            presenter.show(attaches: attaches)
        }
    }

    // MARK: - Private
}

extension AttacherInteractor: AttacherInteractable {
}

extension AttacherInteractor: AttacherPresentableListener {
    func addAction() {
        if component.type == .image {
            // try to load an image
            checkCameraAuthorization() { [weak self] (authorized) in
                if authorized {
                    let imagePicker = UIImagePickerController()
                    imagePicker.sourceType = .camera
                    self?.presenter.takePhoto(with: imagePicker)
                } else {
                    DDLogError("user denied usage of camera...")
                }
            }
        } else {
            // attach a message
            presenter.showInputDialog(title: "Enter a message".localized,
                                      message: "Please describe what's happened".localized,
                                      okAction: { [weak self] (text) in
                                        let m = createMsgAttachment(msg: text)
                                        self?.storage.create(attachment: m) {
                                            self?.updateData()
                                        }
            })
        }
    }

    func captured(image: UIImage) {
        presenter.showInputDialog(title: "Enter a message".localized,
                                  message: "Please enter what's happened".localized,
                                  okAction: { [weak self] (text) in
                                    let imageData = UIImagePNGRepresentation(image)!
                                    let name = NSUUID().uuidString
                                    let img = createImageAttachment(name: name, msg: text)
                                    self?.saver.save(FileTypes.image, name: name, with: imageData)
                                    self?.storage.create(attachment: img) {
                                        self?.updateData()
                                    }
        })
    }

    func deleteCell(at cell: Int) {
        if let attaches = storage.getAttachments(of: component.type) {
            let at = attaches[cell]
            storage.delete(attachment: at) { [weak self] in
                if at.type == .image {
                    self?.saver.remove(image: at.img)
                }
                self?.updateData()
            }
        }
    }
}
