//
//  AttacherViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol AttacherPresentableListener: CommonPresentableListener, CommonCycleEvents {
    func addAction()
    func captured(image: UIImage)
    func deleteCell(at cell: Int)
}

final class AttacherViewController: CommonVC, AttacherViewControllable {
    weak var listener: AttacherPresentableListener? {
        didSet {
            commonListener = listener
        }
    }
    @IBOutlet weak var tableView: UITableView!
    var type: String = ""
    var records: [Attachment]? = nil
    var imgLoader: ImageLoaderService? = nil

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Attach".localized + " " + type

        tableView.dataSource = self
        tableView.delegate = self

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                                 target: self,
                                                                 action: #selector(addAction))
    }

    @objc func addAction() {
        listener?.addAction()
    }
}

extension AttacherViewController: UINavigationControllerDelegate {
}

extension AttacherViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            listener?.captured(image: image)
        }
    }
}

extension AttacherViewController: AttacherPresentable {
    func set(type: String) {
        self.type = type
    }

    func show(attaches: [Attachment]) {
        records = attaches
        tableView.reloadData()
    }

    func set(imgLoader: ImageLoaderService) {
        self.imgLoader = imgLoader
    }

    func takePhoto(with picker: UIImagePickerController) {
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
}

extension AttacherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let recs = records {
            return recs.count
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "attachmentCell")!
        let d = records![indexPath.row]
        if d.type == .image, let img = imgLoader?.localLoad(image: d.img) {
            cell.imageView?.image = img
        }
        cell.textLabel?.text = d.msg
        return cell
    }
}

extension AttacherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            listener?.deleteCell(at: indexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //listener?.selectedCellChanged(to: indexPath.row)
    }
}
