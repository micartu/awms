//
//  AttacherBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol AttacherDependency: CommonDependency {
    var imgLoader: ImageLoaderService { get }
    var storage: StorageService { get }
    var saver: FileService { get }
}

final class AttacherComponent: CommonComponent<AttacherDependency> {
    var type: AttachType

    init(dependency: AttacherDependency, type: AttachType) {
        self.type = type
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol AttacherBuildable: Buildable {
    func build(withListener listener: AttacherListener, type: AttachType) -> AttacherRouting
}

final class AttacherBuilder: Builder<AttacherDependency>, AttacherBuildable {

    override init(dependency: AttacherDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: AttacherListener, type: AttachType) -> AttacherRouting {
        let c = AttacherComponent(dependency: dependency, type: type)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "attacher") as! AttacherViewController
        let interactor = AttacherInteractor(presenter: viewController, component: c)
        interactor.listener = listener
        return AttacherRouter(interactor: interactor, viewController: viewController)
    }
}
