//
//  TaskBoardInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol TaskBoardRouting: CommonViewableRouting {
    func show(items: [TaskDesc])
    func hideRoundMenu()
    func showScanBoard(with router: ScanBoardRouting)
    func removeScanBoard()
    func showDelivery(with router: DeliveryRouting)
    func removeDelivery()
    func showAttacher(with router: AttacherRouting)
    func removeAttacher()
    func showTruckDelivery(with router: TruckDeliveryRouting)
    func removeTruckDelivery()
}

protocol TaskBoardPresentable: CommonPresentable {
    weak var listener: TaskBoardPresentableListener? { get set }
    func refreshData(at pos: Int, with data: TaskDesc)
    func showElements(_ items: [TaskDesc])
    func show(user: String)
    func show(roundMenu: Bool)
    func closeRoundMenu()
}

protocol TaskBoardListener: CommonPresentableListener, CommonChildListener {
}

final class TaskBoardInteractor: CommonInteractor<TaskBoardPresentable>,
    TaskBoardInteractable, TaskBoardPresentableListener,
ScanBoardListener, DeliveryListener, AttacherListener, TruckDeliveryListener {

    weak var router: TaskBoardRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: TaskBoardListener? {
        didSet {
            commonListener = listener
        }
    }
    private let taskLoader: TaskService
    private let uploader: UploaderService
    private let menu: LeftMenuRouting
    private let root: RootRouting
    private let storage: StorageService
    private let scanner: ScannerProtocol
    private let cleaner: CleanerService
    private let login: String
    private var selectedCell = -1
    internal var tasks: [Task]? = nil
    private var tasks2show: [TaskDesc]? = nil
    internal var component: TaskBoardComponent
    internal var childTask: Task? = nil

    init(presenter: TaskBoardPresentable,
         component c: TaskBoardComponent) {
        self.taskLoader = c.dependency.taskLoader
        self.menu = c.menu!
        self.login = c.dependency.login
        self.storage = c.dependency.storage
        self.cleaner = c.dependency.cleaner
        self.uploader = c.dependency.uploader
        self.root = c.dependency.root!
        self.scanner = c.dependency.scanner
        self.component = c
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        presenter.listener = self
        initMenu()
        scanner.bindTo(delegate: self)
        menu.subscribe(self)
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    // MARK: - LeftMenuListener

    override func selectedMenu(_ index: MenuType) {
        if childTask == nil {
            switch index {
            case .logoutMenu:
                menu.unsubscribe(self)
                root.logout()
            default:
                print("TaskBoardInteractor: unsupported menu type: \(index)")
            }
        }
    }

    // MARK: - TaskBoardPresentableListener

    func goToNextStep() {
        if selectedCell > -1 {
            go(to: tasks?[selectedCell])
        } else {
            // try to find a next task to go to based on the amount of fulfilled work
            if let tasks = tasks {
                for t in tasks {
                    switch t {
                    case is TaskScan:
                        if let bagItems = storage.getBagItems() {
                            if bagItems.count > 0 {
                                continue
                            }
                        }
                        go(to: t)
                        return
                    case is TaskPrint:
                        fallthrough
                    case is TaskTruckDelivery:
                        fallthrough
                    case is TaskDelivery:
                        if t.state != .finished {
                            go(to: t)
                            return
                        }
                    default:
                        print("cannot go to the task: \(t.type)!")
                    }
                }
            }
        }
    }

    internal func go(to task: Task?) {
        if let printTask = task as? TaskPrint {
            // almost the same as scan task but for printing
            let scanb = ScanBoardBuilder(dependency: self.component)
            let r = scanb.build(withListener: self, with: printTask)
            childTask = printTask
            router?.showScanBoard(with: r)
            return
        }
        if let scanTask = task as? TaskScan {
            let scanb = ScanBoardBuilder(dependency: self.component)
            let r = scanb.build(withListener: self, with: scanTask)
            childTask = scanTask
            router?.showScanBoard(with: r)
        }
        if let deliveryTask = task as? TaskDelivery {
            if areAnyOrderedItemsInBag() {
                let deliveryb = DeliveryBuilder(dependency: self.component)
                let r = deliveryb.build(withListener: self, task: deliveryTask)
                childTask = deliveryTask
                router?.showDelivery(with: r)
            } else {
                presenter.show(title: "No ordered items".localized,
                               error: "Please scan awaited items and order them (sync with server)".localized)
            }
        }
        if let deliveryTask = task as? TaskTruckDelivery {
            let delb = TruckDeliveryBuilder(dependency: self.component)
            let r = delb.build(withListener: self, task: deliveryTask)
            childTask = task
            router?.showTruckDelivery(with: r)
        }
    }

    private func showAttacher(of type: AttachType) {
        let attacher = AttacherBuilder(dependency: self.component)
        let r = attacher.build(withListener: self, type: type)
        router?.showAttacher(with: r)
    }

    func doneAction() {
        presenter.showYesNO(title: "Send data to server".localized,
                            message: "Would you like to send all work back to server and finish your work?".localized,
                            actionYes: { [unowned self] (action) in
                                self.presenter.closeRoundMenu()
                                self.presenter.showBusyIndicator()
                                self.uploader.uploadData { (err) in
                                    self.presenter.hideBusyIndicator()
                                    if let e = err {
                                        self.presenter.show(title: "Error".localized,
                                                            error: e.localizedDescription)
                                    }
                                    else {
                                        self.presenter.showBusyIndicator()
                                        self.cleaner.cleanTasks {
                                            self.presenter.hideBusyIndicator()
                                            self.selectedMenu(.logoutMenu)
                                        }
                                    }
                                }
        }, actionNo: nil)
    }

    func photoAction() {
        showAttacher(of: .image)
    }

    func chatAction() {
        showAttacher(of: .msg)
    }

    func selectedCellChanged(to cell: Int) {
        selectedCell = cell
    }

    func deselectCell() {
        selectedCell = -1
    }

    // MARK: - TaskBoardInteractable

    func showUserInfo() {
        presenter.showBusyIndicator()
        taskLoader.askForUserInfoWith(login: login) { [unowned self] (user, err) in
            if let u = user {
                if u.name.count > 0 {
                    self.presenter.show(user: u.name)
                }
                else {
                    self.presenter.show(user: u.login)
                }
                self.updateTasks()
            } else {
                self.presenter.hideBusyIndicator()
                if let e = err {
                    self.presenter.show(title: "Warning".localized, error: e.localizedDescription)
                }
            }
        }
    }

    func updateTasks() {
        presenter.showBusyIndicator()
        taskLoader.askForTasks() { [unowned self] (tasks, err) in
            self.tasks = tasks
            self.presenter.hideBusyIndicator()
            if let tasks = tasks {
                var items = [TaskDesc]()
                var acomplished = true
                for t in tasks {
                    var title: String
                    var descr: String? = nil
                    var img: UIImage? = nil
                    switch t.type {
                    case .truckDelivery:
                        title = "Delivery with a Truck".localized
                    case .printLabels:
                        title = "Printing Labels".localized
                        var count = 0
                        var scanned = 0
                        if let items = (t as! TaskScan).items {
                            for i in items {
                                count += Int(i.amount)
                                scanned += i.scanned
                            }
                        }
                        descr = String.localizedStringWithFormat("Printed: %d out of awaited: %d".localized, scanned, count)
                    case .scan:
                        title = "Scanning".localized
                        var count = 0
                        var scanned = 0
                        if let items = (t as! TaskScan).items {
                            for i in items {
                                count += Int(i.amount)
                                scanned += i.scanned
                            }
                        }
                        descr = String.localizedStringWithFormat("Scanned: %d out of awaited: %d".localized, scanned, count)
                    case .delivery:
                        title = "Delivery".localized
                    default:
                        title = "Unknown".localized
                    }
                    var task = TaskDesc(title: title)
                    if let d = descr {
                        task.subtitle = d
                    }
                    switch t.state {
                    case .finished:
                        img = self.doneImage()
                    default:
                        acomplished = false
                        img = nil
                    }
                    task.image = img
                    items.append(task)
                }
                self.tasks2show = items
                if items.count > 0 {
                    self.presenter.showElements(items)
                    if acomplished {
                        self.doneAction()
                    }
                } else {
                    self.presenter.show(title: "No Actions".localized,
                                        error: "Nothing to do for today".localized)
                }
            } else if let e = err {
                self.presenter.show(title: "Error".localized,
                                    error: e.localizedDescription,
                                    action: { [unowned self] action in
                                        self.selectedMenu(.logoutMenu)
                })
            }
        }
    }

    // MARK: - CommonChildListener

    override func backAction() {
        initMenu()
        if childTask != nil {
            if childTask is TaskScan {
                router?.removeScanBoard()
            } else if childTask is TaskDelivery {
                router?.removeDelivery()
            } else if childTask is TaskTruckDelivery {
                router?.removeTruckDelivery()
            }
        }
        component.lastScan = ""
        childTask = nil
    }

    override func taskAcomplished(id: Int) {
        if let tasks = tasks {
            for i in tasks.indices {
                let t = tasks[i]
                if t.id == id {
                    t.state = .finished
                    storage.update(task: t) { [unowned self] in
                        var tt = self.tasks2show![i]
                        tt.image = self.doneImage()
                        self.presenter.refreshData(at: i, with: tt)
                    }
                }
            }
        }
    }

    // MARK: - CommonPresentableListener

    override func controllerRemoved() {
        backAction()
        router?.removeAttacher()
        component.lastScan = ""
    }

    override func moveBackInNavigation() {
        super.moveBackInNavigation()
        scanner.unbind(delegate: self)
        menu.unsubscribe(self)
        component.lastScan = ""
    }

    // MARK: - Private

    private func doneImage() -> UIImage {
        return UIImage(named: "done_simple")!
    }

    private func areAnyOrderedItemsInBag() -> Bool {
        if let items = storage.getBagItems() {
            for i in items {
                if i.storedIn > -1 {
                    return true
                }
            }
        }
        return false
    }

    private func initMenu() {
        self.menu.showMenu(menu: [.logoutMenu])
    }
}
