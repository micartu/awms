//
//  TaskBoardCell.swift
//  awms
//
//  Created by Michael Artuerhof on 24.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class TaskBoardCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var data: UILabel!

    func setup(model: TaskDesc) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        data.text = model.data
        cellImageView.image = model.image
    }
}
