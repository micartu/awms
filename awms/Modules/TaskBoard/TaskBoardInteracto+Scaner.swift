//
//  TaskBoardInteracto+Scaner.swift
//  awms
//
//  Created by Michael Artuerhof on 02.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension TaskBoardInteractor: ScannerDelegate {
    func itemScanned(type: ScanItemTypes, data: String) {
        if type == .QR {
            let d = data.split(separator: "|")
            if d.count > 1 && d[0] == "task" {
                if let tasks = self.tasks {
                    let id = String(d[1]).toInt
                    for t in tasks {
                        if t.id == id && childTask == nil {
                            component.lastScan = data
                            go(to: t)
                            break
                        }
                    }
                }
            }
        }
    }
}
