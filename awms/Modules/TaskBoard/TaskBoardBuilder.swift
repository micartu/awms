//
//  TaskBoardBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol TaskBoardDependency: CommonDependency {
    var mapLoader: MapService { get }
    var imgLoader: ImageLoaderService { get }
    var taskLoader: TaskService { get }
    var cleaner: CleanerService { get }
    var storage: StorageService { get }
    var uploader: UploaderService { get }
    var login: String { get }
    var root: RootRouting? { get }
    var scanner: ScannerProtocol { get }
    var saver: FileService { get }
}

final class TaskBoardComponent: CommonComponent<TaskBoardDependency>, ScanBoardDependency, DeliveryDependency, AttacherDependency {
    var mapLoader: MapService {
        get {
            return dependency.mapLoader
        }
    }

    var imgLoader: ImageLoaderService {
        get {
            return dependency.imgLoader
        }
    }

    var saver: FileService {
        get {
            return dependency.saver
        }
    }

    var storage: StorageService {
        get {
            return dependency.storage
        }
    }

    var menu: LeftMenuRouting? {
        get {
            return dependency.root!.menuRouter()
        }
    }

    var scanner: ScannerProtocol {
        get {
            return dependency.scanner
        }
    }

    var lastScan = ""
    var order: OrdererService

    override init(dependency: TaskBoardDependency) {
        self.order = Orderer(network: dependency.network,
                             cache: dependency.storage)
        super.init(dependency: dependency)
    }
}

extension TaskBoardComponent: TruckDeliveryDependency {
}

// MARK: - Builder

protocol TaskBoardBuildable: Buildable {
    func build(withListener listener: TaskBoardListener) -> TaskBoardRouting
}

final class TaskBoardBuilder: Builder<TaskBoardDependency>, TaskBoardBuildable {

    override init(dependency: TaskBoardDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: TaskBoardListener) -> TaskBoardRouting {
        let c = TaskBoardComponent(dependency: dependency)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "taskboard") as! TaskBoardViewController
        let interactor = TaskBoardInteractor(presenter: viewController,
                                             component: c)
        interactor.listener = listener
        return TaskBoardRouter(interactor: interactor, viewController: viewController)
    }
}
