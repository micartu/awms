//
//  TaskBoardViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol TaskBoardPresentableListener: CommonPresentableListener, CommonCycleEvents {
    func doneAction()
    func photoAction()
    func chatAction()

    // tasks
    func selectedCellChanged(to cell: Int)
    func deselectCell()

    // steps
    func goToNextStep()
}

final class TaskBoardViewController: CommonVC, TaskBoardPresentable, TaskBoardViewControllable {
    weak var listener: TaskBoardPresentableListener? {
        didSet {
            commonListener = listener
        }
    }
    var records: [TaskDesc]?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var plusButton: FloatingActionButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var menuView: UIViewX!
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userLabel: UILabel!

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Task Board".localized
        addMenu()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "next"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(goNextAction))

        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44

        closeRoundMenu()
        setupAnimatedElements()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            self.userInfoView.transform = .identity
        })
    }

    // MARK: - Actions

    @objc func goNextAction() {
        listener?.goToNextStep()
    }

    @IBAction func doneAction(_ sender: Any) {
        listener?.doneAction()
    }

    @IBAction func chatAction(_ sender: Any) {
        listener?.chatAction()
    }

    @IBAction func photoAction(_ sender: Any) {
        listener?.photoAction()
    }

    @IBAction func fabTouched(_ sender: Any) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
            if self.menuView.transform == .identity {
                self.closeRoundMenu()
            } else {
                self.menuView.transform = .identity
            }
        }, completion: nil)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let selected = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selected, animated: false)
            listener?.deselectCell()
        }
    }

    // MARK: - Themeble

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        plusButton.use(theme: theme)
        tableView.tintColor = theme.tintColor
        photoButton.tintColor = theme.buttonTextColor
        chatButton.tintColor = theme.buttonTextColor
        doneButton.tintColor = theme.buttonTextColor
        menuView.backgroundColor = theme.tintColor
    }

    // MARK: - TaskBoardViewControllable
    func showElements(_ items: [TaskDesc]) {
        records = items
        tableView.reloadData()
        animateCells()
    }

    func show(user: String) {
        self.userLabel.text = user
    }

    func show(roundMenu: Bool) {
        if roundMenu {
            fabTouched(self)
        } else {
            closeRoundMenu()
        }
    }

    // MARK: - TaskBoardPresentable

    func refreshData(at pos: Int, with data: TaskDesc) {
        let index = IndexPath(row: pos, section: 0)
        records![pos] = data
        tableView.beginUpdates()
        tableView.reloadRows(at: [index], with: .left)
        tableView.endUpdates()
    }

    func closeRoundMenu() {
        menuView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        plusButton.closeButton()
    }

    // MARK: - Private

    func setupAnimatedElements() {
        userInfoView.transform = CGAffineTransform(translationX: -userInfoView.frame.width, y: 0)
    }

    // if the animation was called once
    private var firstTime = true

    func animateCells() {
        if firstTime {
            let cells = tableView.visibleCells
            for cell in cells {
                cell.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
            }
            var delay = 0.9
            for cell in cells {
                UIView.animate(withDuration: 0.3,
                               delay: delay,
                               usingSpringWithDamping: 0.5,
                               initialSpringVelocity: 0,
                               options: .curveEaseIn,
                               animations: {
                    cell.transform = .identity
                }, completion: nil)
                delay += 0.1
            }
            firstTime = false
        }
    }
}

extension TaskBoardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let c = records?.count {
            return c
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableBoardCell") as! TaskBoardCell
        cell.setup(model: records![indexPath.row])
        return cell
    }
}

extension TaskBoardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listener?.selectedCellChanged(to: indexPath.row)
    }
}
