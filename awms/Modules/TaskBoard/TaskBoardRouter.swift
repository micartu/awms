//
//  TaskBoardRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import STPopup

protocol TaskBoardInteractable: Interactable {
    weak var router: TaskBoardRouting? { get set }
    weak var listener: TaskBoardListener? { get set }

    func showUserInfo()
    func updateTasks()
}

protocol TaskBoardViewControllable: CommonViewControllable {
    func showElements(_ items: [TaskDesc])
    func show(user: String)
    func show(roundMenu: Bool)
}

final class TaskBoardRouter: CommonRouter<TaskBoardInteractable, TaskBoardViewControllable>, TaskBoardRouting {

    override init(interactor: TaskBoardInteractable, viewController: TaskBoardViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
        interactor.showUserInfo()
    }

    func show(items: [TaskDesc]) {
        viewController.showElements(items)
    }

    func hideRoundMenu() {
        viewController.show(roundMenu: false)
    }

    func showScanBoard(with router: ScanBoardRouting) {
        scanRouter = router
        attachChild(router)
        viewController.navController()?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }

    func removeScanBoard() {
        if let r = scanRouter {
            detachChild(r)
            scanRouter = nil
            updateData()
        }
    }

    func showDelivery(with router: DeliveryRouting) {
        deliveryRouter = router
        attachChild(router)
        viewController.navController()?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }

    func removeDelivery() {
        if let r = deliveryRouter {
            detachChild(r)
            deliveryRouter = nil
            updateData()
        }
    }

    func showAttacher(with router: AttacherRouting) {
        removeAttacher()
        attachRouter = router
        attachChild(router)
        let vc = router.viewControllable.uiviewController
        let popUp = generatePopUp(for: vc)
        popUp.transitionStyle = .slideVertical
        popUp.present(in: viewController.uiviewController)
    }

    func removeAttacher() {
        if let r = attachRouter {
            detachChild(r)
            attachRouter = nil
        }
    }

    func showTruckDelivery(with router: TruckDeliveryRouting) {
        truckDeliveryRouter = router
        attachChild(router)
        viewController.navController()?.pushViewController(router.viewControllable.uiviewController, animated: true)
    }

    func removeTruckDelivery() {
        if let r = truckDeliveryRouter {
            detachChild(r)
            truckDeliveryRouter = nil
            updateData()
        }
    }

    private func updateData() {
        // update tasks maybe something has changed
        interactor.updateTasks()
    }

    // MARK: - Private

    private var scanRouter: ScanBoardRouting? = nil
    private var deliveryRouter: DeliveryRouting? = nil
    private var attachRouter: AttacherRouting? = nil
    private var truckDeliveryRouter: TruckDeliveryRouting? = nil
}
