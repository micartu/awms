//
//  TaskDesc.swift
//  awms
//
//  Created by Michael Artuerhof on 24.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

struct TaskDesc {
    let title: String
    var subtitle: String
    var data: String
    var image: UIImage? = nil

    init(title: String = "", subtitle: String = "", data: String = "") {
        self.title = title
        self.subtitle = subtitle
        self.data = data
    }
}
