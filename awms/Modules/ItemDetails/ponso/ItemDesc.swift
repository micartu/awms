//
//  ItemDesc.swift
//  awms
//
//  Created by Michael Artuerhof on 06.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

enum ItemDescType {
    case Title
    case Text
    case Img
    case PrintButton
}

struct ItemDesc {
    let type: ItemDescType
    var text: String
    var img: UIImage? = nil
}
