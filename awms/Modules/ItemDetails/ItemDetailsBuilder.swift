//
//  ItemDetailsBuilder.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol ItemDetailsDependency: CommonDependency {
    var mode: ScanBoardMode { get }
    var printer: PrinterService { get }
}

final class ItemDetailsComponent: Component<ItemDetailsDependency> {
    let item: Item

    init(dependency: ItemDetailsDependency, item: Item) {
        self.item = item
        super.init(dependency: dependency)
    }

}

// MARK: - Builder

protocol ItemDetailsBuildable: Buildable {
    func build(withListener listener: ItemDetailsListener, item: Item) -> ItemDetailsRouting
}

final class ItemDetailsBuilder: Builder<ItemDetailsDependency>, ItemDetailsBuildable {

    override init(dependency: ItemDetailsDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: ItemDetailsListener, item: Item) -> ItemDetailsRouting {
        let c = ItemDetailsComponent(dependency: dependency, item: item)
        let viewController = dependency.storyboard.instantiateViewController(withIdentifier: "itemdetails") as! ItemDetailsViewController
        let interactor = ItemDetailsInteractor(presenter: viewController, component: c)
        interactor.listener = listener
        return ItemDetailsRouter(interactor: interactor, viewController: viewController)
    }
}
