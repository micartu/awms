//
//  ItemDetailsViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol ItemDetailsPresentableListener: CommonPresentableListener {
    func printAction()
}

final class ItemDetailsViewController: CommonVC, ItemDetailsPresentable, ItemDetailsViewControllable {

    weak var listener: ItemDetailsPresentableListener? {
        didSet {
            commonListener = listener
        }
    }
    @IBOutlet weak var tableView: UITableView!
    private var records: [ItemDesc]? = nil

    // MARK: - life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        self.theme = theme
    }

    // MARK: - ItemDetailsPresentable

    func show(details: [ItemDesc]) {
        records = details
        tableView.reloadData()
    }

    func set(title: String) {
        self.title = title
    }

    // MARK: - Actions

    @IBAction func printAction() {
        listener?.printAction()
    }

    // MARK: - private

    private var theme: Theme!
}

extension ItemDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let c = records?.count {
            return c
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var c: UITableViewCell? = nil
        let d = records![indexPath.row]
        switch d.type {
        case .Title:
            let cell = tableView.dequeueReusableCell(withIdentifier: "titlerow") as! ItemDetailsCell
            cell.itemLabel.text = d.text
            c = cell
        case .Text:
            let cell = tableView.dequeueReusableCell(withIdentifier: "textrow") as! ItemDetailsCell
            cell.itemLabel.text = d.text
            c = cell
        case .Img:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgrow") as! ItemDetailsCell
            cell.itemImage.image = d.img
            c = cell
        case .PrintButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: "printButtonCell") as! ItemDetailsCell
            cell.apply(theme: self.theme)
            c = cell
        }
        return c!
    }
}

extension ItemDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let d = records![indexPath.row]
        switch d.type {
        case .Img:
            let height = d.img!.size.height
            let width = d.img!.size.width
            let screenWidth = UIScreen.main.bounds.width
            let percent = screenWidth / width
            return percent * height
        case .PrintButton:
            return 54
        default:
            return 44
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
}
