//
//  ItemDetailsInteractor.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs
import RxSwift

protocol ItemDetailsRouting: CommonViewableRouting {
}

protocol ItemDetailsPresentable: CommonPresentable {
    weak var listener: ItemDetailsPresentableListener? { get set }
    func show(details: [ItemDesc])
    func set(title: String)
}

protocol ItemDetailsListener: CommonChildListener {
}

final class ItemDetailsInteractor: CommonInteractor<ItemDetailsPresentable>, ItemDetailsInteractable, ItemDetailsPresentableListener {

    weak var router: ItemDetailsRouting? {
        didSet {
            commonRouter = router
        }
    }
    weak var listener: ItemDetailsListener? {
        didSet {
            commonListener = listener
        }
    }
    let item: Item
    let printer: PrinterService
    let c: ItemDetailsComponent

    init(presenter: ItemDetailsPresentable, component c: ItemDetailsComponent) {
        item = c.item
        printer = c.dependency.printer
        self.c = c
        super.init(presenter: presenter, themeManager: c.dependency.themeManager)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        var i2s = [ItemDesc]()
        i2s.append(ItemDesc(type: .Title, text: "\(item.title)", img: nil))
        i2s.append(ItemDesc(type: .Text, text: "\(item.descr)", img: nil))
        i2s.append(ItemDesc(type: .Img, text: "", img: item.img!))
        if c.dependency.mode == .print {
            i2s.append(ItemDesc(type: .PrintButton, text: "", img: nil))
            presenter.set(title: "Print".localized)
        } else {
            presenter.set(title: "Item Details".localized)
        }
        presenter.show(details: i2s)
    }

    func printAction() {
        printer.print(qr: item.qr) { [unowned self] (success, er) in
            if let e = er {
                self.presenter.show(title: "Error".localized,
                                    error: e.localizedDescription)
            }
        }
    }
}
