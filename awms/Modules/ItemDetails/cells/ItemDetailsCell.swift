//
//  ItemDetailsCell.swift
//  awms
//
//  Created by Michael Artuerhof on 06.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class ItemDetailsCell: UITableViewCell {
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var printButton: CornedButton!

    func apply(theme: Theme) {
        printButton.apply(theme: theme)
    }
}
