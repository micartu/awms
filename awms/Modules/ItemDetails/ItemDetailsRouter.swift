//
//  ItemDetailsRouter.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

protocol ItemDetailsInteractable: Interactable {
    weak var router: ItemDetailsRouting? { get set }
    weak var listener: ItemDetailsListener? { get set }
}

protocol ItemDetailsViewControllable: CommonViewControllable, MenuProtocol {
}

final class ItemDetailsRouter: CommonRouter<ItemDetailsInteractable, ItemDetailsViewControllable>, ItemDetailsRouting {

    override init(interactor: ItemDetailsInteractable, viewController: ItemDetailsViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
