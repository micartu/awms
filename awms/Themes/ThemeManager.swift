//
//  ThemeManager.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class ThemeManager: ThemeManagerProtocol {
    var delegate: ThemeManagerDelegate?
    var theme: Theme?

    init() {
        let blueColor = #colorLiteral(red: 0.2431, green: 0.6431, blue: 0.8627, alpha: 1) /* #3ea4dc */
        let lightBlueColor = #colorLiteral(red: 0, green: 0.4902, blue: 0.8157, alpha: 1) /* #007dd0 */
        let redColor = #colorLiteral(red: 0.8078, green: 0.4314, blue: 0, alpha: 1) /* #ce6e00 */
        let greenColor = #colorLiteral(red: 0, green: 0.698, blue: 0.3608, alpha: 1) /* #00b25c */
        let darkBlueColor = #colorLiteral(red: 0, green: 0.2588, blue: 0.5373, alpha: 1) /* #004289 */
        let errColor = #colorLiteral(red: 0.8667, green: 0, blue: 0, alpha: 1) /* #dd0000 */
        self.theme = Theme(buttonBackColor: blueColor,
                           buttonActivatedBackColor: lightBlueColor,
                           buttonBorderColor: blueColor,
                           buttonTextColor: UIColor.white,
                           buttonDisabledColor: UIColor.lightGray,
                           cellScannedOk: greenColor,
                           cellScannedError: errColor,
                           currentMapTipColor: redColor,
                           additionalMapTipColor: greenColor,
                           animatedMapTipColor: darkBlueColor,
                           tintColor: blueColor,
                           topFont: UIFont(name: "OpenSans-SemiBold", size: 20)!,
                           mainColor: UIColor.black)
    }

    func set(delegate: ThemeManagerDelegate) {
        self.delegate = delegate
    }

    func getCurrentTheme() -> Theme {
        return theme!
    }

    func change(theme: Theme) {
        self.theme = theme
        self.delegate?.themeChanged(theme)
    }
}
