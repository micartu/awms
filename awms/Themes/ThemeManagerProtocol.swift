//
//  ThemeManagerProtocol.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

protocol ThemeManagerDelegate: class {
    func themeChanged(_ theme: Theme)
}

protocol ThemeManagerProtocol: class {
    func set(delegate: ThemeManagerDelegate)
    func getCurrentTheme() -> Theme
}

protocol ThemebleRouting: class {
    func change(theme: Theme)
}

protocol Themeble: class {
    func apply(theme: Theme)
}

struct Theme {
    let buttonBackColor: UIColor
    let buttonActivatedBackColor: UIColor
    let buttonBorderColor: UIColor
    let buttonTextColor: UIColor
    let buttonDisabledColor: UIColor
    let cellScannedOk: UIColor
    let cellScannedError: UIColor
    let currentMapTipColor: UIColor
    let additionalMapTipColor: UIColor
    let animatedMapTipColor: UIColor
    let tintColor: UIColor
    let topFont: UIFont
    let mainColor: UIColor
}
