//
//  AppDelegate.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import RIBs

protocol AppDelegateProtocol: UIApplicationDelegate {
    func description() -> String
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var delegates: [UIResponder & AppDelegateProtocol] = [LoggerInit(),
        CoreDataInit()]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let rootLauncher = RootBuilder(dependency: AppComponent()).build()
        self.rootLauncher = rootLauncher
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        for d in delegates {
            if !(d.application?(application, didFinishLaunchingWithOptions: launchOptions))! {
                print("Problem when loading a delegate: \(d.description())")
                return false
            }
        }
        rootLauncher.launchFromWindow(window)
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        for d in delegates {
            d.applicationDidEnterBackground?(application)
        }
    }

    // MARK: - Private
    private var rootLauncher: LaunchRouting?
}
