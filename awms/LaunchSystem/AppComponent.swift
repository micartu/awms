//
//  AppComponent.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import RIBs

class AppComponent: Component<EmptyDependency>, RootDependency {

    init() {
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        themeManager = ThemeManager()
        network = ServicesAssembler.shared().server
        super.init(dependency: EmptyComponent())
    }

    var storyboard: UIStoryboard
    var themeManager: ThemeManagerProtocol
    var network: NetworkProtocol
}
