//
//  CoreDataInit.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import MagicalRecord

class CoreDataInit: UIResponder, AppDelegateProtocol {
    func description() -> String {
        return "CoreDataInit"
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: "awms")
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (success, err) in
            if let e = err {
                print("error while saving core data: \(e.localizedDescription)")
            }
        })
    }
}
