//
//  LoggerInit.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import CocoaLumberjack

class LoggerInit: UIResponder, AppDelegateProtocol {
    func description() -> String {
        return "LoggerInit"
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        var logLevel: UInt = DDLogLevel.verbose.rawValue
        if let plistPath = Bundle.main.path(forResource: "load", ofType: "plist") {
            let opt = NSDictionary(contentsOfFile: plistPath)!
            if let debugLevel = opt["log_debug_level"] as? UInt {
                logLevel = debugLevel
            }
        }
        if let level = DDLogLevel(rawValue: logLevel) {
            DDLog.add(DDASLLogger.sharedInstance, with: level) // ASL = Apple System Logs
            let fileLogger: DDFileLogger = DDFileLogger() // File Logger
            fileLogger.rollingFrequency = TimeInterval(60 * 60 * 24)  // 24 hours
            fileLogger.logFileManager.maximumNumberOfLogFiles = 7
            DDLog.add(fileLogger, with: level)
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                DDLogDebug("Application version: \(version)")
            }
            return true
        }
        return false
    }
}
