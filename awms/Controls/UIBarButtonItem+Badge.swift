//
//  UIBarButtonItem+Badge.swift
//  awms
//
//  Created by Michael Artuerhof on 06.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var rectOneDigit: CGRect {
        get {
            return CGRect(x: 26, y: 6, width: 14, height: 14)
        }
    }
    private var rectTwoDigit: CGRect {
        get {
            return CGRect(x: 26, y: 6, width: 18, height: 14)
        }
    }
    private var badgeLabel: UILabel? {
        if let b: Any = objc_getAssociatedObject(self, &handle) {
            return b as? UILabel
        } else {
            return nil
        }
    }

    private func setup(badge: UILabel, with number: Int) {
        badge.frame = number > 9 ? rectTwoDigit : rectOneDigit
        badge.alpha = number > 0 ? 1 : 0
        badge.text = "\(number)"
    }

    func addBadge(number: Int, withColor color: UIColor = UIColor.red) {
        guard let view = self.value(forKey: "view") as? UIView else { return }

        badgeLabel?.removeFromSuperview()

        let label = UILabel()
        label.backgroundColor = color
        label.layer.cornerRadius = 7
        label.layer.masksToBounds = true
        label.font = UIFont(name: "OpenSans-SemiBold", size: 10)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.isUserInteractionEnabled = false
        setup(badge: label, with: number)
        view.addSubview(label)
        // make sure that the label has the most front position:
        label.layer.zPosition = CGFloat(MAXFLOAT)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, label, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    func updateBadge(number: Int) {
        if number == 0 {
            removeBadge()
            return
        }
        if let badge = self.badgeLabel {
            setup(badge: badge, with: number)
        } else {
            self.addBadge(number: number)
        }
        self.badgeLabel?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0,
                       options: .curveEaseIn,
                       animations: {
            self.badgeLabel?.transform = .identity
        })
    }

    func removeBadge() {
        badgeLabel?.removeFromSuperview()
        objc_setAssociatedObject(self, &handle, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}
