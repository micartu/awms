//
//  FloatingActionButton.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class FloatingActionButton: UIButtonX {
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        toggleState()
        return super.beginTracking(touch, with: event)
    }

    func use(theme: Theme) {
        self.theme = theme
        applyTheme()
    }

    func closeButton() {
        if transform != .identity {
            toggleState()
        }
    }

    // MARK: - Private

    private func applyTheme() {
        self.backgroundColor = self.theme?.tintColor
        self.tintColor = self.theme?.buttonTextColor
    }

    private func toggleState() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: { [weak self] in
            if self?.transform == .identity {
                self?.transform = CGAffineTransform(rotationAngle: .pi / 4)
                self?.backgroundColor = self?.theme?.buttonActivatedBackColor
            } else {
                self?.transform = .identity
                self?.applyTheme()
            }
        })
    }

    private var theme: Theme?
}
