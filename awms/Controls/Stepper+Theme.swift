//
//  Stepper+Theme.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension Stepper: Themeble {
    func apply(theme: Theme) {
        increaseButton.tintColor = theme.mainColor
        decreaseButton.tintColor = theme.mainColor
    }
}
