//
//  Stepper.swift
//  awms
//
//  Created by Michael Artuerhof on 05.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

protocol StepperDelegate: class {
    func valueChanged(_ value: Int)
}

@IBDesignable
class Stepper: UIControl {
    @IBInspectable var fontName: String = "OpenSans-SemiBold"
    @IBInspectable var fontColor: UIColor = UIColor.black
    @IBInspectable var fontSize: CGFloat = 15
    @IBInspectable var showLabel: Bool = true
    @IBInspectable var showMaximum: Bool = false
    @IBInspectable var minLimit: Bool = false
    let buttonSize: CGFloat = 24

    weak var delegate: StepperDelegate? = nil

    /// value to be shown
    public var value: Int = 0 {
        didSet {
            updateLabel()
        }
    }

    /// maximum possible value to be shown
    public var maxValue: Int = 0

    /// minimum possible value to be shown
    public var minValue: Int = 0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeDefaults()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeDefaults()
    }

    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: showLabel ? label.bounds.width : increaseButton.bounds.width,
                height: increaseButton.bounds.height * 4 + label.bounds.height)
        }
    }

    override func awakeFromNib() {
        initializeDefaults()
    }

    // MARK: - private

    private func initializeControls() {
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: fontName, size: fontSize)
        label.textColor = fontColor
        label.textAlignment = .center
        addSubview(label)
        updateLabel()

        increaseButton = UIButton(type: .custom)
        increaseButton.translatesAutoresizingMaskIntoConstraints = false
        increaseButton.setImage(UIImage(named: "bplus"), for: .normal)
        increaseButton.addTarget(self, action: #selector(increaseButtonPressed), for: .touchUpInside)
        addSubview(increaseButton)

        decreaseButton = UIButton(type: .custom)
        decreaseButton.translatesAutoresizingMaskIntoConstraints = false
        decreaseButton.setImage(UIImage(named: "bminus"), for: .normal)
        decreaseButton.addTarget(self, action: #selector(decreaseButtonPressed), for: .touchUpInside)
        addSubview(decreaseButton)
    }

    private func initializeLayout() {
        let views = ["dButton" : decreaseButton,
            "label" : label,
            "iButton" : increaseButton
            ] as [String : Any]
        let m = ["buttonSize" : buttonSize]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[iButton(==buttonSize)]-2-[label]-2-[dButton(==buttonSize)]-0-|",
                                                      options: .directionLeftToRight,
                                                      metrics: m, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[dButton(==buttonSize)]",
                                                      options: .directionLeftToRight,
                                                      metrics: m, views: views))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[iButton(==buttonSize)]",
                                                      options: .directionLeftToRight,
                                                      metrics: m, views: views))

        addConstraint(NSLayoutConstraint(item: increaseButton,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: label,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: decreaseButton,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1,
                                         constant: 0))
    }


    private func initializeDefaults() {
        backgroundColor = UIColor.clear
        if !initialized {
            initialized = true
            initializeControls()
            initializeLayout()
        }
    }

    private func updateLabel() {
        if showLabel {
            if showMaximum {
                label.text = "\(value)/\(maxValue)"
            } else {
                label.text = "\(value)"
            }
        } else {
            label.text = ""
        }
    }

    private func changeValue(increase: Bool) {
        let value = self.value

        if increase {
            if self.maxValue > 0 {
                self.value = self.value >= self.maxValue ? self.maxValue : self.value + 1
            }
            else {
                self.value += 1
            }
        } else { // decrease
            if self.minValue > 0 || self.minLimit {
                self.value = self.value <= self.minValue ? self.minValue : self.value - 1
            }
            else {
                self.value -= 1
            }
        }
        if self.value != value {
            updateLabel()
            self.sendActions(for: .valueChanged)
            delegate?.valueChanged(self.value)
        }
    }

    @objc func increaseButtonPressed() {
        changeValue(increase: true)

    }

    @objc func decreaseButtonPressed() {
        changeValue(increase: false)
    }

    internal var label: UILabel! = nil
    internal var increaseButton: UIButton! = nil
    internal var decreaseButton: UIButton! = nil
    private var initialized = false
}
