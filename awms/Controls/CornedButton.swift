//
//  CornedButton.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class CornedButton: UIButton {

    let kAnimationScale: CGFloat = 1.03
    let kAnimationDamping: CGFloat = 0.6
    let kAnimationDuration: TimeInterval = 0.3

    // MARK: - Properties
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var backColor: UIColor = UIColor.clear {
        didSet {
            layer.backgroundColor = backColor.cgColor
        }
    }

    @IBInspectable var disabledBackColor: UIColor = UIColor.clear
    @IBInspectable var disabledBorderColor: UIColor = UIColor.clear

    @IBInspectable var enableButton = true {
        didSet {
            if enableButton {
                layer.borderColor = borderColor.cgColor
                layer.backgroundColor = backColor.cgColor
            }
            else {
                layer.borderColor = disabledBorderColor.cgColor
                layer.backgroundColor = disabledBackColor.cgColor
            }
            self.isEnabled = enableButton
        }
    }

    // MARK: - Animation

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        self.transform = CGAffineTransform(scaleX: kAnimationScale, y: kAnimationScale)
        UIView.animate(withDuration: kAnimationDuration,
                       delay: 0,
                       usingSpringWithDamping: kAnimationDamping,
                       initialSpringVelocity: 0,
                       options: .curveEaseIn,
                       animations: {
                        self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}
