//
//  CornedButton+Theme.swift
//  awms
//
//  Created by Michael Artuerhof on 26.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension CornedButton {
    func apply(theme: Theme) {
        self.backColor = theme.buttonBackColor
        self.borderColor = theme.buttonBorderColor
        self.disabledBackColor = theme.buttonDisabledColor
        setTitleColor(theme.buttonTextColor, for: .normal)
        sizeToFit()
    }
}
