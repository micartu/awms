//
//  CTask+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CTask {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CTask> {
        return NSFetchRequest<CTask>(entityName: "CTask")
    }

    @NSManaged public var descr: String
    @NSManaged public var coords: String
    @NSManaged public var id: Int32
    @NSManaged public var type: Int16
    @NSManaged public var state: Int16
    @NSManaged public var map: String
    @NSManaged public var points: String
    @NSManaged public var loadTime: Date
    @NSManaged public var startNode: Int64
    @NSManaged public var items: NSSet?
    @NSManaged public var user: CUser?
}

// MARK: Generated accessors for items
extension CTask {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: CItem)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: CItem)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}
