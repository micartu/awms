//
//  CCapability+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CCapability {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CCapability> {
        return NSFetchRequest<CCapability>(entityName: "CCapability")
    }

    @NSManaged public var id: Int32
    @NSManaged public var h: Double
    @NSManaged public var mass: Double
    @NSManaged public var s: Double
    @NSManaged public var user: CUser?

}
