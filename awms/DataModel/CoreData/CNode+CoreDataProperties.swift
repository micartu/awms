//
//  CNode+CoreDataProperties.swift
//  awms
//
//  Created by michael on 12.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CNode {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CNode> {
        return NSFetchRequest<CNode>(entityName: "CNode")
    }

    @NSManaged public var id: Int32
    @NSManaged public var x: Double
    @NSManaged public var y: Double
    @NSManaged public var map: String?

}
