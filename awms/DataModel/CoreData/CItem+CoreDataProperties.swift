//
//  CItem+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CItem> {
        return NSFetchRequest<CItem>(entityName: "CItem")
    }

    @NSManaged public var id: Int64
    @NSManaged public var title: String?
    @NSManaged public var descr: String?
    @NSManaged public var qr: String?
    @NSManaged public var img: String?
    @NSManaged public var ean: Int64
    @NSManaged public var amount: Double
    @NSManaged public var status: Int16
    @NSManaged public var scanned: Int32
    @NSManaged public var s: Double
    @NSManaged public var h: Double
    @NSManaged public var m: Double
    @NSManaged public var storedIn: Int64
    @NSManaged public var task: CTask?
    @NSManaged public var user: CUser?

}
