//
//  CUser+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CUser> {
        return NSFetchRequest<CUser>(entityName: "CUser")
    }

    @NSManaged public var avatar: String?
    @NSManaged public var login: String?
    @NSManaged public var name: String?
    @NSManaged public var usedM: Double
    @NSManaged public var usedV: Double
    @NSManaged public var attachments: NSSet?
    @NSManaged public var capabilities: NSSet?
    @NSManaged public var items: NSSet?
    @NSManaged public var tasks: NSSet?

}

// MARK: Generated accessors for attachments
extension CUser {

    @objc(addAttachmentsObject:)
    @NSManaged public func addToAttachments(_ value: CAttachment)

    @objc(removeAttachmentsObject:)
    @NSManaged public func removeFromAttachments(_ value: CAttachment)

    @objc(addAttachments:)
    @NSManaged public func addToAttachments(_ values: NSSet)

    @objc(removeAttachments:)
    @NSManaged public func removeFromAttachments(_ values: NSSet)

}

// MARK: Generated accessors for capabilities
extension CUser {

    @objc(addCapabilitiesObject:)
    @NSManaged public func addToCapabilities(_ value: CCapability)

    @objc(removeCapabilitiesObject:)
    @NSManaged public func removeFromCapabilities(_ value: CCapability)

    @objc(addCapabilities:)
    @NSManaged public func addToCapabilities(_ values: NSSet)

    @objc(removeCapabilities:)
    @NSManaged public func removeFromCapabilities(_ values: NSSet)

}

// MARK: Generated accessors for items
extension CUser {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: CItem)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: CItem)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}

// MARK: Generated accessors for tasks
extension CUser {

    @objc(addTasksObject:)
    @NSManaged public func addToTasks(_ value: CTask)

    @objc(removeTasksObject:)
    @NSManaged public func removeFromTasks(_ value: CTask)

    @objc(addTasks:)
    @NSManaged public func addToTasks(_ values: NSSet)

    @objc(removeTasks:)
    @NSManaged public func removeFromTasks(_ values: NSSet)

}
