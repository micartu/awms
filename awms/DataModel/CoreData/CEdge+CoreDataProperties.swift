//
//  CEdge+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CEdge {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CEdge> {
        return NSFetchRequest<CEdge>(entityName: "CEdge")
    }

    @NSManaged public var from: Int32
    @NSManaged public var to: Int32
    @NSManaged public var weight: Double
    @NSManaged public var graph: CGraph?

}
