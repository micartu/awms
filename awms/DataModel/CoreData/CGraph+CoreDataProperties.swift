//
//  CGraph+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CGraph {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CGraph> {
        return NSFetchRequest<CGraph>(entityName: "CGraph")
    }

    @NSManaged public var e: Int32
    @NSManaged public var name: String?
    @NSManaged public var v: Int32
    @NSManaged public var edges: NSSet?

}

// MARK: Generated accessors for edges
extension CGraph {

    @objc(addEdgesObject:)
    @NSManaged public func addToEdges(_ value: CEdge)

    @objc(removeEdgesObject:)
    @NSManaged public func removeFromEdges(_ value: CEdge)

    @objc(addEdges:)
    @NSManaged public func addToEdges(_ values: NSSet)

    @objc(removeEdges:)
    @NSManaged public func removeFromEdges(_ values: NSSet)

}
