//
//  CAttachment+CoreDataProperties.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CAttachment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CAttachment> {
        return NSFetchRequest<CAttachment>(entityName: "CAttachment")
    }

    @NSManaged public var date: Date?
    @NSManaged public var id: Int32
    @NSManaged public var imgPath: String?
    @NSManaged public var msg: String?
    @NSManaged public var type: Int16
    @NSManaged public var user: CUser?

}
