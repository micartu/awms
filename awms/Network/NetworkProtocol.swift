//
//  NetworkProtocol.swift
//  awms
//
//  Created by Michael Artuerhof on 14.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol NetworkProtocol: class {
    func changeCredentials(user: String, pass: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?)
    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?)
    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?)
    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?)
    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?)
    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?)
    func uploadPNG(url: String, data: Data, name: String, completion: ((NSError?) -> Void)?)
}
