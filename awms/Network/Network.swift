//
//  Network.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import Alamofire

class Network: NetworkProtocol {
    let base: String!
    var oauthHandler: OAuth2Handler!
    let sessionManager = SessionManager()
    let cfg = NetworkConfig.getConfiguration()
    var user, pass: String!

    init(user: String, pass: String) {
        base = cfg.baseUrl
        changeCredentials(user: user, pass: pass, success: nil, failure: nil)
    }

    func changeCredentials(user: String, pass: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        self.user = user
        self.pass = pass

        if user.count > 0 {
            let authURL = base + "/token"
            let parameters: Parameters = ["grant_type": "password",
                                          "scope": "everything",
                                          "username": user,
                                          "password": pass,
                                          "client_id": cfg.clientId,
                                          "client_secret": cfg.clientSecret]
            Alamofire.request(authURL,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding(destination: .queryString))
                .responseJSON(completionHandler: { [unowned self] response in
                    self.commonParsingResponse(response, success: { jsonAr in
                        let json = jsonAr[0]
                        guard let token = json["access_token"] as? String else {
                            self.handleProblemWithJson(json, failure: failure)
                            return
                        }
                        guard let refresh_token = json["refresh_token"] as? String else {
                            self.handleProblemWithJson(json, failure: failure)
                            return
                        }
                        self.oauthHandler = OAuth2Handler(
                            clientID: self.cfg.clientId,
                            baseURLString: self.base,
                            accessToken: token,
                            refreshToken: refresh_token
                        )
                        self.sessionManager.adapter = self.oauthHandler!
                        self.sessionManager.retrier = self.oauthHandler!
                        if let s = success {
                            s(json)
                        }
                    }, failure: failure)
                })
        }
        else { // user name isn't set
            handle(failure: failure,
                   descr: "User name cannot be empty".localized)
        }
    }

    private func getCommonQuery(url: String,
                                successArray: (([NSDictionary]) -> Void)?,
                                successDict: ((NSDictionary) -> Void)?,
                                failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        sessionManager.request(absUrl).validate().responseJSON { [unowned self] response in
            self.commonParsingResponse(response, success: { json in
                if let s = successArray {
                    s(json)
                }
                if let s = successDict {
                    s(json[0])
                }
            }, failure: failure)
        }
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        sessionManager.request(absUrl).validate().responseData() { [unowned self] resp in
            if let response = resp.response {
                switch response.statusCode {
                case 200:
                    if let d = resp.data {
                        if let s = success {
                            s(d)
                        }
                    } else {
                        self.handle(failure: failure,
                                    descr: "No Data was returned".localized)
                    }
                default:
                    self.handle(failure: failure,
                                descr: "Network failure".localized)
                }
            }
            else {
                self.handle(failure: failure,
                            descr: "Network failure".localized)
            }
        }
    }

    func getQuery(url: String, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        getCommonQuery(url: url, successArray: nil, successDict: success, failure: failure)
    }

    func getQuery(url: String, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        getCommonQuery(url: url, successArray: success, successDict: nil, failure: failure)
    }

    private func postCommonQuery(url: String,
                                 data: Data,
                                 successArray: (([NSDictionary]) -> Void)?,
                                 successDict: ((NSDictionary) -> Void)?,
                                 failure: ((NSError) -> Void)?) {
        let absUrl = base + url
        let u = URL(string: absUrl)!
        var request = URLRequest(url: u)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        sessionManager.request(request).validate().responseJSON { [unowned self] response in
            self.commonParsingResponse(response, success: { json in
                if let s = successArray {
                    s(json)
                }
                if let s = successDict {
                    s(json[0])
                }
            }, failure: failure)
        }
    }

    func postQuery(url: String, data: Data, success: ((NSDictionary) -> Void)?, failure: ((NSError) -> Void)?) {
        postCommonQuery(url: url, data: data, successArray: nil, successDict: success, failure: failure)
    }

    func postQuery(url: String, data: Data, success: (([NSDictionary]) -> Void)?, failure: ((NSError) -> Void)?) {
        postCommonQuery(url: url, data: data, successArray: success, successDict: nil, failure: failure)
    }

    func uploadPNG(url: String, data: Data, name: String, completion: ((NSError?) -> Void)?) {
        let absUrl = base + url
        sessionManager.upload(multipartFormData: { multi in
            multi.append(data, withName: "fileupload", fileName: name, mimeType: "image/png")
        }, to: absUrl, encodingCompletion: { [unowned self] encoded in
            switch encoded {
            case .success(let upload, _, _):
                upload.validate().responseJSON { response in
                    self.commonParsingResponse(response, success: { json in
                        if let c = completion {
                            c(nil)
                        }
                    }, failure: { (err) in
                        if let c = completion {
                            c(err)
                        }
                    })
                }
            case .failure(let err):
                if let c = completion {
                    c(err as NSError)
                }
            }
        })
    }

    // MARK: - Private

    func commonParsingResponse(_ response: DataResponse<Any>, success: ([NSDictionary]) -> Void, failure: ((NSError) -> Void)?) {
        var error: NSError?
        if let status = response.response?.statusCode {
            switch(status){
            case 200:
                error = nil
            default:
                error = NSError(domain: Const.domain,
                                code: Const.kErrNet,
                                userInfo: [NSLocalizedDescriptionKey: "Network failure"])
            }
        }
        if error == nil {
            if let result = response.result.value {
                if let jsonAr = result as? Array<NSDictionary> {
                    success(jsonAr)
                } else if let json = result as? NSDictionary {
                    success([json])
                }
            } else {
                if let f = failure {
                    error = NSError(domain: Const.domain,
                                    code: Const.kErrNet,
                                    userInfo: [NSLocalizedDescriptionKey: "Cannot parse json"])
                    f(error!)
                }
            }
        }
        else {
            if let f = failure {
                f(error!)
            }
        }
    }

    private func handle(failure: ((NSError) -> Void)?, descr: String) {
        if let f = failure {
            let error = NSError(domain: Const.domain,
                                code: Const.kErrNet,
                                userInfo: [NSLocalizedDescriptionKey: descr])
            f(error)
        }
    }

    private func handleProblemWithJson(_ json: NSDictionary, failure: ((NSError) -> Void)?) {
        var descr: String
        if let description = json["error_description"] as? String {
            descr = description
        }
        else {
            descr = "network problems"
        }
        handle(failure: failure, descr: descr)
    }
}
