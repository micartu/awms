//
//  NetworkConfig.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

struct NetworkConfig {
    var baseUrl: String
    var clientId: String
    var clientSecret: String

    init() {
        self.baseUrl = ""
        self.clientId = ""
        self.clientSecret = ""
    }

    static func getConfiguration() -> NetworkConfig {
        var out = NetworkConfig()
        guard let plistPath = Bundle.main.path(forResource: "network", ofType: "plist") else {
            return out
        }
        let dict = NSDictionary(contentsOfFile: plistPath)!
        guard let baseUrl = dict["server_base_url"] as? String else {
            return out
        }
        out.baseUrl = baseUrl
        guard let clientId = dict["client_id"] as? String else {
            return out
        }
        out.clientId = clientId
        guard let clientSecret = dict["client_secret"] as? String else {
            return out
        }
        out.clientSecret = clientSecret
        return out
    }
}
