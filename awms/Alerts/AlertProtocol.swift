//
//  AlertProtocol.swift
//  awms
//
//  Created by Michael Artuerhof on 15.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

protocol AlertProtocol: class {
    func show(title: String, error: String)
    func show(title: String, error: String, action: ((UIAlertAction) -> Void)?)
    func show(title: String, message: String, action: ((UIAlertAction) -> Void)?)
    func showYesNO(title: String, message: String, actionYes: ((UIAlertAction) -> Void)?, actionNo: ((UIAlertAction) -> Void)?)
    func showInputDialog(title: String, message: String, okAction:((String) -> Void)?)

    // busy indicator
    func showBusyIndicator()
    func hideBusyIndicator()
}
