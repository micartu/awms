//
//  ImageLoaderService.swift
//  awms
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol ImageLoaderService {
    func localLoad(image: String) -> UIImage?
    func load(image: String, completion: @escaping ((UIImage) -> Void))
    func loadedImageNotification() -> String
    func noImage() -> UIImage
}
