//
//  ImageLoader.swift
//  awms
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import CocoaLumberjack

class ImageLoader: ImageLoaderService {
    let imageLoadedNotification = "image_loaded_notification"
    internal let server: NetworkProtocol
    internal let filer: FileService
    internal var cache: StorageService
    internal let queue: DispatchQueue
    internal lazy var noImg = UIImage(named: "no_img")!

    init(network: NetworkProtocol, cache: StorageService, saver: FileService) {
        self.server = network
        self.filer = saver
        self.cache = cache
        self.queue = DispatchQueue(label: Const.dqueue + ".img_load", qos: .utility, attributes: .concurrent)
    }

    func getLocal(name: String) -> String {
        return name.replacingOccurrences(of: "/", with: "A")
    }

    func localLoad(image: String) -> UIImage? {
        let filename = getLocal(name: image)
        return filer.load(image: filename)
    }

    func loadedImageNotification() -> String {
        return imageLoadedNotification
    }

    func load(image: String, completion: @escaping ((UIImage) -> Void)) {
        let filename = getLocal(name: image)
        queue.async { [unowned self] in
            if let img = self.filer.load(image: filename) {
                DispatchQueue.main.async {
                    completion(img)
                }
            } else {
                self.server.getRawQuery(url: "/\(image)", success: { data in
                    // save the image into the local filesystem
                    if let img = UIImage(data: data) {
                        self.filer.save(.image, name: filename, with: data)
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: self.imageLoadedNotification),
                                                            object: img, userInfo: ["name": image])
                            completion(img)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            completion(self.noImage())
                        }
                    }
                    }, failure: { [unowned self] error in
                        DDLogError("got an error while trying to download an image: \(error.localizedDescription)")
                        DispatchQueue.main.async {
                            completion(self.noImage())
                        }
                })
            }
        }
    }

    func noImage() -> UIImage {
        return noImg
    }
}
