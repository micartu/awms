//
//  Scanner+Debug.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension Scanner {

    func loadDebugStuff() {
        guard let plistPath = Bundle.main.path(forResource: "load", ofType: "plist") else {
            return
        }
        let dict = NSDictionary(contentsOfFile: plistPath)!
        if let loadDebug = dict["scanner_dbg_srv_load"] as? Bool {
            if loadDebug {
                if let p = dict["scanner_dbg_srv_port"] as? Int {
                    loadDebugServer(port: p)
                }
            } else {
                print("Debug server won't be loaded!")
            }
        }
    }

    func loadDebugServer(port: Int) {
        srv = BRMServerDbg(debugServerWithPort:port)
        srv?.setBlockToExecute({ (param: String?) in
            let p = param!.split(separator: " ")
            if p.count > 1 {
                let type = p[0]
                let data = String(p[1])
                var stype: ScanItemTypes
                if type == "EAN13" {
                    stype = .EAN13
                } else if type == "QR" {
                    stype = .QR
                } else {
                    stype = .EAN13
                }
                for d in self.delegates {
                    d.itemScanned(type: stype, data: data)
                }
            }
        })
    }
}
