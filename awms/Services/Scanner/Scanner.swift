//
//  Scanner.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import CocoaLumberjack

public class Scanner: ScannerProtocol {
    var delegate: ScannerDelegate?
    var srv: BRMServerDbg?
    var dtccover = DTCScanManager(settingsPrefix: "awmsApp_")

    init() {
        loadDebugStuff()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(barCodeScanned(barcode:)),
                                       name: NSNotification.Name.ScanDeviceManagerScannedBarcode,
                                       object: nil)
        notificationCenter.post(name: NSNotification.Name(rawValue: "connectScanDevice"), object: nil)
    }

    func bindTo(delegate: ScannerDelegate) {
        delegates.append(delegate)
    }

    func unbind(delegate: ScannerDelegate) {
        delegates.removeElementByReference(delegate)
    }

    // MARK: - Private

    @objc private func barCodeScanned(barcode: Notification) {
        if let info = barcode.userInfo {
            let type = info[ScanDeviceManagerUserInfoBarcodeIsoTypeKey] as! String
            let data = info[ScanDeviceManagerUserInfoBarcodeDataKey] as! String
            DDLogDebug("user scanned an item of type: \(type) with data: \(data)")
            var stype: ScanItemTypes
            if type == "EAN13" {
                stype = .EAN13
            } else if type == "QRCode" {
                stype = .QR
            } else {
                stype = .EAN13
            }
            if delegates.count > 0 {
                for d in delegates {
                    d.itemScanned(type: stype, data: data)
                }
            } else {
                delegate?.itemScanned(type: stype, data: data)
            }
        }
    }

    internal var delegates = [ScannerDelegate]()

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
