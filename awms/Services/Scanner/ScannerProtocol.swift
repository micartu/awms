//
//  ScannerProtocol.swift
//  awms
//
//  Created by Michael Artuerhof on 12.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

enum ScanItemTypes {
    case EAN13
    case QR
}

protocol ScannerDelegate: class {
    func itemScanned(type: ScanItemTypes, data: String)
}

protocol ScannerProtocol: class {
    func bindTo(delegate: ScannerDelegate)
    func unbind(delegate: ScannerDelegate)
}
