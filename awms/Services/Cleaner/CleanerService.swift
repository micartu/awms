//
//  CleanerService.swift
//  awms
//
//  Created by Michael Artuerhof on 29.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol CleanerService {
    func cleanTasks(completion: @escaping () -> Void)
}
