//
//  Cleaner.swift
//  awms
//
//  Created by Michael Artuerhof on 29.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Cleaner: CleanerService {
    let storage: StorageService
    let disk: FileService
    let queue: DispatchQueue

    init(storage: StorageService, disk: FileService) {
        self.storage = storage
        self.disk = disk
        self.queue = DispatchQueue(label: Const.dqueue + ".cleaning", qos: .userInitiated, attributes: .concurrent)
    }

    func cleanTasks(completion: @escaping () -> Void) {
        queue.async { [unowned self] in
            // remove images in attachments first
            if let attachments = self.storage.getAttachments() {
                for a in attachments {
                    if a.type == .image {
                        self.disk.remove(image: a.img)
                    }
                }
            }
            self.storage.deleteCurrentUser {
                DispatchQueue.main.async {
                    completion()
                }
            }
        }
    }
}
