//
//  PrinterService.swift
//  awms
//
//  Created by michael on 29.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol PrinterService: class {
    var isConnected: Bool { get }
    func set(address: String)
    func connectToPrinter() -> Bool
    func print(qr: String, with completion: @escaping ((Bool, NSError?) -> Void))
}
