//
//  Printer.swift
//  awms
//
//  Created by michael on 29.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import ExternalAccessory
import CocoaLumberjack

class Printer: NSObject, PrinterService {
    let queue: DispatchQueue
    let printer: ARSFPrinter
    var tasker: ARSTasker!
    var lastCommandTaskId: Int32 = -1
    var isBusy = false
    var isConnected = false
    var deviceFound = false
    var accessory: EAAccessory? = nil
    var defaultCompletion: ((Bool, NSError?) -> Void)? = nil
    var errTimer: Timer? = nil
    let kTimeout4operation: TimeInterval = 10

    override init() {
        self.queue = DispatchQueue(label: Const.dqueue + ".arsprint", qos: .utility) // serial
        self.printer = ARSFPrinter()
        self.tasker = ARSTasker(printer: self.printer)
        super.init()
        printer.delegate = self
        if let name = UserDefaults.standard.value(forKey: Const.kSelectedPrinterName) as? String {
            set(address: name)
        }
    }

    func set(address: String) {
        deviceFound = false
        let connected = EAAccessoryManager.shared().connectedAccessories
        for a in connected {
            if address == a.name {
                if a.protocolStrings.index(of: Const.kPrinterProtocol) != NSNotFound {
                    deviceFound = true
                    accessory = a
                    printer.connect(toPrinter: accessory!)
                    tasker = ARSTasker(printer: printer)
                    tasker.delegate = self
                } else {
                    DDLogError("Couldn't connect to printer: \(a.name) isn't an ARS Printer!")
                }
                break
            }
        }
    }

    func connectToPrinter() -> Bool {
        if accessory == nil {
            if let name = UserDefaults.standard.value(forKey: Const.kSelectedPrinterName) as? String {
                set(address: name)
                if deviceFound {
                    return true
                }
            }
        }
        return false
    }

    func stopTimer() {
        if let timer = errTimer, timer.isValid {
            timer.invalidate()
            errTimer = nil
        }
    }

    func startTimer() {
        stopTimer()
        errTimer = Timer(timeInterval: kTimeout4operation,
                         target: self,
                         selector: #selector(timeoutForOperation),
                         userInfo: nil,
                         repeats: false)
    }

    @objc func timeoutForOperation() {
        if isBusy {
            isBusy = false
            isConnected = false
            if let c = defaultCompletion {
                let er = NSError(domain: Const.domain,
                                 code: Const.kErrPrint,
                                 userInfo: [NSLocalizedDescriptionKey: "Connection to printer lost".localized])
                DispatchQueue.main.async {
                    c(false, er)
                }
                defaultCompletion = nil
            }
        }
    }

    func errDescription(for code: Int) -> String {
        switch Int32(code) {
        case ERR_DEVICE_DAY_IS_CLOSED:
            return "Device day is closed".localized
        default:
            return "Unknown code: \(code)"
        }
    }

    func checkConnectionState() -> NSError? {
        if !isConnected {
            if !connectToPrinter() {
                return NSError(domain: Const.domain,
                               code: Const.kErrPrint,
                               userInfo: [NSLocalizedDescriptionKey: "There is no connection to printer".localized])
            }
        }
        if isBusy {
            return NSError(domain: Const.domain,
                           code: Const.kErrPrint,
                           userInfo: [NSLocalizedDescriptionKey: "Please wait till the end of the current operation".localized])
        }
        return nil
    }

    func canRunNextCommandWith(completion: ((Bool, NSError?) -> Void)) -> Bool {
        if let er = checkConnectionState() {
            completion(false, er)
            return false
        }
        isBusy = true
        startTimer()
        return true
    }

    func print(qr: String, with completion: @escaping ((Bool, NSError?) -> Void)) {
        if !canRunNextCommandWith(completion: completion) {
            return
        }
        queue.async { [unowned self] in
            let id = self.tasker.print(qr, orEAN8: "", orEAN13: "", orEAN128: "")
            self.lastCommandTaskId = Int32(id)
            self.defaultCompletion = completion
        }
    }
}

extension Printer: ARSFPrinterProtocol {
    func statusChanged(_ status: String!) {
        DDLogInfo("printer's status changed to: \(status)")
    }

    func connected() {
        isConnected = true
    }

    func disconnected() {
        isConnected = false
    }
}

extension Printer: ARSTaskerProtocol {
    func printerAnswered(_ str: [Any]!, forSeq seq: Int32) {
        if seq == lastCommandTaskId {
            stopTimer()
            if str.count > 0 {
                let ret = str[0] as! String
                let errCode = ret.toInt
                isBusy = false
                if errCode == 0 {
                    if let c = defaultCompletion {
                        DispatchQueue.main.async {
                            c(true, nil)
                        }
                    }
                } else {
                    if let c = defaultCompletion {
                        let er = NSError(domain: Const.domain,
                                         code: Const.kErrPrint,
                                         userInfo: [NSLocalizedDescriptionKey: errDescription(for: errCode)])
                        DispatchQueue.main.async {
                            c(false, er)
                        }
                        defaultCompletion = nil
                    }
                }
            }
        }
    }
}
