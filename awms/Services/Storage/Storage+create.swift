//
//  Storage+create.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage {
    // MARK: - public

    /// creates user in cache based on its ponso representation
    func create(user: User, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            let u = CUser.mr_createEntity(in: context)!
            u.login = user.login
            u.name = user.name
            u.avatar = user.avatar
            if let caps = user.capabilities {
                for c in caps {
                    let cap = self.create(capability: c, in: context)
                    cap.user = u

                    u.addToCapabilities(cap)
                }
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    /// creates tasks in cache based on theri ponso representation and
    /// attaches them to the given user (if he exists)
    func create(tasks: [Task], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            var u: CUser?
            if let tmp = self.getUser(self.login, in: context) {
                u = tmp
            } else { // create a user please
                let usr = User(login: self.login, name: "", avatar: "", capabilities: nil)
                if let tmp = self.create(user: usr, in: context) {
                    u = tmp
                }
                else { // creation wasn't successful
                    u = nil
                }
            }
            if let usr = u {
                self.attach(tasks: tasks, to: usr, in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func create(attachment: Attachment, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            if let u = self?.getUser(self?.login ?? "", in: context) {
                let ca = CAttachment.mr_createEntity(in: context)!

                ca.id = self?.attach_id ?? 0
                self?.attach_id += 1
                ca.user = u
                ca.date = Date()
                u.addToAttachments(ca)

                self?.update(attachment: ca, with: attachment)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    /// puts an item into user's bag beforehand we should understand that it's possible
    /// e.g. we've got enough space for it
    func putIntoBag(item: Item, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            if let u = self.getUser(self.login, in: context) {
                var found = false
                if let itms = u.items as? Set<CItem> {
                    for i in itms  {
                        if (i.id == item.id || i.ean == item.ean) {
                            // same item, simply add its volumes
                            let diff = Int32(item.scanned) - i.scanned
                            i.scanned += diff
                            u.usedM += Double(diff) * i.m
                            u.usedV += Double(diff) * i.s * i.h
                            found = true
                            break
                        }
                    }
                }
                if !found {
                    if let i = self.get(item: item, in: context) {
                        u.usedV += item.s * item.h * Double(item.scanned)
                        u.usedM += item.m * Double(item.scanned)
                        u.addToItems(i)
                        i.scanned = Int32(item.scanned)

                        i.user = u
                    }
                }
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - internal

    func create(user: User, in context: NSManagedObjectContext) -> CUser? {
        guard let u = CUser.mr_createEntity(in: context) else {
            return nil
        }
        u.login = user.login
        u.name = user.name
        u.avatar = user.avatar
        if let caps = user.capabilities {
            for c in caps {
                let cap = self.create(capability: c, in: context)
                cap.user = u

                u.addToCapabilities(cap)
            }
        }
        return u
    }

    internal func attach(tasks: [Task], to user: CUser, in context: NSManagedObjectContext) {
        for t in tasks {
            let tt = self.create(task: t, in: context)
            tt.user = user
            user.addToTasks(tt)
        }
    }

    internal func create(task: Task, in context: NSManagedObjectContext) -> CTask {
        let t = CTask.mr_createEntity(in: context)!
        if task.id == 0 {
            t.id = task_id
            task_id += 1
        } else {
            t.id = Int32(task.id)
        }
        t.startNode = Int64(task.startNode)
        if let ts = task as? TaskScan {
            if task is TaskPrint {
                t.type = Int16(TaskType.printLabels.rawValue)
            } else {
                t.type = Int16(TaskType.scan.rawValue)
            }
            create(items: ts.items, in: t, with: context)
        } else if let td = task as? TaskDelivery {
            t.type = Int16(TaskType.delivery.rawValue)
            t.map = td.map
        } else if let td = task as? TaskTruckDelivery {
            t.type = Int16(TaskType.truckDelivery.rawValue)
            for pt in td.pointsCoord {
                if t.coords.count > 0 {
                    t.coords += ","
                }
                t.coords += "\(pt.x),\(pt.y)"
            }
            for d in td.pointsDescr {
                if t.points.count > 0 {
                    t.points += "|"
                }
                t.points += "\(d)"
            }
            t.descr = td.descr
        }
        return t
    }

    internal func create(graph: Graph, with name: String, in context: NSManagedObjectContext) -> CGraph {
        let g = CGraph.mr_createEntity(in: context)!
        g.v = Int32(graph.V)
        g.name = name
        for e in graph.edges() {
            let edg = CEdge.mr_createEntity(in: context)!
            edg.from = Int32(e.From())
            edg.to = Int32(e.To())
            edg.weight = e.Weight()

            edg.graph = g
            g.addToEdges(edg)
        }
        return g
    }

    internal func create(nodes: [MapNode], for graph: String, with context: NSManagedObjectContext) {
        for n in nodes {
            let cn = CNode.mr_createEntity(in: context)!
            cn.map = graph
            cn.id = Int32(n.Id)
            cn.x = n.x
            cn.y = n.y
        }
    }

    internal func create(items: [Item]?, in task: CTask, with context: NSManagedObjectContext) {
        if let items = items {
            for i in items {
                let item = CItem.mr_createEntity(in: context)!
                item.id = item_id
                item_id += 1
                update(item: item, with: i)
                item.task = task
                task.addToItems(item)
            }
        }
    }

    internal func create(capability: Capability, in context: NSManagedObjectContext) -> CCapability {
        let cap = CCapability.mr_createEntity(in: context)!
        cap.id = Int32(capability.id)
        cap.s = capability.s
        cap.h = capability.h
        cap.mass = capability.m
        return cap
    }
}
