//
//  Storage+update.swift
//  awms
//
//  Created by Michael Artuerhof on 02.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage {

    // MARK: - public

    func update(task: Task, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            if let t = self.get(task: task, in: context) {
                t.type = Int16(task.type.rawValue)
                t.state = Int16(task.state.rawValue)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func update(tasks: [Task], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            if let u = self.getUser(self.login, in: context) {
                if let t = u.tasks as? Set<CTask> {
                    // remove all tasks first
                    let delete = NSSet(set: t)
                    u.removeFromTasks(delete)
                    for tt in t {
                        self.delete(task: tt, in: context)
                    }
                }
            }
        }, completion: { (success, err) in
            // then add the new ones:
            self.create(tasks: tasks, completion: completion)
        })
    }

    func update(coords: [MapNode], for graph: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            self?.deleteNodes(for: graph, in: context)
            self?.create(nodes: coords, for: graph, with: context)
        }, completion: { (success, err) in
            completion()
        })
    }

    func update(item: Item, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            if let i = self?.get(item: item, in: context) {
                self?.update(item: i, with: item)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func update(attachment a: Attachment, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            let predicate = NSPredicate(format: "(id == %d) AND (user == %@)", a.id, self?.login ?? "")
            if let ca = CAttachment.mr_findFirst(with: predicate, in: context) {
                self?.update(attachment: ca, with: a)
            } else {
                self?.create(attachment: a, completion: completion)
            }
            }, completion: { (success, err) in
                completion()
        })
    }

    internal func update(attachment ca: CAttachment, with a: Attachment) {
        ca.type = Int16(a.type.rawValue)
        ca.imgPath = a.img
        ca.msg = a.msg
        ca.date = a.date
    }

    internal func update(item i: CItem, with item: Item) {
        i.amount = item.amount
        i.qr = item.qr
        i.s = item.s
        i.m = item.m
        i.h = item.h
        i.img = item.imgName
        i.title = item.title
        i.descr = item.descr
        i.ean = Int64(item.ean)
        i.scanned = Int32(item.scanned)
        i.storedIn = Int64(item.storedIn)
        i.status = Int16(item.status.rawValue)
    }

    func update(user: User, completion: @escaping (() -> Void)) {
        if exists(user: user.login) {
            MagicalRecord.save({ (context) in
                let u = self.getUser(user.login, in: context)!
                u.name = user.name
                u.avatar = user.avatar
                if let caps = user.capabilities {
                    if let ccaps = u.capabilities as? Set<CCapability> {
                        var to_update = [Int]()
                        var to_create = [Int]()
                        for c in caps {
                            var found = false
                            for cc in ccaps {
                                if c.id == cc.id {
                                    found = true
                                    // update element
                                    cc.mass = c.m
                                    cc.h = c.h
                                    cc.s = c.s
                                    to_update.append(Int(c.id))
                                    break
                                }
                            }
                            if !found { // create new capability
                                to_create.append(Int(c.id))
                                let cap = self.create(capability: c, in: context)
                                cap.user = u
                            }
                        }
                        for cc in ccaps {
                            if !to_update.contains(Int(cc.id)) && !to_create.contains(Int(cc.id)) {
                                u.removeFromCapabilities(cc)
                                cc.mr_deleteEntity()
                            }
                        }
                    }
                }
            }, completion: { (success, err) in
                completion()
            })
        } else {
            create(user: user, completion: completion)
        }
    }

    func update(graph: Graph, with name: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            if self!.exists(graph: name) {
                self?.delete(graph: name) {
                    let _ = self?.create(graph: graph, with: name, in: context)
                }
            } else {
                let _ = self?.create(graph: graph, with: name, in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }
}
