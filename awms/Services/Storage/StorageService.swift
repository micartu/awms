//
//  StorageService.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

protocol StorageService: class {
    // reinit
    func reload(for user: String)

    // creating
    func create(user: User, completion: @escaping (() -> Void))
    func create(tasks: [Task], completion: @escaping (() -> Void))
    func create(attachment: Attachment, completion: @escaping (() -> Void))
    func putIntoBag(item: Item, completion: @escaping (() -> Void))

    // updating
    func update(user: User, completion: @escaping (() -> Void))
    func update(task: Task, completion: @escaping (() -> Void))
    func update(tasks: [Task], completion: @escaping (() -> Void))
    func update(item: Item, completion: @escaping (() -> Void))
    func update(graph: Graph, with name: String, completion: @escaping (() -> Void))
    func update(coords: [MapNode], for graph: String, completion: @escaping (() -> Void))
    func update(attachment a: Attachment, completion: @escaping (() -> Void))

    // getting
    func getTasksForCurrentUser() -> [Task]?
    func countLoadedTasksFor(login: String) -> UInt
    func getTasksFor(user: String) -> [Task]?
    func get(task id: Int) -> Task?
    func getBagItemsFor(user: String) -> [Item]?
    func getBagItems() -> [Item]?
    func get(user: String) -> User?
    func get(graph: String) -> Graph?
    func getItemBy(ean: Int) -> Item?
    func getCoordForMap(name: String) -> [MapNode]?
    func getAttachmentFor(user: String, of type: AttachType) -> [Attachment]?
    func getAttachments(of type: AttachType) -> [Attachment]?
    func getAttachments() -> [Attachment]?

    // deletion
    func delete(user: String, completion: @escaping (() -> Void))
    func delete(graph: String, completion: @escaping (() -> Void))
    func deleteFromBox(item: Item, completion: @escaping (() -> Void))
    func delete(attachment a: Attachment, completion: @escaping (() -> Void))
    func deleteCurrentUser(completion: @escaping (() -> Void))

    // small queries
    func exists(user: String) -> Bool
    func canPutIntoBag(item: Item) -> Bool
}
