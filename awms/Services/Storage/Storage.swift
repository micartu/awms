//
//  Storage.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import MagicalRecord

class Storage: StorageService {
    internal var login: String

    // MARK: - initialization
    init(login: String) {
        self.login = login
        reload(for: login)
    }

    func reload(for user: String) {
        self.login = user
        // restore ids
        let defaults = UserDefaults.standard
        task_id = Int32(defaults.integer(forKey: Const.kKeyTaskId))
        item_id = Int64(defaults.integer(forKey: Const.kKeyItemId))
        attach_id = Int32(defaults.integer(forKey: Const.kKeyAttachId))
    }

    // MARK: - small queries

    func countLoadedTasksFor(login: String) -> UInt {
        let predicate = NSPredicate(format: "user.login == %@", login)
        return CTask.mr_countOfEntities(with: predicate)
    }

    func exists(user: String) -> Bool {
        let predicate = NSPredicate(format: "login == %@", login)
        if CUser.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    func canPutIntoBag(item: Item) -> Bool {
        if let u = getUser(login) {
            var availVol = 0.0
            var usedMass = 0.0
            if let cap = u.capabilities as? Set<CCapability> {
                // actually we should place it into another variables and do not calculate it everytime
                // but capabilites should be small like max 3 elements so it shouldn't take much time
                for c in cap {
                    availVol += c.h * c.s
                    usedMass += c.mass
                }
            }
            // are there any room available?
            if Double(item.scanned) * item.s * item.h + u.usedV <= availVol {
                return true
            }
        }
        return false
    }

    // MARK: - cleaning

    internal func saveIds() {
        let defaults = UserDefaults.standard
        defaults.set(NSNumber(integerLiteral:Int(task_id)), forKey: Const.kKeyTaskId)
        defaults.set(NSNumber(integerLiteral:Int(item_id)), forKey: Const.kKeyItemId)
        defaults.set(NSNumber(integerLiteral:Int(attach_id)), forKey: Const.kKeyAttachId)
    }

    deinit {
        saveIds()
    }

    // MARK: - Internal

    func exists(graph: String) -> Bool {
        let predicate = NSPredicate(format: "name == %@", graph)
        if CGraph.mr_countOfEntities(with: predicate) > 0 {
            return true
        }
        return false
    }

    internal var task_id: Int32 = 1
    internal var item_id: Int64 = 1
    internal var attach_id: Int32 = 1
}
