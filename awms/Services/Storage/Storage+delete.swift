//
//  Storage+delete.swift
//  awms
//
//  Created by michael on 02.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage {
    // MARK: - public

    func deleteCurrentUser(completion: @escaping (() -> Void)) {
        delete(user: self.login, completion: completion)
    }

    func delete(user: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            if let u = self.getUser(user) {
                if let ts = u.tasks as? Set<CTask> {
                    for t in ts {
                        u.removeFromTasks(t)
                        self.delete(task: t, in: context)
                    }
                }
                if let cps = u.capabilities as? Set<CCapability> {
                    for c in cps {
                        u.removeFromCapabilities(c)
                        c.mr_deleteEntity(in: context)
                    }
                }
                // box items
                if let items = u.items as? Set<CItem>  {
                    for i in items {
                        u.removeFromItems(i)
                        i.mr_deleteEntity(in: context)
                    }
                }
                // attachments
                if let attaches = u.attachments as? Set<CAttachment>  {
                    for a in attaches {
                        u.removeFromAttachments(a)
                        a.mr_deleteEntity(in: context)
                    }
                }
                u.mr_deleteEntity(in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func deleteFromBox(item: Item, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            if let u = self?.getUser(self?.login ?? "") {
                guard let items = u.items as? Set<CItem> else { return }
                for i in items {
                    if i.ean == item.ean {
                        u.usedM -= i.m
                        u.usedV -= i.h * i.s * Double(i.scanned)
                        u.removeFromItems(i)
                        break
                    }
                }
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    func delete(graph: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ (context) in
            if let g = self.getGraph(graph, in: context) {
                if let edgs = g.edges as? Set<CEdge> {
                    for e in edgs {
                        g.removeFromEdges(e)
                        e.mr_deleteEntity(in: context)
                    }
                }
                g.mr_deleteEntity(in: context)
            }
            self.deleteNodes(for: graph, in: context)
        }, completion: { (success, err) in
            completion()
        })
    }

    func delete(attachment a: Attachment, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] (context) in
            let predicate = NSPredicate(format: "(id == %d) AND (user == %@)", a.id, self?.login ?? "")
            if let ca = CAttachment.mr_findFirst(with: predicate, in: context) {
                if let u = ca.user {
                    u.removeFromAttachments(ca)
                    ca.user = nil
                }
                ca.mr_deleteEntity(in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - internal

    internal func deleteNodes(for graph: String, in context: NSManagedObjectContext) {
        if let nodes = self.getInnerCoordForMap(name: graph) {
            for n in nodes {
                n.mr_deleteEntity(in: context)
            }
        }
    }

    internal func delete(task: CTask, in context: NSManagedObjectContext) {
        if let items = task.items as? Set<CItem> {
            for i in items {
                i.mr_deleteEntity(in: context)
            }
        }
        task.mr_deleteEntity(in: context)
    }
}
