//
//  Attachment.swift
//  awms
//
//  Created by Michael Artuerhof on 27.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

enum AttachType: Int, Codable {
    case image = 1
    case msg
}

struct Attachment: Codable {
    let type: AttachType
    let id: Int
    let img: String
    let msg: String
    let date: Date
}

func createMsgAttachment(msg: String) -> Attachment {
    return Attachment(type: .msg, id: 0, img: "", msg: msg, date: Date())
}

func createImageAttachment(name: String, msg: String) -> Attachment {
    return Attachment(type: .image, id: 0, img: name, msg: msg, date: Date())
}
