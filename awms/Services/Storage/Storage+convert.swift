//
//  Storage+convert.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension Storage {
    internal func convert(capability: CCapability) -> Capability {
        let c = Capability(id: UInt(capability.id),
                           s: capability.s,
                           h: capability.h,
                           m: capability.mass)
        return c
    }

    internal func convert(user: CUser) -> User {
        var cap2u: [Capability]?
        if let caps = user.capabilities {
            cap2u = [Capability]()
            for c in caps {
                if let c = c as? CCapability {
                    cap2u!.append(self.convert(capability: c))
                }
            }
        } else {
            cap2u = nil
        }
        let u = User(login: user.login!,
                     name: user.name!,
                     avatar: user.avatar!,
                     capabilities: cap2u)
        return u
    }

    internal func convert(coord: CNode) -> MapNode {
        return MapNode(Id: Int(coord.id),
                        x: coord.x,
                        y: coord.y)
    }

    internal func convert(graph: CGraph) -> Graph {
        let g = Graph(V: Int(graph.v))
        if let edges = graph.edges as? Set<CEdge> {
            for edg in edges {
                let e = convert(edge: edg)
                g.addEdgeDS(e)
            }
        }
        return g
    }

    internal func convert(edge: CEdge) -> Edge {
        let e = Edge(v: Int(edge.from), w: Int(edge.to), weight: edge.weight)
        return e
    }

    internal func updateCommon(task ttask: Task, coredata task: CTask) {
        let state = TaskState(rawValue: Int(task.state))
        ttask.id = Int(task.id)
        if let s = state {
            ttask.state = s
        }
    }

    internal func convert(task: CTask) -> Task? {
        var out: Task? = nil
        if let t = TaskType(rawValue: Int(task.type)) {
            switch t {
            case .printLabels:
                fallthrough
            case .scan:
                var itms: [Item]? = nil
                if let items = task.items {
                    itms = convert(items: items)
                }
                var ttask: TaskScan
                if t == .scan {
                    ttask = TaskScan(items: itms)
                } else {
                    ttask = TaskPrint(items: itms)
                }
                out = ttask
            case .delivery:
                let ttask = TaskDelivery(map: task.map,
                                         startNode: Int(task.startNode))
                out = ttask
            case .truckDelivery:
                let ptDescr = task.points
                let cs = task.coords.split(separator: ",")
                var coords = [String]()
                for i in stride(from: 0, to: cs.count - 1, by: 2) {
                    let x = String(cs[i]).toDouble
                    let y = String(cs[i + 1]).toDouble
                    coords.append("\(x),\(y)")
                }
                let ds = ptDescr.split(separator: "|")
                var dds = [String]()
                for d in ds {
                    dds.append(String(d))
                }
                if let ttask = TaskTruckDelivery(pointDescr: dds,
                                                 pointCoord: coords,
                                                 descr: task.descr) {
                    out = ttask
                } else {
                    out = Task(type: .unknown)
                }
            default:
                let ttask = Task(type: t)
                out = ttask
            }
            if let o = out {
                updateCommon(task: o, coredata: task)
                return o
            }
        }
        return nil
    }

    internal func convert(items: NSSet) -> [Item] {
        var itms = [Item]()
        let descr = NSSortDescriptor(key: "ean", ascending: true)
        let sortedItems = items.sortedArray(using: [descr])
        for i in sortedItems {
            if let ic = convert(item: i as! CItem) {
                itms.append(ic)
            }
        }
        return itms
    }

    internal func convert(item: CItem) -> Item? {
        let state = ItemState(rawValue: Int(item.status))
        let imgLoader = ServicesAssembler.shared().imgLoader
        let img = imgLoader.localLoad(image: item.img!) ?? imgLoader.noImage()
        let i = Item(id: Int(item.id),
                     qr: item.qr!,
                     amount: Double(item.amount),
                     ean: Int(item.ean),
                     status: state ?? .unknown,
                     scanned: Int(item.scanned),
                     storedIn: Int(item.storedIn),
                     title: item.title!,
                     descr: item.descr!,
                     imgName: item.img!,
                     img: img,
                     s: item.s,
                     h: item.h,
                     m: item.m)
        return i
    }

    internal func convert(attachment ca: CAttachment) -> Attachment? {
        let type = AttachType(rawValue: Int(ca.type))
        let a = Attachment(type: type ?? .msg,
                           id: Int(ca.id),
                           img: ca.imgPath ?? "",
                           msg: ca.msg ?? "",
                           date: ca.date ?? Date())
        return a
    }
}
