//
//  Storage+get.swift
//  awms
//
//  Created by Michael Artuerhof on 01.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import MagicalRecord

extension Storage {
    // MARK: - public

    func getTasksForCurrentUser() -> [Task]? {
        return getTasksFor(user: login)
    }

    func getTasksFor(user: String) -> [Task]? {
        let predicate = NSPredicate(format: "user.login == %@", user)
        if let tasks = CTask.mr_findAllSorted(by: "id", ascending: true, with: predicate) {
            var ttasks = [Task]()
            for t in tasks {
                if let tt = convert(task: t as! CTask) {
                    ttasks.append(tt)
                }
            }
            return ttasks
        }
        return nil
    }

    func get(task id: Int) -> Task? {
        let predicate = NSPredicate(format: "(user.login == %@) AND (id == %d)", login, id)
        if let task = CTask.mr_findFirst(with: predicate) {
            return convert(task: task)
        }
        return nil
    }

    func getAttachments() -> [Attachment]? {
        let predicate = NSPredicate(format: "(user == %@)", self.login)
        return getAttachmentsWith(predicate)
    }

    func getAttachments(of type: AttachType) -> [Attachment]? {
        return getAttachmentFor(user: self.login, of: type)
    }

    func getAttachmentFor(user: String, of type: AttachType) -> [Attachment]? {
        let predicate = NSPredicate(format: "(type == %d) AND (user == %@)", type.rawValue, self.login)
        return getAttachmentsWith(predicate)
    }

    private func getAttachmentsWith(_ predicate: NSPredicate) -> [Attachment]? {
        if let attaches = CAttachment.mr_findAllSorted(by: "id", ascending: true, with: predicate) as? [CAttachment] {
            var att = [Attachment]()
            for a in attaches {
                if let at = convert(attachment: a) {
                    att.append(at)
                }
            }
            return att
        }
        return nil
    }

    func get(user: String) -> User? {
        if let u = getUser(user) {
            return convert(user: u)
        }
        return nil
    }

    func getItemBy(ean: Int) -> Item? {
        let predicate = NSPredicate(format: "ean == %i", ean)
        if let i = CItem.mr_findFirst(with: predicate) {
            return convert(item: i as CItem)
        }
        return nil
    }

    func get(graph: String) -> Graph? {
        if let g = getGraph(graph, in: NSManagedObjectContext.mr_default()) {
            return convert(graph: g)
        }
        return nil
    }

    func getBagItemsFor(user: String) -> [Item]? {
        let predicate = NSPredicate(format: "user.login == %@", user)
        if let items = CItem.mr_findAllSorted(by: "ean", ascending: true, with: predicate) {
            var out = [Item]()
            for i in items {
                if let item = convert(item: i as! CItem) {
                    out.append(item)
                }
            }
            return out
        }
        return nil
    }

    func getCoordForMap(name: String) -> [MapNode]? {
        if let nodes = getInnerCoordForMap(name: name) {
            var out = [MapNode]()
            for n in nodes {
                let nn = convert(coord: n)
                out.append(nn)
            }
            return out
        }
        return nil
    }

    func getBagItems() -> [Item]? {
        return getBagItemsFor(user: self.login)
    }

    // MARK: - internal

    func getInnerCoordForMap(name: String) -> [CNode]? {
        let predicate = NSPredicate(format: "map == %@", name)
        if let nodes = CNode.mr_findAllSorted(by: "id", ascending: true, with: predicate) {
            if nodes.count > 0 {
                return nodes as? [CNode]
            }
        }
        return nil
    }

    internal func get(item: Item, in context: NSManagedObjectContext) -> CItem? {
        let predicate = NSPredicate(format: "(task.user.login == %@) AND (ean == %i)", login, item.ean)
        return CItem.mr_findFirst(with: predicate, in: context)
    }

    internal func get(task: Task, in context: NSManagedObjectContext) -> CTask? {
        let predicate = NSPredicate(format: "(user.login == %@) AND (id == %i)", login, task.id)
        return CTask.mr_findFirst(with: predicate, in: context)
    }

    internal func getUser(_ user: String, in context: NSManagedObjectContext) -> CUser? {
        let predicate = NSPredicate(format: "login == %@", login)
        return CUser.mr_findFirst(with: predicate, in: context)
    }

    internal func getUser(_ user: String) -> CUser? {
        return getUser(user, in: NSManagedObjectContext.mr_default())
    }

    internal func getGraph(_ graph: String, in context: NSManagedObjectContext) -> CGraph? {
        let predicate = NSPredicate(format: "name == %@", graph)
        return CGraph.mr_findFirst(with: predicate, in: context)
    }
}
