//
//  Uploader.swift
//  awms
//
//  Created by Michael Artuerhof on 28.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Uploader: UploaderService {
    let storage: StorageService
    let server: NetworkProtocol
    let disk: FileService
    let queue: DispatchQueue

    init(storage: StorageService, server: NetworkProtocol, disk: FileService) {
        self.storage = storage
        self.server = server
        self.disk = disk
        self.queue = DispatchQueue(label: Const.dqueue + ".upload", qos: .userInitiated, attributes: .concurrent)
    }

    func uploadTasks(completion: @escaping (NSError?) -> Void) {
        queue.async { [unowned self] in
            if let tasks = self.storage.getTasksForCurrentUser() {
                let data = try! JSONEncoder().encode(tasks)
                self.server.postQuery(url: "/report_tasks", data: data, success: { (res: [NSDictionary]) in
                    completion(nil)
                }, failure: { (err) in
                    completion(err)
                } )
            }
            else {
                let err = NSError(domain: Const.domain,
                                 code: Const.kErrUpl,
                                 userInfo: [NSLocalizedDescriptionKey: "No tasks found".localized])
                completion(err)
            }
        }
    }

    func uploadAttachments(completion: @escaping (NSError?) -> Void) {
        queue.async { [unowned self] in
            if let attachments = self.storage.getAttachments() {
                let data = try! JSONEncoder().encode(attachments)
                self.server.postQuery(url: "/attachments", data: data, success: { (res: [NSDictionary]) in
                    completion(nil)
                }, failure: { (err) in
                    completion(err)
                })
            }
            else {
                completion(nil)
            }
        }
    }

    func uploadImagesInAttachments(completion: @escaping (NSError?) -> Void) {
        queue.async { [unowned self] in
            let group = DispatchGroup()
            let lock = NSLock()
            var anyErr: NSError? = nil
            if let attachments = self.storage.getAttachments() {
                for a in attachments {
                    if a.type == .image {
                        if let d = self.disk.contentsOf(image: a.img) {
                            group.enter()
                            self.server.uploadPNG(url: "/upload", data: d, name: a.img) { err in
                                if let e = err {
                                    lock.lock()
                                    anyErr = e
                                    lock.unlock()
                                }
                                group.leave()
                            }
                        }
                    }
                }
            }
            group.notify(queue: self.queue) {
                completion(anyErr)
            }
        }
    }

    func uploadData(completion: @escaping (NSError?) -> Void) {
        let group = DispatchGroup()
        var errTasks: NSError? = nil
        var errAttachment: NSError? = nil
        var errImgsInAttachment: NSError? = nil
        group.enter()
        uploadTasks { (err) in
            errTasks = err
            group.leave()
        }
        group.enter()
        uploadAttachments { (err) in
            errAttachment = err
            group.leave()
        }
        group.enter()
        uploadImagesInAttachments { (err) in
            errImgsInAttachment = err
            group.leave()
        }
        group.notify(queue: queue) {
            var resErr: NSError? = nil
            if let err = errTasks {
                resErr = err
            } else if let err = errAttachment {
                resErr = err
            } else if let err = errImgsInAttachment {
                resErr = err
            }
            DispatchQueue.main.async {
                completion(resErr)
            }
        }
    }
}
