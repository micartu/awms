//
//  UploaderService.swift
//  awms
//
//  Created by Michael Artuerhof on 28.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol UploaderService: class {
    func uploadData(completion: @escaping (NSError?) -> Void)
}
