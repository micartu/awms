//
//  MapLoader+Parse.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension MapLoader {
    internal func parseGraph(_ res: NSDictionary) -> Graph? {
        if let v = res["v"] as? Int {
            let g = Graph(V: v)
            if let edg = res["vertices"] as? [NSDictionary] {
                for e in edg {
                    let edge = parseEdge(e)
                    g.addEdgeDS(edge)
                }
            }
            return g
        }
        return nil
    }

    internal func parseEdge(_ res: NSDictionary) -> Edge {
        let from = parseInt(res, key: "from")
        let to = parseInt(res, key: "to")
        let weight = parseDouble(res, key: "weight")
        let e = Edge(v: from, w: to, weight: weight)
        return e
    }

    internal func parseNodes(_ res: [NSDictionary]) -> [MapNode] {
        var out = [MapNode]()
        for ndict in res {
            let n = parseNode(ndict)
            out.append(n)
        }
        return out
    }

    internal func parseNode(_ res: NSDictionary) -> MapNode {
        let id = parseInt(res, key: "id")
        let x = parseDouble(res, key: "x")
        let y = parseDouble(res, key: "y")
        return MapNode(Id: id,
                       x: x,
                       y: y)
    }
}
