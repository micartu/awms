//
//  MapLoader.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class MapLoader: MapService {
    internal let server: NetworkProtocol
    internal let filer: FileService
    internal var cache: StorageService
    internal let queue: DispatchQueue

    init(network: NetworkProtocol, cache: StorageService, saver: FileService) {
        self.server = network
        self.filer = saver
        self.cache = cache
        queue = DispatchQueue(label: Const.dqueue + ".map_service", qos: .utility, attributes: .concurrent)
    }

    func get(graph: String, answer: @escaping (Graph?, NSError?) -> Void) {
        if let g = cache.get(graph: graph) {
            answer(g, nil)
        } else {
            let error = NSError(domain: Const.domain,
                                code: Const.kErrMap,
                                userInfo: [NSLocalizedDescriptionKey: "no graph in cache!".localized])
            answer(nil, error)
        }
    }

    func fetch(graph: String, answer: @escaping (NSError?) -> Void) {
        queue.async { [unowned self] in
            self.server.getQuery(url: "/graph", success: { (res: NSDictionary) in
                if let g = self.parseGraph(res) {
                    self.cache.update(graph: g, with: graph) {
                        DispatchQueue.main.async {
                            answer(nil)
                        }
                    }
                } else {
                    let error = NSError(domain: Const.domain,
                                        code: Const.kErrMap,
                                        userInfo: [NSLocalizedDescriptionKey: "Can't parse graph".localized])
                    DispatchQueue.main.async {
                        answer(error)
                    }
                }
                }, failure: { (err) in
                    DispatchQueue.main.async {
                        answer(err)
                    }
            })
        }
    }

    func getMapCoordinatesOf(graph: String, answer: @escaping ([MapNode]?, NSError?) -> Void) {
        if let nodes = cache.getCoordForMap(name: graph) {
            answer(nodes, nil)
        } else {
            let error = NSError(domain: Const.domain,
                                code: Const.kErrMap,
                                userInfo: [NSLocalizedDescriptionKey: "Can't load graph's coordinates".localized])
            answer(nil, error)
        }
    }

    func fetchCoordinatesOf(graph: String, answer: @escaping (NSError?) -> Void) {
        queue.async { [unowned self] in
            self.server.getQuery(url: "/coordinates", success: { (res: [NSDictionary]) in
                let nodes = self.parseNodes(res)
                self.cache.update(coords: nodes, for: graph) {
                    DispatchQueue.main.async {
                        answer(nil)
                    }
                }
            }, failure: { (err) in
                DispatchQueue.main.async {
                    answer(err)
                }
            })
        }
    }
}
