//
//  MapService.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol MapService: class {
    func get(graph: String, answer: @escaping (Graph?, NSError?) -> Void)
    func fetch(graph: String, answer: @escaping (NSError?) -> Void)
    func getMapCoordinatesOf(graph: String, answer: @escaping ([MapNode]?, NSError?) -> Void)
    func fetchCoordinatesOf(graph: String, answer: @escaping (NSError?) -> Void)
}
