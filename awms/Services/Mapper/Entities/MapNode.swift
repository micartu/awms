//
//  MapNode.swift
//  awms
//
//  Created by michael on 12.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

struct MapNode {
    let Id: Int
    let x: Double
    let y: Double
}
