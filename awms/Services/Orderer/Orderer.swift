//
//  Orderer.swift
//  awms
//
//  Created by michael on 16.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Orderer: OrdererService {
    internal let server: NetworkProtocol
    internal var cache: StorageService
    internal let queue: DispatchQueue

    init(network: NetworkProtocol, cache: StorageService) {
        self.server = network
        self.cache = cache
        self.queue = DispatchQueue(label: Const.dqueue + ".orderer", qos: .utility, attributes: .concurrent)
    }

    func order(items: [Item], answer: @escaping ([Item]?, NSError?) -> Void) {
        queue.async { [unowned self] in
            let data = try! JSONEncoder().encode(items)
            self.server.postQuery(url: "/item/add", data: data, success: { (res: [NSDictionary]) in
                if let items = loadItems(res, parseStored: true) {
                    if items.count > 0 {
                        let group = DispatchGroup()
                        for i in items {
                            group.enter()
                            self.cache.update(item: i) {
                                group.leave()
                            }
                        }
                        group.notify(queue: self.queue) {
                            DispatchQueue.main.async {
                                answer(items, nil)
                            }
                        }
                        return
                    }
                }
                DispatchQueue.main.async {
                    answer(nil, nil)
                }
            }, failure: { err in
                DispatchQueue.main.async {
                    answer(nil, err)
                }
            })
        }
    }
}
