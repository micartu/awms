//
//  OrdererService.swift
//  awms
//
//  Created by michael on 16.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol OrdererService: class {
    func order(items: [Item], answer: @escaping ([Item]?, NSError?) -> Void)
}
