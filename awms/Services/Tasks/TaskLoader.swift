//
//  TaskLoader.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class TaskLoader: TaskService {
    internal let server: NetworkProtocol
    internal let filer: FileService
    internal var cache: StorageService
    internal var imgLoader: ImageLoaderService
    internal let mapLoader: MapService
    internal let queue: DispatchQueue
    internal var login = ""

    init(network: NetworkProtocol,
         cache: StorageService,
         imgLoader: ImageLoaderService,
         mapLoader: MapService,
        saver: FileService) {
        self.server = network
        self.filer = saver
        self.cache = cache
        self.mapLoader = mapLoader
        self.imgLoader = imgLoader
        queue = DispatchQueue(label: Const.dqueue + ".task_loader", qos: .utility, attributes: .concurrent)
    }

    func change(cache: StorageService) {
        self.cache = cache
    }

    func changeLogin(name: String) {
        login = name
    }

    func askForUserInfoWith(login: String, answer: @escaping (User?, NSError?) -> Void) {
        queue.async { [unowned self] in
            self.fetchUser() { (user, err) in
                if let u = user {
                    self.cache.update(user: u) {
                        DispatchQueue.main.async {
                            answer(u, nil)
                        }
                    }
                } else {
                    // try to get it from the local cache then
                    if let u = self.cache.get(user: login) {
                        DispatchQueue.main.async {
                            answer(u, nil)
                        }
                    } else {
                        // bad news, nothing was found in cache and no connection?
                        let er = NSError(domain: Const.domain,
                                         code: Const.kErrTask,
                                         userInfo: [NSLocalizedDescriptionKey: "Cannot fetch user info".localized])
                        DispatchQueue.main.async {
                            answer(nil, er)
                        }
                    }
                }
            }
        }
    }

    func askForTasks(_ answer: @escaping ([Task]?, NSError?) -> Void) {
        queue.async { [unowned self] in
            if self.cache.countLoadedTasksFor(login: self.login) == 0 {
                // nothing loaded yet, try to load from server
                self.server.getQuery(url: "/tasks", success: { (res: [NSDictionary]) in
                    if let tasks = self.parseTasks(res) {
                        let group = DispatchGroup()
                        var errChild: NSError? = nil
                        for t in tasks {
                            if t is TaskDelivery {
                                let td = t as! TaskDelivery
                                group.enter()
                                self.mapLoader.fetch(graph: td.map) { err in
                                    if let e = err {
                                        group.leave()
                                        errChild = e
                                    } else {
                                        self.mapLoader.fetchCoordinatesOf(graph: td.map) { err in
                                            group.leave()
                                            errChild = err
                                        }
                                    }
                                }
                            }
                        }
                        group.enter()
                        self.cache.update(tasks: tasks) {
                            group.leave()
                        }
                        group.notify(queue: self.queue) {
                            // return to the user updated tasks from the cache:
                            if let e = errChild {
                                DispatchQueue.main.async {
                                    answer(nil, e)
                                }
                            } else {
                                let t = self.cache.getTasksFor(user: self.login)
                                DispatchQueue.main.async {
                                    answer(t, nil)
                                }
                            }
                        }
                    }
                }, failure: { (err) in
                    DispatchQueue.main.async {
                        answer(nil, err)
                    }
                })
            } else { // fetch tasks from cache if any
                DispatchQueue.main.async {
                    answer(self.cache.getTasksFor(user: self.login), nil)
                }
            }
        }
    }

    func countOfTasksFor(user: String) -> UInt {
        return cache.countLoadedTasksFor(login: user)
    }

    func fetchUser(_ answer: @escaping (User?, NSError?) -> Void) {
        server.getQuery(url: "/user", success: { [unowned self] (res: NSDictionary) in
            if let u = self.parseUser(res) {
                answer(u, nil)
            } else {
                answer(nil, nil)
            }
        }, failure: { (err) in
            answer(nil, err)
        })
    }
}
