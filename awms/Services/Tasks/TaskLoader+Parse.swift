//
//  TaskLoader+Parse.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import CocoaLumberjack

extension TaskLoader {

    // MARK: - Tasks

    internal func parseTasks(_ res: [NSDictionary]) -> [Task]? {
        var out = [Task]()
        for t in res {
            out.append(parseTask(t))
        }
        if out.count > 0 {
            return out
        }
        return nil
    }

    internal func parseTask(_ res: NSDictionary) -> Task {
        var out: Task
        let id = parseInt(res, key: "id")
        let type = res["type"]! as! String
        let state = res["state"]! as! String
        let startNode = res["start_node"] as? Int
        if type == "scan" || type == "print_labels" {
            var items: [Item]? = nil
            if let itms = res["items"] as? [NSDictionary] {
                items = loadItems(itms, parseStored: false)
            }
            if type == "scan" {
                out = TaskScan(items: items)
            } else {
                out = TaskPrint(items: items)
            }
        } else if type == "delivery" {
            let map = res["map"] as! String
            out = TaskDelivery(map: map, startNode: startNode ?? 0)
            server.getRawQuery(url: "/img/\(map).svg", success: { [unowned self] data in
                // save the image into the local filesystem
                self.filer.save(.mapImage, name: map, with: data)
                (out as! TaskDelivery).img = map
            }, failure: { error in
                DDLogError("got an error while trying to download a map: \(error.localizedDescription)")
            })
        } else if type == "transportation" {
            let r = res["descr"]! as! NSDictionary
            let coord_a = r["a_coord"]! as! String
            let descr_a = r["a_descr"]! as! String
            let coord_b = r["b_coord"]! as! String
            let descr_b = r["b_descr"]! as! String
            let descr = r["descr"]! as! String
            if let t = TaskTruckDelivery(pointDescr: [descr_a, descr_b],
                                    pointCoord: [coord_a, coord_b],
                                    descr: descr) {
                out = t
            } else {
                out = Task(type: .unknown)
            }
        } else {
            out = Task(type: .unknown)
        }
        out.state = parseTaskState(state)
        out.id = id
        return out
    }

    internal func parseTaskState(_ state: String) -> TaskState {
        if state == "unfinished" {
            return .unfinished
        } else if state == "finished" {
            return .finished
        }
        return .unknown
    }

    // MARK: - User

    internal func parseUser(_ res: NSDictionary) -> User? {
        if let login = res["login"] as? String {
            let name = res["name"] as? String ?? "?"
            let avatar = res["avatar"] as? String ?? "?"
            var capabilities: [Capability]?
            if let c = res["capabilities"] as? [NSDictionary] {
                capabilities = parseCapabilities(c)
            } else {
                capabilities = nil
            }
            let user = User(login: login,
                            name: name,
                            avatar: avatar,
                            capabilities: capabilities)
            return user
        }
        return nil
    }

    internal func parseCapabilities(_ res: [NSDictionary]) -> [Capability]? {
        var out = [Capability]()
        for d in res {
            let id = d["id"] as? String ?? "0"
            let s = parseDouble(d, key: "s")
            let h = parseDouble(d, key: "h")
            let m = parseDouble(d, key: "m")
            let c = Capability(id: id.toUInt, s: s, h: h, m: m)
            out.append(c)
        }
        if out.count > 0 {
            return out
        }
        return nil
    }

    // MARK: - Private
}

func loadItems(_ res: [NSDictionary], parseStored: Bool) -> [Item]? {
    var out = [Item]()
    let imgLoader = ServicesAssembler.shared().imgLoader
    for d in res {
        guard let qr = d["qr"] as? String else {
            continue
        }
        let ean = parseInt(d, key: "ean")
        let s = parseDouble(d, key: "s")
        let h = parseDouble(d, key: "h")
        let m = parseDouble(d, key: "m")
        let title = d["title"] as? String ?? ""
        let descr = d["descr"] as? String ?? ""
        let imgName = d["img"] as? String ?? ""
        let storedIn = parseStored ? parseInt(d, key: "cellid") : -1
        var i = Item(id: 0,
                     qr: qr,
                     amount: 1,
                     ean: ean,
                     status: .unknown,
                     scanned: 0,
                     storedIn: storedIn,
                     title: title,
                     descr: descr,
                     imgName: imgName,
                     img: imgLoader.noImage(),
                     s: s,
                     h: h,
                     m: m)
        imgLoader.load(image: imgName) { img in
            i.img = img
        }
        out.append(i)
    }
    if out.count > 0 {
        return out
    }
    return nil
}

func parseDouble(_ res: NSDictionary, key: String) -> Double {
    var out: String
    if let tmp = res[key] as? Double {
        return tmp
    }
    if let tmp = res[key] as? String {
        out = tmp
    } else {
        out = "0"
    }
    return out.toDouble
}

func parseInt(_ res: NSDictionary, key: String) -> Int {
    var out: Int
    if let tmp = res[key] as? Int {
        out = tmp
    } else {
        out = 0
    }
    return out
}
