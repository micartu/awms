//
//  TaskService.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol TaskService: class {
    func changeLogin(name: String)
    func askForUserInfoWith(login: String, answer: @escaping (User?, NSError?) -> Void)
    func askForTasks(_ answer: @escaping ([Task]?, NSError?) -> Void)
    func countOfTasksFor(user: String) -> UInt
    
    func change(cache: StorageService)
}
