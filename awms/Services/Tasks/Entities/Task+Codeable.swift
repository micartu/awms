//
//  Task+Codeable.swift
//  awms
//
//  Created by Michael Artuerhof on 29.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension TaskType: Codable {
    private enum CodingKeys: CodingKey {
        case scan
        case delivery
        case unknown
    }

    func encode(to encoder: Encoder) throws {
        var str: String
        switch self {
        case .scan:
            str = "scan"
        case .printLabels:
            str = "print_labels"
        case .delivery:
            str = "delivery"
        case .truckDelivery:
            str = "transportation"
        case .unknown:
            str = "unknown"
        }
        try str.encode(to: encoder)
    }
}

extension TaskState: Codable {
    private enum CodingKeys: String, CodingKey {
        case unfinished = "unfinished"
        case finished = "finished"
        case unknown = "unknown"
    }

    func encode(to encoder: Encoder) throws {
        var str: String
        switch self {
        case .unfinished:
            str = "unfinished"
        case .finished:
            str = "finished"
        case .unknown:
            str = "unknown"
        }
        try str.encode(to: encoder)
    }
}
