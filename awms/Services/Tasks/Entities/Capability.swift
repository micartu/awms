//
//  Capability.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

struct Capability {
    let id: UInt
    let s: Double
    let h: Double
    let m: Double
}
