//
//  User.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

struct User {
    let login: String
    let name: String
    let avatar: String
    let capabilities: [Capability]?
}
