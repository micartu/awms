//
//  Item.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

enum ItemState: Int, Codable {
    case unknown = 0
    case toDeliver
    case delivered
}

struct Item: Codable {
    var id = 0
    let qr: String
    let amount: Double
    let ean: Int
    var status: ItemState = .unknown
    var scanned: Int
    var storedIn: Int = -1
    let title: String
    let descr: String
    let imgName: String
    var img: UIImage? = nil
    let s: Double
    let h: Double
    let m: Double
    enum CodingKeys: String, CodingKey {
        case id
        case qr
        case title
        case descr
        case amount
        case ean
        case scanned
        case s
        case h
        case m
        case storedIn = "cellid"
        case imgName = "img"
    }
}
