//
//  Task.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

enum TaskType: Int {
    case scan = 1
    case printLabels
    case delivery
    case truckDelivery
    case unknown
}

enum TaskState: Int {
    case unfinished = 1
    case finished
    case unknown
}

class Task: Encodable {
    var id: Int = 0
    let type: TaskType
    var state: TaskState
    let startNode: Int

    init(type: TaskType, state: TaskState, startNode: Int) {
        self.type = type
        self.state = state
        self.startNode = startNode
    }

    convenience init(type: TaskType) {
        self.init(type: type, state: .unknown, startNode: -1)
    }

    enum CodingKeys: String, CodingKey {
        case id
        case type
        case state
        case startNode = "start_node"
    }
}

/// user has to scan items for their ordering on the warehouse storage
class TaskScan: Task {
    var items: [Item]?
    let imgLoader: ImageLoaderService

    init(items: [Item]?, type: TaskType) {
        self.items = items
        self.imgLoader = ServicesAssembler.shared().imgLoader
        super.init(type: type, state: .unknown, startNode: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(imageUpdated(notification:)),
                                               name: NSNotification.Name(rawValue: imgLoader.loadedImageNotification()),
                                               object: nil)
    }

    convenience init(items: [Item]?) {
        self.init(items: items, type: .scan)
    }

    enum ScanCodingKeys: String, CodingKey {
        case items
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ScanCodingKeys.self)
        try container.encode(items, forKey: .items)
        try super.encode(to: encoder)
    }

    @objc func imageUpdated(notification: Notification) {
        if let items = items {
            let name = notification.userInfo!["name"] as? String ?? ""
            for i in items.indices {
                if items[i].imgName == name {
                    let img = notification.object as? UIImage ?? imgLoader.noImage()
                    self.items![i].img = img
                }
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

/// user has to print labels for a given set of items
class TaskPrint: TaskScan {
    init(items: [Item]?) {
        super.init(items: items, type: .printLabels)
    }
}

/// user has to deliver items to the point in warehouse where given item was put
class TaskDelivery: Task {
    let map: String
    var img = ""
    init(map: String, startNode: Int) {
        self.map = map
        super.init(type: .delivery, state: .unknown, startNode: startNode)
    }

    enum DeliveryCodingKeys: String, CodingKey {
        case map
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DeliveryCodingKeys.self)
        try container.encode(map, forKey: .map)
        try super.encode(to: encoder)
    }
}

/// there are some items to be delivered by a truck
class TaskTruckDelivery: Task {
    let pointsDescr: [String]
    let pointsCoord: [CGPoint]
    let descr: String

    init?(pointDescr: [String], pointCoord: [String], descr: String) {
        self.pointsDescr = pointDescr
        self.descr = descr
        var coords = [CGPoint]()
        for pt in pointCoord {
            let coord = pt.split(separator: ",")
            if coord.count > 1 {
                let x = String(coord[0]).toDouble
                let y = String(coord[1]).toDouble
                coords.append(CGPoint(x: x, y: y))
            } else {
                if coords.count == pointDescr.count {
                    break
                } else {
                    return nil
                }
            }
        }
        self.pointsCoord = coords
        super.init(type: .truckDelivery, state: .unknown, startNode: 0)
    }
}
