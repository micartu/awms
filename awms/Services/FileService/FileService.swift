//
//  FileService.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

enum FileTypes {
    case mapImage
    case image
}

protocol FileService: class {
    func save(_ type: FileTypes, name: String, with data: Data)
    func load(image: String) -> UIImage?
    func remove(image: String)
    func contentsOf(image: String) -> Data?
    func contentsOfMap(_ name: String) -> String
}
