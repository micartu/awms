//
//  FileSaver.swift
//  awms
//
//  Created by Michael Artuerhof on 07.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import CocoaLumberjack

class FileSaver: FileService {
    func save(_ type: FileTypes, name: String, with data: Data) {
        do {
            let fileName = path(type: type, name: name, create: true)
            try data.write(to: fileName)
            DDLogInfo("file of type \(type) saved at path: \(fileName)")
        }
        catch {
            DDLogError("Cannot create file of \(type) with name: \(name)")
        }
    }

    func load(image: String) -> UIImage? {
        if let d = contentsOf(image: image)  {
            return UIImage(data: d)
        }
        return nil
    }

    func contentsOf(image: String) -> Data? {
        let filePath = path(type: .image, name: image, create: false)
        return try? Data(contentsOf: filePath)
    }

    func remove(image: String) {
        let filePath = path(type: .image, name: image, create: false)
        do {
            try FileManager.default.removeItem(at: filePath)
            DDLogInfo("Deleted an image with name: \(image)")
        }
        catch {
            DDLogError("Cannot delete an image with name: \(image)")
        }
    }

    func contentsOfMap(_ name: String) -> String {
        let filePath = path(type: .mapImage, name: name, create: false)
        if let m = try? String.init(contentsOf: filePath, encoding: .utf8) {
            return m
        }
        return ""
    }

    // MARK: - Private

    func path(type: FileTypes, name: String, create: Bool) -> URL {
        var d: String
        switch type {
        case .mapImage:
            d = "map"
        case .image:
            d = "img"
        }
        let fm = FileManager.default
        let cachesurl = try! fm.url(for:.applicationSupportDirectory,
                                   in: .userDomainMask,
                                   appropriateFor: nil,
                                   create: true)
        let dirPath = cachesurl.appendingPathComponent(d)
        if create {
            do {
                try fm.createDirectory(at: dirPath,
                                       withIntermediateDirectories: true,
                                       attributes: nil)
            }
            catch {
                DDLogError("Cannot create directory at path name: \(dirPath)")
            }
        }
        return dirPath.appendingPathComponent(name)
    }
}
