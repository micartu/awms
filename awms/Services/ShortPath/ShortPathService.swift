//
//  ShortPathService.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol ShortPathService: class {
    func calculateDistancesFrom(startPoint: Int, completion: @escaping (() -> Void))
    func pathTo(finalDestination: Int) -> [Edge]
    func costTo(finalDestination: Int) -> Double
}
