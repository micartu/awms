//
//  Graph.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Graph: NSObject {
    let V: Int
    var E: Int
    var adj: [[Edge]]

    init(V: Int) {
        assert(V >= 0)
        self.V = V
        self.E = 0
        adj = [[Edge]](repeating: [Edge](), count: V)
    }

    func addEdge(_ e: Edge) {
        let v = e.From()
        adj[v].append(e)
        E += 1
    }

    func addEdgeDS(_ e: Edge) {
        addEdge(e)
        let edgeReversed = Edge(v: e.To(), w: e.From(), weight: e.Weight())
        addEdge(edgeReversed)
    }

    func edges() -> [Edge] {
        var e = [Edge]()
        for i in 0..<adj.count {
            for edg in adj[i] {
                if !containsIn(edges: e, an: edg) {
                    e.append(edg)
                }
            }
        }
        return e
    }

    private func containsIn(edges: [Edge], an edge: Edge) -> Bool {
        for e in edges {
            if e.Weight() == edge.Weight() {
                if (e.From() == edge.From() && e.To() == edge.To()) ||
                    (e.From() == edge.To() && e.To() == edge.From()) {
                    return true
                }
            }
        }
        return false
    }

    deinit {
        let count = adj.count
        for i in 0..<count {
            adj[i].removeAll()
        }
        adj.removeAll()
    }
}
