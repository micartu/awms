//
//  Edge.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Edge: NSObject {
    fileprivate let v: Int
    fileprivate let w: Int
    fileprivate let weight: Double

    init(v: Int, w: Int, weight: Double) {
        self.v = v
        self.w = w
        self.weight = weight
    }

    func From() -> Int {
        return v
    }

    func To() -> Int {
        return w
    }

    func Weight() -> Double {
        return weight
    }
}
