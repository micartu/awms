//
//  Dijkstra.swift
//  awms
//
//  Created by Michael Artuerhof on 10.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class Dijkstra: ShortPathService {
    var distTo = [Double]()       // distTo[v] = distance  of shortest s->v path
    var edgeTo = [Edge]()         // edgeTo[v] = last edge on shortest s->v path
    var g: Graph
    let queue: DispatchQueue

    fileprivate var pq: PriorityQueue<iW> // priority queue of vertices
    fileprivate var startPoint: Int = -1  // a starting node

    struct iW { // private structure for PriorityQueue
        let ind: Int
        let weight: Double

        init(_ i: Int, weight: Double) {
            ind = i
            self.weight = weight
        }
    }

    init(graph: Graph) {
        g = graph
        queue = DispatchQueue(label: Const.dqueue + ".dijkstra", qos: .userInitiated, attributes: .concurrent)
        pq = PriorityQueue<iW>({(a: iW, b: iW) -> Bool in
            return a.weight < b.weight
        })
    }

    func calculateDistancesFrom(startPoint: Int, completion: @escaping (() -> Void)) {
        queue.async { [unowned self] in
            DTraceGraphBeginRecalculateFor(node: startPoint)
            self.startPoint = startPoint
            self.distTo = [Double](repeating: Double.infinity, count: self.g.V)
            self.edgeTo = [Edge](repeating: Edge(v:0, w:0, weight:0), count: self.g.V)
            self.distTo[startPoint] = 0.0
            self.pq.push(iW(startPoint, weight: 0.0))
            while !self.pq.isEmpty {
                let v = self.pq.pop()!
                let adjV = self.g.adj[v.ind]
                for (ed) in adjV {
                    self.relax(ed)
                }
            }
            DispatchQueue.main.async {
                completion()
            }
            DTraceGraphEndRecalculateFor(node: startPoint)
        }
    }

    fileprivate func relax(_ e: Edge) {
        let v = e.From()
        let w = e.To()
        if distTo[w] > distTo[v] + e.Weight() {
            DTraceGraphRelax(w, v, e.Weight())
            distTo[w] = distTo[v] + e.Weight()
            DTraceGraphNodeWeight(w, weight: distTo[w])
            edgeTo[w] = e
            pq.push(iW(w, weight: distTo[w]))
        }
    }

    func pathTo(finalDestination v: Int) -> [Edge] {
        var path = [Edge]()
        var edge: Edge
        var k = v
        repeat {
            edge = edgeTo[k]
            DTraceGraphPath(from: edge.From(), to: edge.To())
            path.append(edge)
            k = edge.From()
        }
        while (edge.From() != startPoint)
        return path
    }

    func costTo(finalDestination v: Int) -> Double {
        if v >= 0 && v < distTo.count {
            return distTo[v]
        }
        return 0
    }

    deinit {
        pq.removeAll()
        distTo.removeAll()
        edgeTo.removeAll()
    }
}
