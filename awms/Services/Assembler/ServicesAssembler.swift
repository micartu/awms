//
//  ServicesAssembler.swift
//  awms
//
//  Created by Michael Artuerhof on 23.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class ServicesAssembler {
    private static var sharedServices: ServicesAssembler = {
        let network = Network(user: "", pass: "")
        let cache = Storage(login: "")
        let saver = FileSaver()
        let printer = Printer()
        let scaner = Scanner()
        let cleaner = Cleaner(storage: cache,
                                disk: saver)
        let uploader = Uploader(storage: cache,
                                server: network,
                                disk: saver)
        let imgLoader = ImageLoader(network: network,
                                    cache: cache,
                                    saver: saver)
        let mapLoader = MapLoader(network: network,
                                  cache: cache,
                                  saver: saver)
        let shared = ServicesAssembler(network: network,
                                       cache: cache,
                                       saver: saver,
                                       cleaner: cleaner,
                                       uploader: uploader,
                                       mapLoader: mapLoader,
                                       imgLoader: imgLoader,
                                       printer: printer,
                                       scanner: scaner)
        return shared
    }()

    let server: NetworkProtocol
    let filer: FileService
    let cache: StorageService
    let cleaner: CleanerService
    let uploader: UploaderService
    let scanner: ScannerProtocol
    let printer: PrinterService
    let mapLoader: MapService
    let imgLoader: ImageLoaderService

    init(network: NetworkProtocol,
         cache: StorageService,
         saver: FileService,
         cleaner: CleanerService,
         uploader: UploaderService,
         mapLoader: MapService,
         imgLoader: ImageLoaderService,
         printer: PrinterService,
         scanner: ScannerProtocol) {
        self.server = network
        self.filer = saver
        self.cleaner = cleaner
        self.uploader = uploader
        self.cache = cache
        self.mapLoader = mapLoader
        self.imgLoader = imgLoader
        self.printer = printer
        self.scanner = scanner
    }

    // MARK: - Accessors

    class func shared() -> ServicesAssembler {
        return sharedServices
    }
}
