//
//  HelpPopUpVC.swift
//  awms
//
//  Created by michael on 20.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class HelpPopUpVC: CommonVC {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var labelInfo: UILabel!
    var theme: Theme? = nil
    var text: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        if let t = theme {
            imageV.tintColor = t.tintColor
            labelInfo.text = self.text!
            labelInfo.sizeToFit()
            let imgHeight = imageV.frame.origin.y + imageV.bounds.height
            let textHeightReal = labelInfo.frame.origin.y + labelInfo.bounds.height
            let textHeight = max(imgHeight, textHeightReal) + 10
            self.contentSizeInPopup = CGSize(width: UIScreen.main.bounds.width,
                                             height: textHeight)
            self.landscapeContentSizeInPopup = CGSize(width: UIScreen.main.bounds.height,
                                                      height: textHeight)
        }
        self.title = "Help".localized
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Public

    func show(text: String) {
        self.text = text
    }

    // MARK: - Themeble

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        self.theme = theme
    }
}
