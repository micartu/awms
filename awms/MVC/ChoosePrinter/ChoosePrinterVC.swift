//
//  ChoosePrinterVC.swift
//  awms
//
//  Created by Michael Artuerhof on 30.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit
import ExternalAccessory

class ChoosePrinterVC: CommonVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hiddenView: UIView!
    private var accessories: [EAAccessory]? = nil
    var currentName = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Choose printer".localized

        accessories = EAAccessoryManager.shared().connectedAccessories
        if accessories!.count < 1 {
            hiddenView.isHidden = false
        } else {
            hiddenView.isHidden = true
        }
        tableView.dataSource = self
        tableView.delegate = self

        if let name = UserDefaults.standard.value(forKey: Const.kSelectedPrinterName) as? String {
            currentName = name
        }
    }
}

extension ChoosePrinterVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let acs = accessories {
            return acs.count
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deviceCell")!
        var name = accessories![indexPath.row].name
        if name.count == 0 {
            name = "unknown".localized
        }
        cell.textLabel?.text = name
        if name == currentName {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
}

extension ChoosePrinterVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > accessories?.count ?? 0 {
            return
        }
        let defaults = UserDefaults.standard
        let name = accessories![indexPath.row].name
        defaults.set(name, forKey: Const.kSelectedPrinterName)
        defaults.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
}
