//
//  AboutViewController.swift
//  awms
//
//  Created by Michael Artuerhof on 03.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

class AboutViewController: CommonVC {
    @IBOutlet weak var versionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "v \(version)"
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
        dismiss(animated: true)
    }
}
