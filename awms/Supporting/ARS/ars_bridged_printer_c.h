//
//  ars_bridged_printer_c.h
//  ARSTest
//
//  Created by Michael Artuerhof on 24/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#ifndef ars_bridged_printer_c_h
#define ars_bridged_printer_c_h

#include <stdint.h>

int ARSwriteBytes(void *object, const unsigned char *buffer, unsigned int bufferSize);
int ARSreadBytes(void *object, unsigned char *buffer, unsigned int bufferSize);

void ARSrtask_data(void *object, uint8_t* data, int len);
void ARSrtask_seq_command(void *object, int seq, int command);

#endif /* ars_bridged_printer_c_h */
