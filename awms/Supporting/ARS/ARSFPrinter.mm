//
//  Printer.m
//  ARSTest
//
//  Created by Michael Artuerhof on 24/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "ARSFPrinter.h"
#import "ARSBridgedPrinter.hpp"
#import "EADSessionController.h"
#import <ExternalAccessory/ExternalAccessory.h>
#include <string.h>

static NSString* const kPrinterProtocol = @"com.datecs.printer.fiscal.2";

@interface ARSFPrinter (){
    ARSBridgedPrinter  *_printer;
    EADSessionController *_ea;
    EAAccessory *_accessory;
    bool _connected;
}

@end

@implementation ARSFPrinter

int ARSwriteBytes(void *object, const unsigned char *buffer, unsigned int bufferSize)
{
    return [(__bridge id)object writeBytes:buffer size:bufferSize];
}

int ARSreadBytes(void *object, unsigned char *buffer, unsigned int pLenIncBuffer)
{
    return [(__bridge id)object readBytes:buffer size:pLenIncBuffer];
}

- (id)init {
    self = [super init];
    if (self){
        _connected = false;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(_accessoryDidConnect:)
                                                     name:EAAccessoryDidConnectNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(_accessoryDidDisconnect:)
                                                     name:EAAccessoryDidDisconnectNotification
                                                   object:nil];
        [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
        _ea = [EADSessionController sharedController];
    }
    return self;
}

- (void)dealloc
{
}

-(void)setBridgedPrinter:(void*)p {
    _printer = (ARSBridgedPrinter*)p;
}

-(void)connectToPrinter:(EAAccessory *)accessory {
    _accessory = accessory;
    [_ea setupControllerForAccessory:accessory
                  withProtocolString:kPrinterProtocol];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_sessionDataReceived:)
                                                 name:EADSessionDataReceivedNotification
                                               object:nil];
    [_ea openSession];

    if (self.delegate)
        [self.delegate connected];

    _connected = true;
}

#pragma mark - EADSessionController methods

- (void)_sessionDataReceived:(NSNotification *)notification {
    assert(_printer);
    _printer->dataAvailable();
}

- (void)_accessoryDidConnect:(NSNotification *)notification {
    EAAccessory *connectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
    [self connectToPrinter:connectedAccessory];
}

- (void)_accessoryDidDisconnect:(NSNotification *)notification {
    EAAccessory *disconnectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
    if (disconnectedAccessory == _accessory){
        if (self.delegate)
            [self.delegate disconnected];
        [[NSNotificationCenter defaultCenter] removeObserver:EADSessionDataReceivedNotification];
        [_ea closeSession];
        _connected = false;
    }
}

#pragma mark - bridge methods

- (int)writeBytes:(const unsigned char *)buffer size: (int)bufferSize {
    NSInteger res = ERR_NO_CONNECTION;
    if (_connected){
        NSData *data = [NSData dataWithBytes:buffer length:bufferSize];
        if (![_ea writeData:data]){
            _connected = false;
            return (int)res;
        }
        res = bufferSize;
    } else { // disconnected
        assert(_accessory);
        [self connectToPrinter:_accessory];
    }
    return (int)res;
}

- (int)readBytes:(unsigned char *)buffer size: (int)bufferSize {
    int bytesAvailable = (int)[_ea readBytesAvailable];
    NSData *data = [_ea readData:bytesAvailable];
    if (data){
        int min_len = MIN(bytesAvailable, bufferSize);
        memcpy(buffer, [data bytes], min_len);
        return min_len;
    }
    return bytesAvailable;
}
@end
