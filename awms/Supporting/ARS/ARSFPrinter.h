//
//  Printer.h
//  ARSTest
//
//  Created by Michael Artuerhof on 24/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EAAccessory;

@protocol ARSFPrinterProtocol <NSObject>

- (void)connected;
- (void)disconnected;
- (void)statusChanged:(NSString*)status;

@end

@interface ARSFPrinter : NSObject

- (void)connectToPrinter:(EAAccessory *)accessory;
- (void)setBridgedPrinter:(void*)p;

@property (nonatomic, strong) NSString* status;
@property (nonatomic, weak) id<ARSFPrinterProtocol> delegate;

@end
