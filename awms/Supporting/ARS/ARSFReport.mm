//
//  ARSFReport.m
//  ARSTest
//
//  Created by Michael Artuerhof on 28/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "ARSFReport.h"
#import "ARSBridgedReport.hpp"

@implementation ARSFReport

void ARSrtask_data(void *object, uint8_t* data, int len){
    [(__bridge id)object rtaskWithData:data andLen:len];
}

void ARSrtask_seq_command(void *object, int seq, int command){
    [(__bridge id)object rtaskWithSEQ:seq andCommand:command];
}

- (void)checkRTask:(void*)task {
    //arsp::rtask* r = (arsp::rtask*)task;
    //NSLog(@"seq: %x", r->seq());
}

- (void)rtaskWithData:(uint8_t*)data andLen:(int)len {
    //NSLog(@"came data with len: %ld", (long)len);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate)
            [self.delegate cameData:data withLen:len];
    });
}

- (void)rtaskWithSEQ:(int)seq andCommand:(int)command {
    //NSLog(@"came command %ld with seq: %ld", (long)command, (long)seq);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate)
            [self.delegate cameCommand:command withSEQ:seq];
    });
}
@end
