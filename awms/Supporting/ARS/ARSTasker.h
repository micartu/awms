//
//  ARSTasker.h
//  ARSTest
//
//  Created by Michael Artuerhof on 11/09/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import <Foundation/Foundation.h>

enum PaymentType {
    PaymentCash = 0,
    PaymentCard = 1,
};

@protocol ARSTaskerProtocol <NSObject>

- (void)printerAnswered:(NSArray*)str forSeq:(int)seq;

@end

@class ARSFPrinter;

@interface ARSTasker : NSObject

@property (nonatomic, weak) id<ARSTaskerProtocol> delegate;

- (instancetype)initWithPrinter:(ARSFPrinter*)printer;

/**
 opens day
 */
- (NSInteger)openDayForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer andOperatorName:(NSString*)opName;
- (NSInteger)openDayForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer;

/**
 opens a cheque
 */
- (NSInteger)openChequeForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer andOperatorName:(NSString*)opName isReturnCheque:(BOOL)retCheque;
- (NSInteger)openChequeForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer;

/**
 * cancels an opennened cheque
 */
- (NSInteger)cancelCheque;

/**
 * closes an openned cheque
 */
- (NSInteger)closeCheque;

/**
 registers an item for cheque
 */
- (NSInteger)registerItem:(NSString*)name
                 forTaxGr:(NSInteger)tax
            andDepartment:(NSInteger)depart
          andDiscountType:(NSInteger)discountType
         andDiscountValue:(double)discountValue
              andQuantity:(double)quantity
                 andPrice:(double)price;

- (NSInteger)registerItem:(NSString*)name
                 forTaxGr:(NSInteger)tax
            andDepartment:(NSInteger)depart
              andQuantity:(double)quantity
                 andPrice:(double)price;

/**
 registers the sum of money received from user
 */
- (NSInteger)paymentSum:(double)sum ofType:(enum PaymentType)type;

/**
 opens non fiscal receipt
 */
- (NSInteger)openNonFiscalReceipt;

/**
 prints text on non fiscal receipt
 */
- (NSInteger)printNonFiscalText:(NSString*)string;

/**
 closes non fiscal receipt
 */
- (NSInteger)closeNonFiscalReceipt;

/**
 prints X report
 */
- (NSInteger)xReport;

/**
 prints Z report
 */
- (NSInteger)zReport;

/**
 command for sending a copy of a next printed cheque to the given address (email or telephone)
 @param address email or telephone where the copy would be sent to
 */
- (NSInteger)sendCopyOfChequeToEMailOrTelephone:(NSString*)address;

/**
 prints QR, ean8, ean13, ean128
 */
- (NSInteger)print:(NSString*)qr orEAN8:(NSString*)ean8 orEAN13:(NSString*)ean13 orEAN128:(NSString*)ean128;
@end
