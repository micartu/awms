//
//  ARSBridgedReport.hpp
//  ARSTest
//
//  Created by Michael Artuerhof on 28/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <ars_printer/arsreport.h>

using namespace arsp;

class ARSBridgedReport : public Report
{
public:
    ARSBridgedReport(void* objc);
    ~ARSBridgedReport();

    virtual void checkRTask(rtask* ans);

private:
    void* _class;
};
