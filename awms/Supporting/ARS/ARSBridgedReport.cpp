//
//  ARSBridgedReport.cpp
//  ARSTest
//
//  Created by Michael Artuerhof on 28/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#include "ARSBridgedReport.hpp"
#include "ars_bridged_printer_c.h"
#include <ars_printer/rtask.h>

using namespace arsp;

ARSBridgedReport::ARSBridgedReport(void* objc) :
_class(objc)
{
}

ARSBridgedReport::~ARSBridgedReport() {
}

void ARSBridgedReport::checkRTask(rtask* ans) {
    ARSrtask_data(_class, ans->data(), ans->len());
    ARSrtask_seq_command(_class, ans->seq(), ans->cmd());
}
