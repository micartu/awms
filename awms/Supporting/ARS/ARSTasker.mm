//
//  ARSTasker.m
//  ARSTest
//
//  Created by Michael Artuerhof on 11/09/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#import "ARSTasker.h"
#import "ARSFPrinter.h"
#import "ARSBridgedPrinter.hpp"
#import "ARSFReport.h"
#import "ARSBridgedReport.hpp"
#import <ars_printer/arscommander.h>
#include <string.h>

#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif

enum sale_codes {
    SALES = 0,
    SHIFT_OPEN = 4,
};

@interface ARSTasker () <ARSFReportProtocol>
{
    arsp::ARSCommander *_commander;
    ARSFPrinter *_printer;
    ARSFReport *_reporter;
    ARSBridgedReport *_br_reporter;
    ARSBridgedPrinter *_br_printer;
    int _seq;
    int _rseq;
}

@end

@implementation ARSTasker

#pragma mark - Init

- (instancetype)init {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"initWithPrinter must be used!"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initWithPrinter:(ARSFPrinter*)printer {
    if (self = [super init] ) {
        _printer = printer;
        _br_printer= new ARSBridgedPrinter((__bridge void*)_printer);
        [_printer setBridgedPrinter:_br_printer]; // back link to c++ - class
        _commander = new arsp::ARSCommander(*_br_printer);
        // subscriber
        _reporter = [[ARSFReport alloc] init];
        _reporter.delegate = self;
        _br_reporter = new ARSBridgedReport((__bridge void*)_reporter);
        _commander->addSubscriber(_br_reporter);
    }
    return self;
}

#pragma mark - Inner Utilities

- (char*)convertTextIntoDOS866:(NSString*)text {
    static char ret_data[1024];
    memset(ret_data, 0, sizeof(ret_data));
    CFStringRef str = CFStringCreateWithCString(NULL,
                                                [text cStringUsingEncoding:NSUTF8StringEncoding],
                                                kCFStringEncodingUTF8);
    CFIndex len = MIN(CFStringGetLength(str), sizeof(ret_data));
    CFRange rangeToProcess = CFRangeMake(0, len);
    CFIndex usedBufferLength;
    CFStringGetBytes(str, rangeToProcess,
                     kCFStringEncodingDOSRussian,
                     '?',
                     FALSE,
                     (UInt8*)ret_data, len, &usedBufferLength);
    ret_data[len] = '\0'; // just for sure:) even if we've got memset beforehand!)
    return ret_data;
}

#pragma mark - ARSFReportProtocol

- (void)cameData:(uint8_t*)data withLen:(int)len {
    if (self.delegate && data) {
        int slen = len - 10;
        char s[slen + 1];
        strncpy(s, (const char*)data, sizeof(s));
        s[slen] = 0;
        NSString *str = [NSString stringWithUTF8String:s];
        [self.delegate printerAnswered:[str componentsSeparatedByString:@"\t"] forSeq:_rseq];
    }
}

- (void)cameCommand:(int)command withSEQ:(int)seq {
    _rseq = seq;
}

#pragma mark - functions

- (void)openFiscalReceiptOfType:(NSInteger)type
                    forOperator:(NSInteger)op
                andOperatorName:(NSString*)opName
                        andPass:(NSInteger)pass
                 isReturnCheque:(BOOL)retCheque
                       andBuyer:(NSString*)buyer {
    struct arsp::fiscalReceiptParams p;
    memset(&p, 0, sizeof(p));
    p.OpCode = (int)op;
    p.Type = (int)type;
    p.TillNmb = 1; // hardcoded place
    snprintf(p.OpPwd,sizeof(p.OpPwd), "%ld", (long)pass);
    strncpy(p.Buyer,
            [buyer cStringUsingEncoding:NSUTF8StringEncoding],
            sizeof(p.Buyer));
    strncpy(p.OpName,
            [opName cStringUsingEncoding:NSUTF8StringEncoding],
            sizeof(p.OpName));
    if (retCheque)
        p.Type = 1; // return type of cheque
    _seq = _commander->getSeq();
    _commander->openFiscalReceipt(p);
}

- (NSInteger)openDayForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer andOperatorName:(NSString*)opName {
    int code = _commander->getSeq();
    [self openFiscalReceiptOfType:SHIFT_OPEN
                      forOperator:op
                  andOperatorName:opName
                          andPass:pass
                   isReturnCheque:NO
                         andBuyer:buyer];
    return code;
}

- (NSInteger)openDayForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer {
    return [self openDayForOperator:op andPass:pass andBuyer:buyer andOperatorName:@""];
}

- (NSInteger)openChequeForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer andOperatorName:(NSString*)opName isReturnCheque:(BOOL)retCheque {
    int code = _commander->getSeq();
    [self openFiscalReceiptOfType:SALES
                      forOperator:op
                  andOperatorName:opName
                          andPass:pass
                   isReturnCheque:retCheque
                         andBuyer:buyer];
    return code;
}

- (NSInteger)openChequeForOperator:(NSInteger)op andPass:(NSInteger)pass andBuyer:(NSString*)buyer {
    return [self openChequeForOperator:op andPass:pass andBuyer:buyer andOperatorName:@"" isReturnCheque:NO];
}

- (NSInteger)cancelCheque {
    int code = _commander->getSeq();
    _commander->cancelFiscalReceipt();
    return code;
}

- (NSInteger)closeCheque {
    int code = _commander->getSeq();
    _commander->closeFiscalReceipt();
    return code;
}

- (NSInteger)registerItem:(NSString*)name
                 forTaxGr:(NSInteger)tax
            andDepartment:(NSInteger)depart
          andDiscountType:(NSInteger)discountType
         andDiscountValue:(double)discountValue
              andQuantity:(double)quantity
                 andPrice:(double)price {
    int code = _commander->getSeq();
    struct arsp::saleRegistrationParams p;
    memset(&p, 0, sizeof(p));
    strncpy(p.PluName,
            [self convertTextIntoDOS866:name],
            sizeof(p.PluName));
    p.TaxGr = (int)tax;
    p.Department = (int)depart;
    p.Quantity = quantity;
    p.DiscountType = (int)discountType;
    p.DiscountValue = discountValue;
    p.Price = price;
    _commander->saleRegistration(p);
    return code;
}

- (NSInteger)registerItem:(NSString*)name
                 forTaxGr:(NSInteger)tax
            andDepartment:(NSInteger)depart
              andQuantity:(double)quantity
                 andPrice:(double)price {
    return [self registerItem:name
                     forTaxGr:tax
                andDepartment:depart
              andDiscountType:0
             andDiscountValue:0
                  andQuantity:quantity
                     andPrice:price];
}

- (NSInteger)paymentSum:(double)sum ofType:(enum PaymentType)type {
    int code = _commander->getSeq();
    struct arsp::paymentsCalculationParams p;
    memset(&p, 0, sizeof(p));
    p.PaidMode = (arsp::PaidModes) type;
    p.Amount = sum;
    _commander->paymentsCalculation(p);
    return code;
}

- (NSInteger)openNonFiscalReceipt {
    int code = _commander->getSeq();
    _commander->openNonFiscalReceipt();
    return code;
}

- (NSInteger)closeNonFiscalReceipt {
    int code = _commander->getSeq();
    _commander->closeNonFiscalReceipt();
    return code;
}

- (NSInteger)printNonFiscalText:(NSString*)string {
    int code = _commander->getSeq();
    struct nonFiscalTextParams p;
    memset(&p, 0, sizeof(p));
    strncpy(p.text, [self convertTextIntoDOS866:string], sizeof(p.text));
    _commander->printNonFiscalText(p);
    return code;
}

- (NSInteger)XZReport:(char)report {
    int code = _commander->getSeq();
    _commander->report(report);
    return code;
}

- (NSInteger)xReport {
    return [self XZReport:XReport];
}

- (NSInteger)zReport {
    return [self XZReport:ZReport];
}

- (NSInteger)sendCopyOfChequeToEMailOrTelephone:(NSString*)address {
    int code = _commander->getSeq();
    struct gsmEmailParams p;
    memset(&p, 0, sizeof(p));
    if ([address containsString:@"@"])
        p.Type = gsmEmailTypeEmail;
    else
        p.Type = gsmEmailTypeGSM;
    strncpy(p.out, [address UTF8String], sizeof(p.out));
    _commander->writeGSMorEmailForFiscalReceipt(p);
    return code;
}

- (NSInteger)print:(NSString*)qr orEAN8:(NSString*)ean8 orEAN13:(NSString*)ean13 orEAN128:(NSString*)ean128 {
    int code = _commander->getSeq();
    struct barCodeParams p;
    memset(&p, 0, sizeof(p));
    NSString *param;
    if (qr.length) {
        param = qr;
        p.type = 4;
    } else if (ean8.length) {
        param = ean8;
        p.type = 1;
    } else if (ean13.length) {
        param = ean13;
        p.type = 2;
    } else if (ean128.length) {
        param = ean128;
        p.type = 3;
    } else {
        param = @"?";
    }
    strncpy(p.data,
            [self convertTextIntoDOS866:param],
            sizeof(p.data));
    p.QRcodeSize = 10;
    _commander->printBarCode(p);
    return code;
}

@end
