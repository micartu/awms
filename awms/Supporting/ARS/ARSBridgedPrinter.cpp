//
//  ARSBridgedPrinter.cpp
//  ARSTest
//
//  Created by Michael Artuerhof on 24/03/2017.
//  Copyright © 2017 Dataphone. All rights reserved.
//

#include "ARSBridgedPrinter.hpp"
#include "ars_bridged_printer_c.h"

ARSBridgedPrinter::ARSBridgedPrinter(void* objc)
: Printer(),
_class(objc) {
}

int ARSBridgedPrinter::write(const uint8_t* data, unsigned int len) {
    return ARSwriteBytes(_class, data, len);
}

int ARSBridgedPrinter::read(uint8_t* buf, unsigned int buflen) {
    return ARSreadBytes(_class, buf, buflen);
}

ARSBridgedPrinter::~ARSBridgedPrinter() {
    _class = NULL;
}
