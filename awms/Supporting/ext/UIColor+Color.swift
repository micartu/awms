//
//  UIColor+Color.swift
//  awms
//
//  Created by michael on 22.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation
import Macaw

extension UIColor {
    func toMacaw() -> Color {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        let _ = getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha)
        return Color.rgba(r: Int(fRed * 255),
                          g: Int(fGreen * 255),
                          b: Int(fBlue * 255),
                          a: Double(fAlpha))
    }
}
