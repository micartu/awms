//
//  String+Convert.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

extension String {
    var toDouble: Double {
        return Double(self) ?? 0.0
    }
}

extension String {
    var toInt: Int {
        return Int(self) ?? 0
    }
}

extension String {
    var toUInt: UInt {
        return UInt(self) ?? 0
    }
}
