//
//  consts.swift
//  awms
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

struct Const {
    // string constants
    static let dqueue = "com.bearmonti.awms"
    static let domain = "awms_domain"
    static let kKeyTaskId = "task_id"
    static let kKeyItemId = "item_id"
    static let kKeyAttachId = "attach_id"
    static let kSelectedPrinterName = "selected_printer_name"
    static let kPrinterProtocol = "com.datecs.printer.fiscal.2"
    // error codes
    static let kErrNet = -1
    static let kErrTask = -2
    static let kErrUpl = -3
    static let kErrPrint = -4
    static let kErrMap = -5
}
