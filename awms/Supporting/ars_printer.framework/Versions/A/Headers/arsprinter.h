#pragma once
#include <stdint.h>
#include <pthread.h>

namespace arsp {
	// possible errors of printer
	enum print_err {
		ERR_NONE = 0,
		ERR_PRINTER_BUSY = -1,
		ERR_NO_CONNECTION = -2,
	};

	/**
	 * an abstract class which represents object printer.
	 *
	 * User must implement methods read and write (which are like posix functions).
	 * And if some data is available to read on it user **must** call function dataAvailable
	 */
	class Printer
	{
		pthread_mutex_t _mutex;

		// is data ready for reading?
		pthread_cond_t  _data_ready;
	public:
		Printer() {
			init();
		}

		virtual ~Printer() { };

		/**
		 * Initializes condition for data ready
		 * must be called from Printer's constructor!
		 * Do not forget to call it from Printer's descendant!
		 */
		void init() {
			pthread_mutex_init(&_mutex, NULL);
			pthread_cond_init(&_data_ready, NULL);
		}

		/**
		 * Method writes len bytes from data into printer and returns
		 * number of bytes successfully written to it
		 */
		virtual int write(const uint8_t* data, unsigned int len) = 0;

		/**
		 * Method reads available data from printer and
		 * returns how many were actually read
		 */
		virtual int read(uint8_t* buf, unsigned int buflen) = 0;

		/**
		 * must be called from a descendant class if it has some data to be processed
		 */
		void dataAvailable() {
			pthread_mutex_lock(&_mutex);
			pthread_cond_signal(&_data_ready);
			pthread_mutex_unlock(&_mutex);
		}

		/**
		 * waits till any data for reading will be available
		 */
		void waitForData() {
			pthread_mutex_lock(&_mutex);
			pthread_cond_wait(&_data_ready, &_mutex);
			pthread_mutex_unlock(&_mutex);
		}
	};
};
