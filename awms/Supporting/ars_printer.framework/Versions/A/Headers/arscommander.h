#pragma once
#include <stdint.h>
#include <string>
#include "fstructs.h"

// based on document:
// FISCAL PRINTER. ars.mobile Ф.Programmer’s Manual
// ver. 1.19 (March, 2017)
namespace arsp {

	class ARSCommanderImpl;
	class Printer;
	class Report;

	/**
	 * Routes commands for fiscal printer, executes them and
	 * reads the answer(s) from printer back
	 */
	class ARSCommander
	{
		ARSCommanderImpl* _cmd;

	public:
		/**
		 * @param printer a link to printer = interface implementing
		 * read/write commands (almost posix style)
		 */
		ARSCommander(Printer& printer);
		~ARSCommander();

		/**
		 * returns current seq number used by the last packet generation
		 */
		int getSeq();

		/**
		 * removes all tasks for reading/writing (needed for tests)
		 */
		void removeAllTasks();

		/**
		 * Adds a subscriber for notifications from printer
		 *
		 * @param sub a subscriber which receives notifications
		 * in form of class rtask.
		 *
		 * @details rtask will be delivered to the subscriber twice:
		 *  	1) in the moment when the command to execution was received.
		 *  		In that case data field of the subscriber will be NULL
		 *  		you shouldn't do anything with the object (**do not delete** it)
		 *
		 *  	2) when the answer from the printer was received.
		 *  		In that case the data in rtask'll be not empty (with answer packet
		 *   		but without the command, seq, etc (they are provided through methods of rtask)
		 *   		only with ans string + 9 status bytes)
		 *   		after processing the object **MUST** be deleted with delete operator, because
		 *   		in that moment it's not in the processing queue and if the user won't delete it
		 *   		there would be a memory leak!:) Maybe I should process it the base class (Report)
		 *   		but now it's not the case...
		 */
		void addSubscriber(Report* sub);

		/**
		 * Removes subscriber
		 *
		 * @param sub a subscriber which receives notifications from printer
		 * 	should be called if the subscriber doesn't need notifications
		 * 	any more
		 */
		void removeSubscriber(Report* sub);

		/**
		 * Manually sets sequence number: field seq in packet (one byte)
		 *
		 * @details the seq field will be managed automatically, but if you want to pass
		 * 	the same seq numbers twice or maybe generate command with that seq then
		 * 	you should call that command (e.g. for tests).
		 * 	But in normal mode it's not needed
		 */
		void setSequenceNumber(uint8_t num);

		/**
		 * returns current version of the library
		 */
		const char* version();

		/**
		 * Waits for writer to finish its one job.
		 * Needed for tests.
		 */
		void waitOneOperation();

		/**
		 * Called after one operation was finished (command was sent and answer was received from printer).
		 * Needed for tests.
		 */
		void processingEnded();

		/**
		 * Executes custom user's command
		 *
		 * @param command any possible number for a command which printer would understand
		 *
		 * @param param string with parameters to printer which correspond the command number's syntax
		 *
		 */
		void customCommand(int command, const char* param);

		/**
		 * opens a new non fiscal receipt.
		 *
		 * command's number: 38
		 *
		 * @param param parameter:
		 * 1 - don't print header lines
		 * \code
		 * 38 (26h) Открытие служебного чека
		 * Параметры команды:
		 * • Param – При значении 1 – не печатается клише;
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber – Текущий номер чека (1-99999999);
		 * \endcode
		 */
		void openNonFiscalReceipt(char param = 0);

		/**
		 * closes an opened non fiscal receipt (must be called after openNonFiscalReceipt)
		 *
		 * command's number: 39
		 * @param param parameter:
		 * 1 - don't print header lines
		 *
		 * \code
		 * 39 (27h) Закрытие служебного чека
		 * • Param – При значении 1 – не печатается клише;
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>
		 * • ErrorCode Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber - Текущий номер чека (1-99999999);
		 * \endcode
		 */
		void closeNonFiscalReceipt(char param = 0);

		/**
		 * rolls printer's paper (feeds paper)
		 *
		 * command's number: 44
		 *
		 * @param amount of paper to be rolled
		 * \code
		 * 44 (2Ch) Протяжка ленты
		 * Опциональные параметры:
		 * • Lines – Число протягиваемых линий от 1 до 99. По умолчанию: 1;
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void feedPaper(unsigned int amount);

		/**
		 * Printing of a free non-fiscal text
		 *
		 * command's number: 42
		 * @param p parameter corresponding to command
		 * \code
		 * 42 (2Ah) Печать свободного текста в служебном чеке
		 * • Text – текст от 0...48 символов;
		 * • Bold – 0 или 1, 1 = жирный текст; пустое поле = нормальный текст;
		 * • Italic - 0 или 1, 1 = курсив; пустое поле = нормальный текст;
		 * • DoubleH - 0 или 1, 1 = текст двойной высоты; пустое поле = нормальный текст;
		 * • Underline - 0 или 1, 1 = подчеркнутый текст; пустое поле = нормальный текст;
		 * • alignment - 0, 1 или 2. 0=выравнивание по левому краю, 1=по центру, 2=по правому краю; пустое поле = выравнивание по левому краю;
		 * • condensed - 0 или 1, 1 = уменьшенный текст; пустое поле = нормальный текст; Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void printNonFiscalText(struct nonFiscalTextParams& p);

		/**
		 *
		 * checks connection mode
		 *
		 * command's number: 45
		 * \code
		 * 45 (2Dh) Проверка связи с ПК
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void checkConnectionMode();

		/**
		 * opens fiscal receipt
		 *
		 * command's number: 48
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 48 (30h) Открытие фискального чека
		 * Параметры команды:
		 * Примечание: Система налогообложения по выбору. Обязательные параметры:
		 * OpCode – Номер оператора от 1 до 30;
		 * OpPwd – Пароль оператора (строка цифр ascii. длина 1...8);
		 * TillNmb – Номер отдела продаж 1...99999;
		 * Type
		 * o0 - "Приход";
		 * o1 - "Возврат прихода";
		 * o2 - "Расход";
		 * o3 - "Возврат расхода";
		 * o4 - "Открытие смены";
		 * o10 - "Коррекция Приход";
		 * o11 - "Коррекция Возврат прихода";
		 * o12 - "Коррекция Расход";
		 * o13 - "Коррекция Возврат расхода";
		 * Опциональные параметры:
		 * • Buyer – Адрес электронной почты или номер телефона покупателя для отправки электронной копии чека;
		 * • VatSystem – Система налогообложения в чеке. Если не указана, используется система налогообложения по умолчанию;
		 * • OperName - Имя оператора. Если OpCode=30, то имя можно менять постоянно. Если OpCode=1-29, то имя оператора можно задать только один раз при открытии смены.
		 * • OperINN – ИНН оператора. (От 10 до 12 цифр).
		 * Ответ(1): {ErrorCode}<SEP>{SlipNumber}<SEP>>{nDoc}<SEP>{nShift}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber – Текущий номер чека (1...99999999);
		 * • nDoc – Порядковый номер фискального документа для текущей смены (1...99999999);
		 * • nShift – Текущий номер смены (1...9999);
		 * Ответ(2): {ErrorCode}<SEP>{nFD}<SEP>{SignFP}<SEP>{nShift}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nFD – Номер фискального документа - ФД (1...99999999).;
		 * • SignFP – Фискальный признак документа - ФП (1...99999999).;
		 * • nShift – Текущий номер смены (1...9999);
		 * \endcode
		 */
		void openFiscalReceipt(struct fiscalReceiptParams& p);

		/**
		 * registers a sale
		 *
		 * command's number: 49
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 49 (31h) Регистрация продажи
		 * Обязательные параметры: {PluName},{TaxGr},{Price}
		 * • PluName – Наименование товара (до 64 символов);
		 * • TaxGr – Ставка НДС ( цифры '1'...'6' );
		 * • Price – Цена товара ( 0.00...9999999.99 ). Со знаком минус '-' для операций возврата;
		 * • Department – Номер отдела (0...99). Если '0' – без отдела;
		 * Опциональные параметры: {Quantity},{DiscountType},{DiscountValue}
		 * • Quantity – Количество товара ( 0.001...99999.999 ). По умолчанию: 1.000;
		 * Примечание: !!! Максимальное значение {Price} * {Quantity} равно 9999999.99 !!!
		 * DiscountType – Тип скидки. По умолчанию: 0;
		 * o'0' – без скидки;
		 * o'1' – наценка в процентах;
		 * o'2' – скидка в процентах;
		 * DiscountValue - Значение;
		 * oПроцент скидки ( 0.00...100.00 ) – для операций в процентах;
		 * Sign – Признак предмета расчета (цифры '1'...'13')
		 * o'1' – Товар;
		 * o'2' – Подакцизный товар;
		 * o'3' – Работа;
		 * o'4' – Услуга;
		 * o'5' – Ставка азартной игры;
		 * o'6' – Выигрыш АИ;
		 * o'7' – Лотерейный билет;
		 * o'8' – Выигрыш лотереи;
		 * o'9' – Предоставление РИД;
		 * o'10' – Платеж;
		 * o'11' – Агентское вознаграждение;
		 * o'12' – Составной предмет расчета;
		 * o'13' – Иной предмет расчета;
		 * • SignPay – Только для Синтаксиса 2! Признак способа расчета (цифры '1'...'7');
		 * o'1' – Предоплата 100%;
		 * o'2' – Предоплата;
		 * o'3' – Аванс;
		 * o'4' – Полный расчет;
		 * o'5' – Частичный расчет и кредит; o'6' – Передача в кредит;
		 * o'7' – Оплата кредита;
		 * Примечание: Если {DiscountType} не равно 0, {DiscountValue} должно содержать значение. Если {DiscountType} равно 0 или пустое, {DiscountValue} должно быть пустым;
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber – Текущий номер чека (1...99999999);
		 * \endcode
		 */
		void saleRegistration(struct saleRegistrationParams& p);

		/**
		 * returns the active VAT rates
		 *
		 * command's number: 50
		 * \code
		 * 50 (32h) Возврат активных ставок НДС
		 * Ответ: {ErrorCode}<SEP>{nZreport}<SEP>{Tax1}<SEP>{Tax2}<SEP>{Tax3}<SEP>
		 * {Tax4}<SEP>{Tax5}<SEP>{Tax6}<SEP>{EntDate}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nZreport – Номер первого Отчета о закрытии смены (1÷1830);
		 * • TaxX – Ставка налоговой группы ( 0.00÷99.99 = налоговые ставки, 100.00 = без ставки, 100.01 = запрещена );
		 * • EntDate – Дата ввода (формат: DD-MM-YY );
		 * \endcode
		 */
		void returnVAT();

		/**
		 * shows/prints Subtotal
		 *
		 * command's number: 51
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 51 (33h) Промежуточный итог
		 * Print – Печать промежуточного итога. По умолчанию: 0;
		 * o'0' – Не печатать;
		 * o'1' – Печать суммы промежуточного итога;
		 * Display – Показать промежуточный итог на экране клиенту. По умолчанию: 0;
		 * o'0' – Не показывать;
		 * o'1' – Показать промежуточный итог на экране;
		 * • DiscountType – Должно быть пустым!
		 * • DiscountValue - Должно быть пустым!
		 * Примечание: Если {DiscountType} не равен 0, {DiscountValue} должен содержать значение. Если {DiscountType} равен 0 ил пустой, {DiscountValue} должно быть пустым;
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>{Subtotal}<SEP>{Sum1}<SEP> {Sum2}<SEP>{Sum3}<SEP>{Sum4}<SEP>{Sum5}<SEP>{Sum6}<SEP>
		 * ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена; SlipNumber – Текущий номер чека (1...9999999);
		 * Subtotal – Промежуточный итог чека ( 0.00...9999999.99 );
		 * SumX – Оборот чека по налоговой ставке НДС X ( 0.00...9999999.99 );
		 * \endcode
		 */
		void subtotal(struct subtotalParams& p);

		/**
		 * calculates payment
		 *
		 * command's number: 53
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 53 (35h) Оплата и подсчет итоговой суммы (ИТОГ)
		 * Параметры команды:
		 * PaidMode – Тип оплаты;
		 * o'0' – Наличные;
		 * o'1' – Электронными;
		 * o'2' – Кредит;
		 * o'3' – Ваучер
		 * o'4' – Купон;
		 * o'5' – Оплата No5;
		 * o'6' – Оплата в иностранной валюте;
		 * PaidMode – Тип оплаты;
		 * o'0' – Наличные;
		 * o'1' – Электронными;
		 * o'2' – Предварительная оплата (АВАНС);
		 * o'3' – Последующая оплата (КРЕДИТ); o'4' – Иная форма оплаты;
		 * o'5' – Оплата No5;
		 * o'6' – Оплата в иностранной валюте;
		 * Amount – Сумма оплаты ( 0.00÷9999999.99);
		 * PaidMode – Тип оплаты;
		 * o'6' – иностранная валюта;
		 * Amount – Сумма к оплате ( 0.00÷9999999.99);
		 * Change – Валюта сдачи. По умолчанию = '0'; o'0' – Основная валюта;
		 * o'1' – Иностранная валюта (Запрещено !);
		 * Ответ: {ErrorCode}<SEP>{Status}<SEP>{Amount}<SEP>
		 * ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Status – Статус;
		 * o'D' – Выводится если сумма оплаты меньше или равно сумме чека. Остаточная сумма к оплате (может быть равна 0) выводится в Amount.
		 * o'R' – Выводится если сумма оплаты больше, чем сумма чека. Сумма сдачи выводится в Amount;
		 * • Amount – Остаточная сумма к оплате (может быть 0) или сумма сдачи ( 0.00÷9999999.99);
		 * \endcode
		 */
		void paymentsCalculation(struct paymentsCalculationParams& p);

		/**
		 * prints free fiscal text
		 *
		 * command's number: 54
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 54 (36h) Печать свободного текста в фискальном чеке
		 * Опциональные параметры:
		 * • Text – текст от 0...48 символов;
		 * • Bold – 0 или 1, 1 = жирный текст; пустое поле = нормальный текст;
		 * • Italic - 0 или 1, 1 = курсив; пустое поле = нормальный текст;
		 * • DoubleH - 0 или 1, 1 = текст двойной высоты; пустое поле = нормальный текст;
		 * • Underline - 0 или 1, 1 = подчеркнутый текст; пустое поле = нормальный текст;
		 * • alignment - 0, 1 или 2. 0=выравнивание по левому краю, 1=по центру, 2=по правому краю; пустое поле = выравнивание по левому краю;
		 * • condensed - 0 или 1, 1 = уменьшенный текст; пустое поле = нормальный текст; Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void printFreeFiscalText(struct nonFiscalTextParams& p);

		/**
		 * closes an opened fiscal receipt (must be called after openFiscalReceipt)
		 *
		 * command's number: 56
		 * \code
		 * 56 (38h) Закрытие фискального чека
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>{nFD}<SEP>{SignFP}<SEP>
		 * {nDoc}<SEP>{nShift}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber – Текущий номер чека (1...99999999);
		 * • nFD – Номер фискального документа - ФД (1...99999999).;
		 * • SignFP – Фискальный признак документа - ФП (1...99999999).;
		 * • nDoc – Порядковый номер фискального документа для текущей смены (1...99999999);
		 * • nShift – Текущий номер смены (1...9999);
		 * \endcode
		 */
		void closeFiscalReceipt();

		/**
		 * prints free fiscal text
		 *
		 * command's number: 57
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 57 (39h) Ввод Номера мобильного телефона/Адреса электронной почты клиента в фискальном чеке/Признаки и параметры Агентов (для ФФД 1.05)
		 * Обязательные параметры:
		 * • Type – Тип данных;
		 * o'0' – Номер мобильного телефона;
		 * o'1' – Адрес электронной почты;
		 * o'2' – Отмена данных (удалить введенные ранее данные);
		 * o'3' – Тип коррекции;
		 * o'4' – Основание для коррекции;
		 * o'5' – Дата документа;
		 * o'6' – Номер документа
		 * o'1005' – Адрес оператора перевода;
		 * o'1016' – ИНН оператора перевода;
		 * o'1026' – Наименование оператора перевода;
		 * o'1044' – Операция платежного агента;
		 * o'1057' – Признак агента;
		 * o'1073' – Телефон платежного агента;
		 * o'1074' – Телефон оператора по приему платежей; o'1075' – Телефон оператора перевода;
		 * o'1171' – Телефон поставщика;
		 * Data – в зависимости от Type;
		 * oType=0 – Номер мобильного телефона. Строка в формате ASCII до 16 символов;
		 * oType=1 – Адрес электронной почты. Строка в формате ASCII до 64 символов;
		 * oType=2 – Не имеет значения (также приравнивается к этому значению, если параметр не указан);
		 * oType=3 – Тип коррекции: 1) по предписанию; 2) самостоятельно;
		 * oType=4 – Основание для коррекции 2 строки в формате ASCII каждая до 128 символов;
		 * oType=5 – Дата документа. Значение в формате ДД.ММ.ГГ.;
		 * oType=6 – Номер документа. Строка в формате ASCII до 28 символов;
		 * oType=1005 – Адрес оператора перевода. 2 строки в формате ASCII каждая до 128 символов;
		 * oType=1016 – ИНН оператора перевода. Для банковских платежных агентов (субагентов).10-12 цифр.
		 * oType=1026 – Наименование оператора перевода. Для банковских платежных агентов (субагентов). Строка в формате ASCII до 64 символов;
		 * oType=1044 – Операция платежного агента. Для банковских платежных агентов (субагентов). Строка в формате ASCII до 24 символов;
		 * oType=1073 – Телефон платежного агента. Для платежного агента, платежного субагентаТелефон оператора по приему платежей. При осуществлении деятельности платежного агента и платежного субагента. банк.платежного агента, банк.платежного субагента.
		 * oType=1074 – Телефон оператора по приему платежей. При осуществлении деятельности платежного агента и платежного субагента.
		 * oType=1075 – Телефон оператора перевода. Для оператора по переводу денежных средств.
		 * oType=1171 – Телефон поставщика. Для платежного агента, платежного субагента.
		 *
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void writeGSMorEmailForFiscalReceipt(struct gsmEmailParams& p);

		/**
		 * registers a sale of a programmed item
		 *
		 * command's number: 58
		 *
		 * @param p parameter corresponding to command
		 * \code
		 * 58 (3Ah) Регистрация продажи запрограммированного товара
		 * Обязательные параметры: {PluCode}
		 * • PluCode – Код товара (1...3000). Со знаком минус '-' для операций возврата;
		 * Опциональные параметры: {Quantity}, {DiscountType}, {DiscountValue}
		 * • Quantity – Количество товара ( 0.001...99999.999 ). По умолчанию: 1.000;
		 * • Price – Цена товара ( 0.00...9999999.99 );
		 * Примечание: !!! Максимальное значение {Price} * {Quantity} равно 9999999.99 !!!
		 * • •
		 * DiscountType – Тип скидки. По умолчанию: 0;
		 * o'0' – нет скидки;
		 * o'1' – наценка в процентах;
		 * o'2' – скидка в процентах;
		 * DiscountValue - Значение;
		 * oПроцентная ставка ( 0.00...100.00 ); - для операций с процентной ставкой;
		 * Примечание: Если {DiscountType} не равняется нулю, то {DiscountValue} должно содержать значение. Если {DiscountType} равняется нулю или не имеет значения, то {DiscountValue} также не должно иметь значения;
		 * Операции возврата производятся указанием знака минус '-' перед PluCode! Для проведения операции возврата, параметр 'Price' должна быть идентичной цены продажи.
		 * Ответ: {ErrorCode}<SEP>{SlipNumber}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SlipNumber – Текущий номер чека (1...99999999);
		 * \endcode
		 */
		void saleRegistrationProgIt(struct saleRegistrationParams& p);

		/**
		 * cancels fiscal receipt
		 *
		 * command's number: 60
		 * \code
		 * 60 (3Ch) Отмена фискального чека
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void cancelFiscalReceipt();

		/**
		 * sets date and time of the fiscal printer
		 *
		 * command's number: 61
		 *
		 * @param p date and time to be installed
		 * \code
		 * 61 (3Dh) Установка даты и времени
		 * Обязательные параметры:
		 * DateTime – Дата и время в формате: DD-MM-YY hh:mm:ss»;
		 * oDD – День;
		 * oMM – Месяц;
		 * oYY – Год;
		 * ohh – Час;
		 * omm – Минуты;
		 * oss – Секунды;
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void setDateTime(struct dateTime& p);

		/**
		 * reads date and time of the fiscal printer
		 *
		 * command's number: 62
		 *
		 * \code
		 * 62 (3Eh) Считывание даты и времени
		 * Ответ: {ErrorCode}<SEP>{DateTime}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • DateTime – Дата и время в формате: «DD-MM-YY hh:mm:ss»;
		 * oDD – День;
		 * oMM – Месяц;
		 * oYY – Год;
		 * ohh – Час;
		 * omm – Минуты;
		 * oss – Секунды;
		 * \endcode
		 */
		void readDateTime();

		/**
		 * asks for information about last fiscal entry
		 *
		 * command's number: 64
		 *
		 * @param p type and record type of the info
		 * \code
		 * Type – Тип возвращаемых данных. По умолчанию: 0;
		 * o0 – Обороты по налоговым ставкам включая КОРРЕКЦИИ;
		 * o1 – Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ;
		 * o2 – Обороты по налоговым ставкам, только по КОРРЕКЦИЯМ;
		 * o3 – Сумма налогов по налоговым ставкам, только по КОРРЕКЦИЯМ;
		 * RecType – Тип чека. По умолчанию: 0;
		 * o'0' – «Приход»;
		 * o'1' – «Возврат прихода»;
		 * o'2' – «Расход»;
		 * o'3' – «Возврат расхода»;
		 * Ответ: {ErrorCode}<SEP>{nRep}<SEP>{Sum1}<SEP>{Sum2}<SEP>{Sum3}<SEP>
		 * {Sum4}<SEP>{Sum5}<SEP>{Sum6}<SEP>{Date}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nRep – Номер Отчета о закрытии смены (1÷1830);
		 * • SumX – Оборот или сумма НДС группы НДС X определенного Type ( 0.00÷9999999.99);
		 * • Date – Дата фискальной записи в формате: DD-MM-YY;
		 * \endcode
		 */
		void lastFiscalEntryInfo(struct lastFiscalEntryParams& p);

		/**
		 * returns info about daily taxation
		 *
		 * command's number: 65
		 *
		 * @param type Type of returned data. Default: 0;
		 * \code
		 * 65 (41h) Информация о налогах за текущий день
		 * Type – Тип возвращаемых данных. По умолчанию: 0
		 * o0 – Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям «Приход» и «Коррекция Прихода»;
		 * o1 – Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Приход" и «Коррекция Прихода»;
		 * o2 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Возврат прихода" и «Коррекция Возврата Прихода»;
		 * o3 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Возврат прихода" и «Коррекция Возврата Прихода»;
		 * o4 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Расход" и «Коррекция Расхода»;
		 * o5 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Расход" и «Коррекция Расхода»;
		 * o6 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Возврат расхода" и «Коррекция Возврата Расхода»;
		 * o7 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Возврат расхода" и «Коррекция Возврата Расхода»;
		 * o8 Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Приход";
		 * o9 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Приход";
		 * o10 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Возврат прихода";
		 * o11 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Возврат прихода";
		 * o12 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Расход";
		 * o13 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Расход";
		 * o14 - Обороты по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Возврат расхода";
		 * o15 - Сумма налогов по налоговым ставкам включая КОРРЕКЦИИ, по операциям "Коррекция Возврат расхода";
		 * Ответ: {ErrorCode}<SEP>{nRep}<SEP>{Sum1}<SEP>{Sum2}<SEP>{Sum3}<SEP> {Sum4}<SEP>{Sum5}<SEP>{Sum6}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nRep – Номер смены (1÷1830);
		 * • SumX – Оборот или сумма налогов по типу возвращаемых данных Type ( 0.00÷9999999.99);
		 * \endcode
		 */
		void dailyTaxationInfo(char type);

		/**
		 * returns info about remaining entries for Z-Reports
		 *
		 * command's number: 68
		 * \code
		 * 68 (44h) Количество доступных записей смен в ФП
		 * Параметры команды: нет
		 * Ответ: {ErrorCode}<SEP>{ReportsLeft}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • ReportsLeft – Количество доступных записей смен в ФП (0÷1830).
		 * \endcode
		 */
		void remainingEntriesZReportsFM();

		/**
		 * makes X- or Z- reports
		 *
		 * command's number: 69
		 *
		 * @param type of the report to be produced
		 * \code
		 * 69 (45h) Отчеты
		 * Параметры команды: {ReportType}<SEP>
		 * Обязательные параметры:
		 * • ReportType – Тип отчета;
		 * o'X' – Отчет без закрытия смены;
		 * o'Z' – Отчет о закрытии смены;
		 * Ответ: {ErrorCode}<SEP>{nRep}<SEP>{nFD}<SEP>{SignFP}<SEP>{Sum1}<SEP>{Sum2}<SEP> {Sum3}<SEP>{Sum4}<SEP>{Sum5}<SEP>{Sum6}<SEP>{Ret1}<SEP>{Ret2}<SEP> {Ret3}<SEP>{Ret4}<SEP>{Ret5}<SEP>{Ret6}<SEP>{Purch1}<SEP>{Purch2}<SEP> {Purch3}<SEP>{Purch4}<SEP>{Purch5}<SEP>{Purch6}<SEP>{RetPurch1}<SEP>
		 * {RetPurch2}<SEP>{RetPurch3}<SEP>{RetPurch4}<SEP>{RetPurch5}<SEP>{RetPurch6}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nRep – Номер смены (1÷1830);
		 * • nFD – Номер фискального документа - ФД (1...99999999) для отчетов о закрытии смены. Для отчетов без закрытия смены поле пустое;
		 * • SignFP – Фискальный признак документа - ФП (1...99999999) для отчетов о закрытии смены. Для отчетов без закрытия смены поле пустое;
		 * • SumX – Оборот по группе налоговых ставок X по операциям "Приход" ( 0.00÷9999999.99);
		 * • RetX - Оборот по группе налоговых ставок X по операциям "Возврат прихода" ( 0.00÷9999999.99);
		 * • PurchX - Оборот по группе налоговых ставок X по операциям "Расход" ( 0.00÷9999999.99);
		 * • RetPurchX- Оборот по группе налоговых ставок X по операциям "Возврат расхода" (0.00÷9999999.99);
		 * \endcode
		 */
		void report(const char type);

		/**
		 * cash in and out operations
		 *
		 * command's number: 70
		 *
		 * @param type of operation:
		 * o '0' - cash in;
		 * o '1' - cash out;
		 * o '2' - cash in - (foreign currency);
		 * o '3' - cash out - (foreign currency);
		 *
		 * @param amount the sum (0.00÷9999999.99); If Amount=0, the only Answer is returned,
		 * and receipt does not print.
		 * \code
		 * 70 (46h) Операции внесения и выплаты
		 * Обязательные параметры:
		 * • Type – Тип операции;
		 * o'0' – Внесение; o'1' – Выплата;
		 * Опциональные параметры:
		 * • Amount – Сумма (0.00÷9999999.99); если Amount=0, выводится только ответ без печати чека.
		 * Ответ: {ErrorCode}<SEP>{CashSum}<SEP>{CashIn}<SEP>{CashOut}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • CashSum – Сумма, внесенная в денежный ящик ( 0.00÷9999999.99 );
		 * • CashIn – Итоговая сумма операций внесения (0.00÷9999999.99);
		 * • CashOut – Итоговая сумма операций выплаты (0.00÷9999999.99).
		 * \endcode
		 */
		void cashInOut(char type, double amount);

		/**
		 * prints diagnostic information on printer's paper
		 *
		 * command's number: 71
		 *
		 * @param type of the information printed;
		 * Devices with LAN module:
		 * o '0' - general information about the device;
		 * o '1' - test of the modem;
		 * o '2' - reserved;
		 * o '3' - get LAN settings;
		 * o '4' - test of the LAN interface;
		 * o '5' - test of the ФН ("фискальный накопитель");
		 * o '6' - get ФН ("фискальный накопитель") parameters;
		 * Devices with WiFi module:
		 * o '0' - general information about the device;
		 * o '1' - test of the modem;
		 * o '2' - test of the WiFi module;
		 * o '3' - get wireless access points of the WiFi module;
		 * o '31'- print wireless access points of the WiFi module;
		 * o '4' - reserved;
		 * o '5' - test of the ФН ("фискальный накопитель");
		 * o '6' - get ФН ("фискальный накопитель") parameters;
		 *
		 * \code
		 * 71 (47h) Печать диагностической информации
		 * Опциональные параметры:
		 * • InfoType – Тип печатаемой информации. По умолчанию: 0;
		 * Для ККТ с интерфейсом LAN:
		 * o '0' – Общая диагностическая информация об устройстве;
		 * o '1' – Тест модема;
		 * o '2' – Зарезервировано;
		 * o '3' – Получение настроек LAN интерфейса;
		 * o '4' – Тест LAN интерфейса;
		 * o '5' – Тест ФН;
		 * o '6' – Получение параметров ФН;
		 * Для ККТ с интерфейсом WiFi:
		 * o '0' – Общая диагностическая информация об устройстве;
		 * o '1' – Тест модема;
		 * o '2' – Тест модуля WiFi;
		 * o '3' – Получение списка точек доступа WiFi;
		 * o '31' – Печать списка точек доступа WiFi;
		 * o '4' – Зарезервировано;
		 * o'5'–ТестФН;
		 * o '6' – Получение параметров ФН;
		 * Ответ(1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ(2): {0}<SEP>{LAN_IP}<SEP>{LAN_NetMask}<SEP>{LAN_Gateway}<SEP> {LAN_PriDNS}<SEP>{LAN_SecDNS}<SEP>{DHCPenable}<SEP>
		 * • LAN_IP – IP адрес в формате "xxx.xxx.xxx.xxx";
		 * • LAN_NetMask – Маска подсети в формате "xxx.xxx.xxx.xxx";
		 * • LAN_Gateway – Шлюз в формате "xxx.xxx.xxx.xxx";
		 * • LAN_PriDNS – Основной DNS сервер в формате "xxx.xxx.xxx.xxx";
		 * • LAN_SecDNS - Основной DNS сервер в формате "xxx.xxx.xxx.xxx";
		 * • DHCPenable - 1 - DHCP включен, 0- DHCP - выключен;
		 * Ответ(3): {0}<SEP>>{nAP,AP_SSID,Security}<SEP>....
		 * • nAP,AP_SSID,Security – Информация об одной точки доступа: o nAP – десятичный порядковый номер;
		 * o AP_SSID – имя SSID;
		 * o Security – уровень безопасности;
		 * Ответ(4): {0}<SEP>{Version}<SEP>{Number}<SEP>{Phase}<SEP>{ShiftStatus}<SEP> {WarningFlags}<SEP>{DateDeadline}<SEP>{RegLeft}<SEP>{ShiftNumber}<SEP> {ReceiptsCount}<SEP>{LastDocIssued}<SEP>{FirstDocUnsent}<SEP>
		 * • Version – Версия микропрограммы ФН, до 16 символов в формате ascii;
		 * • Number – Заводской номер ФН, до 16 символов в формате ascii;
		 * • Phase – Фаза работы ФН;
		 * • ShiftStatus – Статус смены;
		 * • WarningFlags – Флаги предупреждения;
		 * • DateDeadline – Дата окончания работы ФН, в формате "DD-MM-YYYY";
		 * • RegLeft – Количество доступных перерегистраций;
		 * • ShiftNumber – Номер текущей смены;
		 * • ReceiptsCount – Количество фискальных документов для текущей смены;
		 * • LastDocIssued – Номер последнего фискального документа;
		 * • FirstDocUnsent – Номер первого неотправленного в ОФД документа. Если документов к отправке нет, то поле будет пустым.
		 * Информация по выводимым параметрам доступна в утвержденном документе "Описание интерфейса фискального накопителя".
		 * \endcode
		 */
		void printDiagnostic(char type);

		/**
		 * changes fiscal parameters
		 *
		 * command's number: 72
		 *
		 * @param p fiscalization params
		 * \code
		 * 72 (48h) Фискализация (Ввод ИНН и РН ККТ)
		 * Обязательные параметры:
		 * • SerialNumber – Заводской номер ЗН ККТ - 10 цифр;
		 * • TAXnumber – ИНН (10 или 12 цифр);
		 * • REGnumber – Регистрационный номер ККТ (РН ККТ) - 16 цифр;
		 * • InspPssword – Пароль Системного администратора (до 8 цифр);
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void fiscalization(struct fiscalizationParams& p);

		/**
		 * reads the status of the printer
		 *
		 * command's number: 74
		 * \code
		 * 74 (4Ah) Считывание статуса
		 * Параметры команды: нет
		 * Ответ: {ErrorCode}<SEP>{StatusBytes}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • StatusBytes – Байты статуса (см. описание байтов статуса ККТ);
		 * \endcode
		 */
		void readStatus();

		/**
		 * reads the status of the current or last receipt
		 *
		 * command's number: 76
		 * \code
		 * 76 (4Ch) Статус фискального чека
		 * Параметры команды: нет
		 * Ответ: {ErrorCode}<SEP>{IsOpen}<SEP>{Number}<SEP>{Items}<SEP>{Amount}<SEP>{Paid}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • IsOpen – Текущий статус чека;
		 * o '0' – Чек закрыт;
		 * o '1' – Кассовый чек Прихода открыт;
		 * o '2' – Кассовый чек Возврат Прихода открыт;
		 * o '3' – Кассовый чек Расхода открыт;
		 * o '4' – Кассовый чек Возврат Расхода открыт;
		 * o '5' – Кассовый чек Коррекции Прихода открыт;
		 * o '6' – Кассовый чек Коррекции Возврата Прихода открыт;
		 * o '7' – Кассовый чек Коррекции Расхода открыт;
		 * o '8' – Кассовый чек Коррекции Возврата Расхода открыт;
		 * o '9' – Открыт Служебный чек;
		 * • Number – Номер текущего или последнего фискального чека (1-9999999);
		 * • Items – Количество продаж в текущем или в последнем фискальном чеке(1-999);
		 * • Amount – Сумма текущего или последнего фискального чека (0.00-9999999.99);
		 * • Paid – Сумма оплаты текущего или последнего фискального чека(0.00-9999999.99).
		 * \endcode
		 */
		void readReceiptStatus();

		/**
		 * reads the status of FN and OFD
		 *
		 * command's number: 77
		 * \code
		 * 77 (4Dh) Статус ФН и статус передачи данных в ОФД
		 * Параметры команды: нет
		 * Ответ: {ErrorCode}<SEP>{StatusShift}<SEP>{End24hours}<SEP>{ShiftNumber}<SEP>
		 * {DocNumber}<SEP>{UnsentDocCount}<SEP>{FirstUnsent}<SEP>{DateTimeUnsent}<SEP>
		 * ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * StatusShift - 1 – Смена открыта, 0 – Смена закрыта;
		 * End24hours - 1 – Прошло 24 часа с начала открытия смены, 0 – Прошло менее 24 часов с начала открытия смены;
		 * ShiftNumber – Номер смены;
		 * • DocNumber – Номер фискального документа. Если смена закрыта, указывается номер последнего фискального документа. ( 0 – если это первая смена). Если смена открыта, но нет фискальных документов - 0. В иных случаях, номер последнего фискального документа, который был сгенерирован в ФН;
		 * • UnsentDocCount – Количество неотправленных документов из ФН (Отправка документов из ФН происходит по принципу FIFO);
		 * • FirstUnsent – Номер первого неотправленного документа (если UnsentDocCount равно 0, то данное поле будет пустым).;
		 * • DateTimeUnsent – Дата и время первого неотправленного документа в формате "ДД.MM.ГГ ЧЧ:ММ" (если UnsentDocCount равно 0, то данное поле будет пустым).
		 * \endcode
		 */
		void readFNStatus();

		/**
		 * plays sound
		 *
		 * command's number: 80
		 * \code
		 * 80 (50h) Воспроизведение звука
		 * Обязательные параметры:
		 * • Hz - Частота (0...65535);
		 * • mSec – Время в милисекундах (0...65535);
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void playSound(uint16_t hz, uint16_t msec);

		/**
		 * switches on/off printing
		 * mode = 0 - on, 1 - off
		 *
		 * command's number: 81
		 * \code
		 * 81 (51h) Отключение печати документов
		 * Обязательные параметры:
		 * • Value – Тип операции;
		 * o '0' – Включение печати чека;
		 * o '1' – Выключение печати чека; Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void switchOffPrint(char mode);

		/**
		 * programs VAT
		 *
		 * command's number: 83
		 *
		 * @param p params to be programmed
		 * \code
		 * 83 (53h) Программирование ставок НДС
		 * Обязательные параметры:
		 * TaxX – Значение ставки НДС X; o 0.00...99.99 - разрешена;
		 * o 100.00 – без налога;
		 * o 100.01 - запрещена;
		 * Примечание: Ставки {Tax1}-{Tax4} должны обязательно содержать значения ( 0.00÷99.99 или 100.00 или 100.01);
		 * Ставки {Tax5}-{Tax6} могут быть пустыми или содержать значения ( 0.00÷99.99 или 100.00 или 100.01).
		 * Decimal_point;
		 * o '0' – работа с ценами в целых числах ( Запрещено! );
		 * o '2' - работа с дробными ценами;
		 * Ответ: {ErrorCode}<SEP>{RemainingChanges}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • RemainingChanges – Количество доступных изменений ставок НДС (1...30);
		 * \endcode
		 */
		void programVAT(struct programVATParams& p);

		/**
		 * prints a bar code
		 *
		 * command's number: 84
		 *
		 * @param p bar code parameters
		 * \code
		 * 84 (54h) Печать штрих-кода
		 * Type – Тип штрих-кода;
		 * o '1' - EAN8. {Data} должен содержать 8 цифр;
		 * o '2' - EAN13. {Data} должен содержать 13 цифр;
		 * o '3' - Code128. {Data} должен содержать символы кода ASCII между 32 и 127. Длина
		 * параметра {Data} от 3-х до 31 символа;
		 * o '4' - QRcode. {Data} должен содержать символы кода ASCII между 32 и 127. Длина
		 * параметра {Data} от 3-х до 279 символа; Data – Данные штрих-кода;
		 * ▪ Длина параметра {Data} зависит от типа штрих-кода.
		 * QrcodeSize – Множитель количества точек ( 3÷10 ). По умолчанию: 5;
		 * Примечание: Печать штрих-кода возможна только если открыт фискальный или нефискальный чек.
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Примечание: Параметры печати штрих-кода (CMD255 - BarCodeHeight и BarcodeName) должны быть запрограммированы заранее.
		 * \endcode
		 */
		void printBarCode(struct barCodeParams& p);

		/**
		 * returns the date of the last fiscal record
		 *
		 * command's number: 86
		 * \code
		 * 86 (56h) Дата последней фискальной записи
		 * Параметры команды: Нет
		 * Ответ: {ErrorCode}<SEP>{DateTime}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • DateTime – Дата и время последней фискальной записи в формате: DD-MM-YYYY hh:mm:ss;
		 * \endcode
		 */
		void lastFiscalDate();

		/**
		 * asks item's groups information
		 *
		 * command's number: 87
		 *
		 * @param group number of item group (1-99) or negative => prints info about all groups
		 * \code
		 * 87 (58h) Получение информации по группам товаров
		 *  Обязательные параметры:
		 * • ItemGroup – Номер группы товаров, если параметр не введен, печатается отчет по товарным группам.
		 * Ответ:{ErrorCode}<SEP>{TotSales}<SEP>{TotSum}<SEP>{Name}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • TotSales – Количество продаж по данной группе товаров за день;
		 * • TotSum – Накопленная сумма по данной группе товаров за день;
		 * • Name – Наименование группы товаров;
		 * \endcode
		 */
		void itemGroupInfo(char group);

		/**
		 * asks about department's information
		 *
		 * command's number: 88
		 *
		 * @param dep number of a department (1÷99). If zero or negative - printing of Department report;
		 *
		 * @param rec_type type of receipts. Default: 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 * \code
		 * 88 (58h) Получение информации по отделу
		 * Опциональные параметры:
		 * • Department – Номер отдела (1-99), если параметр не введен, печатается отчет по всем отделам;
		 * • RecType – Тип кассовых операций(чеков). По умолчанию значение 0.
		 * o 0 - Приход
		 * o 1 – Возврат прихода o 2 – Расход
		 * o 3 – Возврат расхода
		 * Ответ: {ErrorCode}<SEP>{TaxGr}<SEP>{Price}<SEP>{TotQty}<SEP>{TotSum}<SEP>{Name}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • TaxGr – группа налоговой ставки (1-6);
		 * • Price – Максимальная сумма операции по отделу (0.00-9999999.99);
		 * • TotQty - Количество проданного товара в данном отделе в текущую смену по типу
		 * кассовых операций RecType (0.001-999999.999);
		 * • TotSum- Сумма продаж в данном отделе в текущую смену по типу кассовых
		 * операций(чеков) RecType (0.00-999999999.99);
		 * • Name – Наименование отдела (до 32 символов);
		 * \endcode
		 */
		void departmentInfo(char dep, char rec_type);

		/**
		 * tests fiscal memory (read or write)
		 *
		 * command's number: 89
		 *
		 * @param write type of test
		 * o 0 - Read test.
		 * o 1 - Write and read test;
		 * \code
		 * 89 (59h) Проверка фискальной памяти
		 * Параметры команды: {Write}<SEP> Опциональные параметры:
		 * • Write – Метод проверки. По умолчанию значение 0. o 0 – Тест на чтение
		 * o 1 – Тест на запись и чтение
		 * Ответ: {ErrorCode}<SEP>{Records}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Records – Количество свободных записей (0-16);
		 * \endcode
		 */
		void fiscalMemoryTest(char write);

		/**
		 * shows device's diagnostic
		 *
		 * command's number: 90
		 *
		 * @param type of diagnostic for detailed reference see documentation to printer
		 * \code
		 * 90 (5Ah) Диагностическая информация
		 * Ответ(1): {ErrorCode}<SEP>{Name}<SEP>{FwRev}<SEP>{FwDate}<SEP> {FwTime}<SEP>{Checksum}<SEP>{Sw}<SEP>{SerialNumber}<SEP>
		 * Ответ(2): {Name},{FwRev} {Sp} {FwDate} {Sp} {FwTime},{Checksum},{Sw},{SerialNumber}
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Name – Наименование устройства (до 32 символов);
		 * • FwRev – Версия ПО. 6 символов;
		 * • Sp – Пробел. 1 символ;
		 * • FwDate – Дата ПО. DDMMMYY. 7 символов;
		 * • FwTime – Время ПО. hhmm. 4 символа.
		 * • Checksum – Контрольная сумма ПО. 4 символа;
		 * • Sw - Переключатели с Sw1 по Sw8. 8 символов (не используется в этом устройстве,
		 * всегда 00000000);
		 * • SerialNumber – Заводской номер устройства (Десять цифр);
		 * \endcode
		 */
		void showDiagnostic(int type);

		/**
		 * programs serial number of printer
		 *
		 * command's number: 91
		 *
		 * @param serialNumber new serial number of the printer
		 * \code
		 * 91 (5Bh) Программирование заводского номера
		 * Параметры команды: {SerialNumber}<SEP> Обязательные параметры:
		 * • SerialNumber – Заводской номер десять цифр;
		 * Ответ: {ErrorCode}<SEP>{Country}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Country – Наименование страны;
		 * \endcode
		 */
		void programSerialNumber(const char* serialNumber);

		/**
		 * prints separate line
		 *
		 * command's number: 92
		 *
		 * @param type of the line to be printed
		 * \code
		 * 92 (5Ch) Печать разделительной линии
		 * Обязательные параметры:
		 * • Type – Тип разделительной линии.
		 * o '1' – Разделительная линия символами '-';
		 * o '2' - Разделительная линия символами '-' и ' ';
		 * o '3' - Разделительная линия символами '=';
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void printSeparateLine(enum separateLineType type);

		/**
		 * fiscal memory report
		 *
		 * command's number: 94
		 *
		 * @param p parameters of report
		 * \code
		 * 94 (5Еh) Сменный отчет по датам
		 * Параметры команды: {Type}<SEP>{Start}<SEP>{End}<SEP> Обязательные параметры:
		 * •
		 * Type – Тип отчета;
		 * o '0' - Сокращенный;
		 * o '1' - Полный;
		 * o '10' – Сокращенный в файл;
		 * o '11' – Полный в файл;
		 * o '3' – Чтение следующей строки отчета 10 или 11 как текст;
		 * Опциональные параметры:
		 * Start – Начальная дата (формат "DD-MM-YY"). По умолчанию: Дата ввода ИНН и РН ККТ;
		 * • End – Конечная дата (формат "DD-MM-YY"). По умолчанию: Текущая дата;
		 * Ответ (1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ (2): {ErrorCode}<SEP>{TextData}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • TextData – Текстовый документ (до 48 символов);
		 * \endcode
		 */
		void fiscalMemoryReport(struct fiscalMemoryReportParams& p);

		/**
		 * fiscal memory z-report or shift status
		 *
		 * command's number: 95
		 *
		 * @param p parameters of report
		 * \code
		 * 95 (5Fh) Отчет по номерам Отчетов о закрытии смены, Отчеты ФН
		 * Обязательные параметры:
		 * Type – Тип отчета;
		 * o '0' - Сокращенный;
		 * o '1' - Полный;
		 * o '10' – Сокращенный в файл;
		 * o '11' – Полный в файл;
		 * o '3' – Чтение следующей строки отчета 10 или 11 как текст;
		 * o '5' – Отчет статуса смены; (опциональные параметры не влияют);
		 * o '6' – Печать документов из ФН, Печать всех фискальных документов от начального, до последнего;
		 * o '7' – Отчет регистраций ФН, Печать всех отчетов перерегистраций ФН от начального, до последнего.
		 * Опциональные параметры:
		 * • First – Номер первого Отчета из блока отчетов. По умолчанию: 1;
		 * • Last - Номер последнего Отчета из блока отчетов. По умолчанию: Номер последнего
		 * отчета; Ответ (1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ (2): {ErrorCode}<SEP>{TextData}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • TextData – Текст документа (до 48 символов);
		 * Ответ (3): {ErrorCode}<SEP>{nFD}<SEP>{SignFP}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nFD – Номер фискального документа ФД (1-99999999);
		 * • SignFP – Номер фискального признака документа ФП (1-99999999).
		 * \endcode
		 */
		void fiscalMemoryZReport(struct fiscalMemoryZReportParams& p);

		/**
		 * programs TAX number
		 *
		 * command's number: 98
		 *
		 * @param tax number to be programmed
		 * \code
		 * 98 (62h) Программирование ИНН
		 * Обязательные параметры:
		 * • TAXNumber – Идентификационный номер налогоплательщика ИНН (10 или 12 цифр);
		 * Ответ: {ErrorCode}<SEP>{Country}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void programTAX(const char* tax);

		/**
		 * reads TAX number
		 *
		 * command's number: 99
		 * \code
		 * 99 (63h) Чтение запрограммированного ИНН
		 * Параметры команды: Нет
		 * Ответ: {ErrorCode}<SEP>{TAXNumber}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие;
		 * Ответ «0» = команда выполнена;
		 * • TAXNumber – Идентификационный номер налогоплательщика ИНН (двенадцать цифр);
		 * \endcode
		 */
		void readTAX();

		/**
		 * asks for error explanation
		 *
		 * command's number: 100
		 *
		 * @param err number to be explained
		 * \code
		 * 100 (64h) Чтение ошибки по коду
		 * Параметры команды: {Code}<SEP> Обязательные параметры:
		 * • Code – номер ошибки (номер со знаком минус вначале);
		 * Ответ: {ErrorCode}<SEP>{Code}<SEP>{ErrorMessage}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие;
		 * Ответ «0» = команда выполнена;
		 * • Code – номер ошибки для разъяснения;
		 * • ErrorMessage – расшифровка ошибки Code.
		 * \endcode
		 */
		void readError(int err);

		/**
		 * sets new password
		 *
		 * command's number: 101
		 *
		 * @param p password parameters
		 * \code
		 * 101 (65h) Установка пароля кассира
		 * Параметры команды: {OpCode}<SEP>{OldPwd}<SEP>{NewPwd}<SEP> Обязательные параметры:
		 * • OpCode –Номер кассира (1-30);
		 * • NewPwd –Пароль кассира (Строка цифр в кодировке Ascii, длиной 4-8);
		 * Опциональные параметры:
		 * • OldPwd – Старый пароль кассира или пароль администратора (кассиры 29 и 30). Может быть пустым если установлена сервисная перемычка;
		 * Ответ : {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void setPassword(struct password& p);

		/**
		 * registers operator with his password
		 *
		 * command's number: 102
		 *
		 * @param opCode Operator number from 1...32;
		 *
		 * @param opPass Operator password, ascii string of digits. Lenght from 1...8;
		 * \code
		 * 102 (66h) Регистрация кассира
		 * Параметры команды: {OpCode}<SEP>{OpPwd}<SEP> Обязательные параметры:
		 * • OpCode –Номер кассира (1-30);
		 * • OpPwd – Пароль кассира (Строка цифр в кодировке Ascii, длиной 1-8);
		 * Ответ : {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void regOperator(char opCode, const char *opPass);

		/**
		 * asks about current receipt's information
		 *
		 * command's number: 103
		 * \code
		 * 103 (67h) Информация о текущем чеке
		 * Параметры команды: Нет
		 * Ответ: {ErrorCode}<SEP>{Sum1}<SEP>{Sum2}<SEP>{Sum3}
		 * <SEP>{Sum4}<SEP>{Sum5}<SEP>{Sum6}<SEP>
		 * • ErrorCode – Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SumX – Текущая накопленная сумма по группе НДСx (0.00-9999999.99);
		 * \endcode
		 */
		void receiptInfo();

		/**
		 * reports about operators' actions
		 *
		 * command's number: 105
		 *
		 * @param firstOp Operator's number from 1...30;
		 *
		 * @param lastOp Operator's number from 1...30;
		 *
		 * @param clear Clears registers for operators. Default: 0;
		 * o '0' - Do not clear registers for operators;
		 * o '1' - Clear registers for operators;
		 * \code
		 * 105 (69h) Отчет по кассирам
		 * Опциональные параметры:
		 * • FirstOper – Первый кассир в отчете (1-30). По умолчанию: 1;
		 * • LastOper – Последний кассир в отчете (1-30). По умолчанию: 30;
		 * • Clear – Тип отчета. По умолчанию: 0;
		 * o '0' – Отчет по кассирам без гашения;
		 * o '1' – Отчет по кассирам с гашением;
		 * Ответ : {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void reportOperators(char firstOp, char lastOp, char clear);

		/**
		 * sets drawer openning time
		 *
		 * command's number: 106
		 *
		 * @param time of openning of money box
		 * \code
		 * 106 (6Ah) Открытие денежного ящика
		 * Параметры команды: {mSec}<SEP> Опциональные параметры:
		 * • mSec – Длительность импульса на открытие денежного ящика в миллисекундах (1- 65535);
		 * Ответ : {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void setDrawerOpeningTime(char time);

		/**
		 * gives information about programmed items
		 *
		 * command's number: 107
		 * \code
		 * 107 (6Bh) Программирование и считывание параметров товаров
		 * Параметры команды:
		 * • 'I' – Информация о товаре;
		 * • 'P' – Программирование товара;
		 * Обязательные параметры:
		 * o PLU – Код товара (1-100000);
		 * o TaxGr – Ставка НДС (буквы 'A', 'B', 'C' 'D' 'E' 'F');
		 * o Dep - Отдел (1-99);
		 * o Group – Группа товаров (1-99);
		 * o PriceType – Тип цены ('0' – фиксированная цена, '1' – свободная цена, '2' – максимальная цена);
		 * o Price - Цена (0.00-9999999.99);
		 * o AddQty – Байт со значением 'A';
		 * o Quantity - Количество (0.000-99999.999);
		 * o Name – Наименование товара (до 32 символов);
		 * Опциональные параметры:
		 * o BarX – Штрих-код X (до 13 цифр);
		 * • 'A' - Изменение доступного количества товара; Синтаксис: {Option}<SEP>{PLU}<SEP>{Quantity}<SEP>
		 * Обязательные параметры:
		 * o PLU – Код товара (1-100000);
		 * o Quantity - Количество (0.001...99999.999);
		 * • 'D' – Удаление товара;
		 * Обязательные параметры:
		 * o firstPLU – Код первого товара из диапазона удаляемых товаров (1-100000). Если
		 * значение этого параметра 'A', то все товары будут удалены (lastPLU может быть
		 * пустым); Опциональные параметры:
		 * o lastPLU - Код последнего товара из диапазона удаляемых товаров (1-100000).
		 * По умолчанию: {firstPLU};
		 * • 'R' – Считывание данных товара; Синтаксис: {Option}<SEP>{PLU}<SEP>
		 * Обязательные параметры:
		 * o PLU – Код товара (1-100000);
		 * • 'F' – Возврат данных первого найденного запрограммированного товара;
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 0;
		 * • 'L' - Возврат данных последнего найденного запрограммированного товара;
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 100000;
		 * • 'N' - Возврат данных следующего найденного запрограммированного товара;
		 * • 'f' - Возврат данных первого найденного товара имеющего обороты продаж;
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 0;
		 * • 'l' - Возврат данных последнего найденного товара имеющего обороты продаж; Синтаксис: {Option}<SEP>{PLU}<SEP>
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 100000; Ответ(2)
		 * • 'n' - Возврат данных следующего найденного товара имеющего обороты продаж;
		 * Примечание: Сначала эта команда должна быть выполнена с опцией 'F' или 'L'. Это определит данные какого товара будут возвращены: следующий ('F') или предыдущий ('L');
		 * Синтаксис: {Option}<SEP>
		 * • 'X' – Поиск первого незапрограммированного товара; Синтаксис: {Option}<SEP>{PLU}<SEP>
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 0;
		 * • 'x' - Поиск последнего незапрограммированного товара; Синтаксис: {Option}<SEP>{PLU}<SEP>
		 * Опциональные параметры:
		 * o PLU – Код товара (1-100000). По умолчанию: 100000;
		 * Примечание: Сначала эта команда должна быть выполнена с опцией 'f' или 'l'. Это определит данные какого товара будут возвращены: следующий ('f') или предыдущий ('l');
		 * Ответ (1) : {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ(2):{ErrorCode}<SEP>{PLU}<SEP>{TaxGr}<SEP>{Dep}<SEP> {Group}<SEP>{PriceType}<SEP>{Price}<SEP>{Turnover}<SEP>{SoldQty}<SEP> {StockQty}<SEP>{Bar1}<SEP>{Bar2}<SEP>{Bar3}<SEP>{Bar4}<SEP>{Name}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • PLU – Код товара (1-100000);
		 * • TaxGr – Ставка НДС (1-6);
		 * • Dep - Отдел (1-99);
		 * • Group – Группа товаров (1-99);
		 * • PriceType– Тип цены ( '0' – фиксированная цена, '1' – свободная цена, '2' –
		 * максимальная цена); • Price - Цена (0.00-9999999.99);
		 * • Turnover – Накопленная сумма по товару (0.00-9999999.99);
		 * • SoldQty – Количество проданного товара (0.000-99999.999);
		 * • StockQty – Текущее количество (0.000-99999.999);
		* • BarX – Штрих-код X (до 13 цифр);
		* • Name – Наименование товара (до 32 символов).
			* Ответ(3): {ErrorCode}<SEP>{Total}<SEP>{Prog}<SEP>{NameLen}<SEP>
			* • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		* • Total - Общее количество программируемых товаров (100000);
		* • Prog - Общее количество запрограммированных товаров (0-100000);
		* • NameLen – Максимальная длина наименования товара (32).
			* Ответ(4): {ErrorStatus}<SEP>{PLU}<SEP>
			* • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		* • PLU – Код товара (1-100000).
			* \endcode
		*/
		void itemInfo();

		/**
		 * programs item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 *
		 */
		void programItem(struct itemsInfo& p);

		/**
		 * sets item's quantity
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 *
		 */
		void setAvailabilityOfItem(struct itemsInfo& p);

		/**
		 * deletes items from PluNum till lastPlu
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be programmed
		 */
		void deleteItems(struct itemsInfo& p);

		/**
		 * reads item's info by its number
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be read
		 */
		void readItem(struct itemsInfo& p);

		/**
		 * finds an item info by its number
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void findItem(struct itemsInfo& p);

		/**
		 * retuns a last found item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastFoundItem(struct itemsInfo& p);

		/**
		 * retuns a next found item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextFoundItem(struct itemsInfo& p);

		/**
		 * retuns a next found item with sales on it
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextSalesItem(struct itemsInfo& p);

		/**
		 * data about the last found item with sales on it;
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastSalesItem(struct itemsInfo& p);

		/**
		 * data for the next found item with sales on it
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void nextFoundSalesItem(struct itemsInfo& p);

		/**
		 * first not programmed item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void firstNotProgrammedItem(struct itemsInfo& p);

		/**
		 * last not programmed item
		 *
		 * command's number: 107
		 *
		 * @param p item's info to be found
		 */
		void lastNotProgrammedItem(struct itemsInfo& p);

		/**
		 * prints dublicate receipt
		 *
		 * command's number: 109
		 * \code
		 * 109 (6Dh) Печать дубликата чека
		 * Параметры команды: Нет
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void printDublicateReceipt();

		/**
		 * prints additional Daily Info
		 *
		 * command's number: 110
		 *
		 * @param type  Type of information. Default: 0;
		 * o '0' - Payments in normal receipts "Кассовых чеков";
		 * o '1' - Payments in correction receipts "Кассовых чеков коррекции";
		 * o '2' - Number and sum of sales;
		 * o '3' - Number and sum of discounts and surcharges;
		 * o '4' - Number and sum of corrections "Сторно" in a receipt and of annulled receipts "Отмененных чеков";
		 * o '5' - Number and sum of cash in and cash out operations;
		 *
		 * @param rec_type Type of receipts (for types 0,1,2,3 and 4). By default RecType - 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 *
		 *
		 * \code
		 * 110 (6Eh) Дополнительная ежедневная информация
		 * Параметры команды: {Type}<SEP>{RecType}<SEP> Опциональные параметры:
		 * • Type – Тип информации. По умолчанию: 0;
		 * o '0' – Способы оплаты в кассовых чеках;
		 * o '1' – Способы оплаты в кассовых чеках коррекции;
		 * o '2' – Количество чеков и сумма продаж;
		 * o '3' – Количество и сумма скидок и наценок;
		 * o '4' – Количество и сумма сторнирований в кассовых чеках, а также в аннулированных кассовых чеках;
		 * o '5' – Количество и сумма операций внесения и изъятия наличных;
		 * • RecType – Тип кассовых операций. По умолчанию: 0;
		 * o '0' – Приход;
		 * o '1' – Возврат прихода;
		 * o '2' – Расход;
		 * o '3' – Возврат расхода;
		 * Ответ(1): {ErrorCode}<SEP>{Pay1}<SEP>{Pay2}<SEP>{Pay3}<SEP>{Pay4}<SEP> {Pay5}<SEP>{Pay6}<SEP>{ForeignPay}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • PayX - Сумма (0.00-9999999.99) оплаченная/возвращенная способом оплаты X в
		 * кассовых чеках;
		 * • ForeignPay - Сумма (0.00-9999999.99) оплаченная/возвращенная способом оплаты X в
		 * кассовых чеках;
		 * Ответ(2): {ErrorCode}<SEP>{Pay1}<SEP>{Pay2}<SEP>{Pay3}<SEP>{Pay4}<SEP> {Pay5}<SEP>{Pay6}<SEP>{ForeignPay}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • PayX - Сумма (0.00-9999999.99) оплаченная/возвращенная способом оплаты X в
		 * кассовых чеках коррекции;
		 * • ForeignPay - Сумма (0.00-9999999.99) оплаченная/возвращенная способом оплаты X в
		 * кассовых чеках коррекции; Ответ(3): {ErrorCode}<SEP>{Num}<SEP>{Sum}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Num – Количество чеков типа RecType включая коррекции ( от 0 и более);
		 * • Sum – Сумма оплаченная/возвращенная по чекам типа RecType (0.00-9999999.99).
		 * Ответ(4): {ErrorCode}<SEP>{qSur}<SEP>{sSur}<SEP>{qDis}<SEP>{sDis}<SEP>{qSurVal}<SEP> {sSurVal}<SEP>{qDisVal}<SEP>{sDisVal}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • qSur – Количество наценок процентных (целое число 0,1,2...);
		 * • sSur – Сумма наценок процентных (0.00-9999999.99);
		 * • qDis – Количество скидок процентных (целое число 0,1,2...);
		 * • sDis – Сумма скидок процентных (0.00-9999999.99);
		 * • qSurVal – Количество наценок по сумме (целое число 0,1,2...);
		 * • sSurVal – Сумма наценок по сумме (0.00-9999999.99);
		 * • qDisVal – Количество скидок по сумме (целое число 0,1,2...);
		 * • sDisVal – Сумма скидок по сумме (0.00-9999999.99);
		 * Ответ(5): {ErrorCode}<SEP>{qVoid}<SEP>{sVoid}<SEP>{qAnul}<SEP>{sAnul}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • qVoid – Количество сторнирований в чеке (целое число 0,1,2...);
		 * • sVoid – Сумма сторнирований в чеке (0.00-9999999.99);
		 * • qAnul – Количество отмененных чеков (целое число 0,1,2...);
		 * • sAnul – Сумма отмененных чеков (0.00-9999999.99);
		 * Ответ(6): {ErrorCode}<SEP>{qCashIn1}<SEP>{sCashIn1}<SEP>{qCashOut1}<SEP>{sCashOut1} <SEP>{qCashIn2}<SEP>{sCashIn2}<SEP>{qCashOut2}<SEP>{sCashOut2}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • qCashIn1 – Количество операций внесения наличных (целое число 0,1,2...);
		 * • sCashIn1 – Сумма операций внесения наличных (0.00-9999999.99);
		 * • qCashOut1 – Количество операций выемки наличных (целое число 0,1,2...);
		* • sCashOut1 – Сумма операций выемки наличных (0.00-9999999.99);
		* • qCashIn2 – Количество операций внесения наличных в альтернативной валюте (целое
				* число 0,1,2...);
		* • sCashIn2 – Сумма операций внесения наличных в альтернативной валюте (0.00-
				* 9999999.99);
		* • qCashOut2 – Количество операций выемки наличных в альтернативной валюте (целое
				* число 0,1,2...);
		* • sCashOut2 – Сумма операций выемки наличных в альтернативной валюте (0.00-
				* 9999999.99);
		* \endcode
		*/
		void additionalDInfo(char type, char rec_type);

		/**
		 * PLU report
		 *
		 * command's number: 111
		 *
		 * @param type  Type of report;
		 * o '0' - PLU turnovers;
		 * o '1' - PLU turnovers with clearing; o '2' - PLU parameters;
		 * o '3' - PLU stock;
		 *
		 * @param firstPlu First PLU in the report (1÷3000). Default: 1;
		 *
		 * @param lastPlu Last PLU in the report (1÷3000). Default: 3000;
		 *
		 * \code
		 * 111 (65h) Отчет по программируемым товарам
		 * Параметры команды: {Type}<SEP>{FirstPLU}<SEP>{LastPLU}<SEP> Обязательные параметры:
		 * • Type – Тип отчета;
		 * o '0' – по оборотам PLU без гашения;
		 * o '1' – по оборотам PLU с гашением;
		 * o '2' – по параметрам PLU;
		 * o '3' – по остаткам PLU;
		 * Опциональные параметры:
		 * • FirstPLU - Первый PLU в отчете (1-100000). По умолчанию: 1;
		 * • LastPLU - Последний PLU в отчете (1-100000). По умолчанию: 100000;
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void pluReport(char type, char firstPlu, char lastPlu);

		/**
		 * Information for operator
		 *
		 * command's number: 112
		 *
		 * @param op Operator number (1÷30);
		 *
		 * @param rec_type Type of receipts. Default: 0;
		 * o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 *
		 * \code
		 * 112 (70h) Информация по кассирам
		 * Параметры команды: {Operator}<SEP>{RecType}<SEP> Обязательные параметры:
		 * • Operator – Номер кассира (1-30); Опциональные параметры:
		 * • RecType – Тип кассовых операций(чеков). По умолчанию значение 0.
		 * o 0 - Приход
		 * o 1 – Возврат прихода
		 * o 2 – Расход
		 * o 3 – Возврат расхода
		 * Ответ:{ErrorCode}<SEP>{Num}<SEP>{Sum}<SEP>{qSur}<SEP>{sSur}<SEP>{qDis}<SEP> {sDis}<SEP>{qSurVal}<SEP>{sSurVal}<SEP>{qDisVal}<SEP>{sDisVal}<SEP>{qVoid}<SEP> {sVoid}<SEP>{qAnul}<SEP>{sAnul}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Num – Количество чеков типа RecType включая коррекции ( от 0 и более);
		 * • Sum – Сумма оплаченная/возвращенная по чекам типа RecType (0.00-9999999.99).
		 * • qSur – Количество наценок процентных (целое число 0,1,2...);
		 * • sSur – Сумма наценок процентных (0.00-9999999.99);
		 * • qDis – Количество скидок процентных (целое число 0,1,2...);
		 * • sDis – Сумма скидок процентных (0.00-9999999.99);
		 * • qSurVal – Количество наценок по сумме (целое число 0,1,2...);
		 * • sSurVal – Сумма наценок по сумме (0.00-9999999.99);
		 * • qDisVal – Количество скидок по сумме (целое число 0,1,2...);
		 * • sDisVal – Сумма скидок по сумме (0.00-9999999.99);
		 * • qVoid – Количество сторнирований в чеке (целое число 0,1,2...);
		 * • sVoid – Сумма сторнирований в чеке (0.00-9999999.99);
		 * • qAnul – Количество отмененных чеков (целое число 0,1,2...);
		 * • sAnul – Сумма отмененных чеков (0.00-9999999.99);
		 * \endcode
		 */
		void infoForOperator(char op, char rec_type);

		/**
		 * Reading FM
		 *
		 * command's number: 116
		 *
		 * @param op type of operation = '0';
		 *
		 * @param address Start address 0÷FFFFFF (format ascii-hex).
		 *
		 * @param count Number of bytes (1÷104)
		 * \code
		 * 116 (74h) Считывание ФП
		 * Обязательные параметры:
		 * • Operation – Тип операции. Всегда = '0';
		 * • Address – Начальный адрес 0-FFFFFF (формат ASCII);
		 * • nBytes – Количество байт (1-104);
		 * Ответ: {ErrorCode}<SEP>{Data}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Data – Считанные данные. Количество байт равно запрошенному в nBytes, умноженного на 2;
		 * \endcode
		 */
		void readFM(char op, char address, char count);

		/**
		 * Reading Mem
		 *
		 * command's number: 121
		 *
		 * @param address Start address 0÷FFFFFF (format ascii-hex).
		 *
		 * @param count Number of bytes (1÷64)
		 * \code
		 * 121 (79h) Считывание исполняемого кода из памяти
		 * Обязательные параметры:
		 * • Address – Начальный адрес (HEX);
		 * • nBytes – Количество байт (1-64); или
		 * • nBytes – Количество байт (0);
		 * Ответ(1) Ответ(2)
		 * Ответ(1): {ErrorCode}<SEP>{Data}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Data – Считанные данные из кода памяти в шестнадцатеричном формате. (2 символа для каждого байта данных).
		 * Ответ(2): {ErrorCode}<SEP>{SizeOfData}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • SizeOfData – Размер исполняемого кода в памяти.
		 * \endcode
		 */
		void readMem(char address, char count);

		/**
		 * Search documents in EJ by date
		 *
		 * command's number: 124
		 *
		 * @param p document parameters
		 * \code
		 * 124 (7Ch) Поиск документа в ЭЖ по дате
		 * Опциональные параметры:
		 * • StartDate – Начальная дата для поиска (формат "DD-MM-YY"). По умолчанию: Дата первого документа;
		 * • EndDate – Конечная дата для поиска (формат "DD-MM-YY"). По умолчанию: Дата последнего документа;
		 * DocType – Тип документа;
		 * o '0' – все документы;
		 * o '1' – фискальные документы (включая отчеты открытия/закрытия смены);
		 * o '2' – не фискальные документы;
		 * o '3' – отчеты закрытия смены;
		 * Ответ: {ErrorCode}<SEP>{DateFirst}<SEP>{DateLast}<SEP>{NumFirst}<SEP>{NumLast}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • DateFirst – Дата первого документа типа DocType в периоде (формат "DD-MM-YY hh:mm:ss");
		 * • DateLast - Дата последнего документа типа DocType в периоде (формат "DD-MM-YY hh:mm:ss");
		 * • NumFirst - Номер первого документа типа DocType в периоде (1-99999999);
		 * • NumLast - Номер последнего документа типа DocType в периоде (1-99999999);
		 * \endcode
		 */
		void searchDocuments(struct searchDocsParams& p);

		/**
		 * Info EJ
		 *
		 * command's number: 125
		 *
		 * @param op Option:
		 * o '0' - Set document to read;
		 * o '1' - Read one line as text. Must be called multiple time to read whole document;
		 * o '2' - Read as data. Must be called multiple time to read whole document;
		 * o '3' - Print document;
		 *
		 * @param doc_num Number of document (1÷99999999). Needed for Option = 0 and 3.
		 *
		 * @param rec_type Document type. Needed for Option = 0.
		 * o '0' - all types;
		 * o '1' - fiscal receipts (including Z-reports);
		 * o '2' - non fiscal receipts;
		 * o '3' - Z-reports;
		 * o "20" - full EJ content for Z report specified in {doc_num};
		 *
		 * \code
		 * 125 (7Dh) Информация из ЭЖ
		 * Параметры команды: {Option}<SEP>{DocNum}<SEP>{RecType}<SEP> Обязательные параметры:
		 * • Option – Тип информации;
		 * o '0' - Установка документа для чтения;
		 * o '1' – Чтение одной строки текста. Для считывания всего документа необходимо выполнить команду несколько раз;
		 * o '2' – Чтение данных. Для считывания всего документа необходимо выполнить
		 * команду несколько раз;
		 * o '3' – Печать документа;
		 * Опциональные параметры:
		 * • DocNum - Номер документа (1-99999999). Необходимо для Option=0 или 3;
		 * • DocType - Тип документа. Необходимо установить Option = 0;
		 * o '0' – все документы;
		 * o '1' – фискальные документы (включая отчеты открытия/закрытия смены);
		 * o '2' – не фискальные документы;
		 * o '3' - отчеты закрытия смены;
		 * o '20' – полное содержание ЭЖ для Отчета закрытия смены указанного в {DocNum};
		 * Ответ(1): {ErrorCode}<SEP>{DocNumber}<SEP>{RecNumber}<SEP> {Date}<SEP>{Type}<SEP>{Znumber}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • DocNumber – Номер документа - общий (1-99999999);
		 * • RecNumber – Номер документа - по Type (1-99999999);
		 * • Date – Дата документа (формат "DD-MM-YY hh:mm:ss");
		 * • Type – Тип документа;
		 * o '0' – все документы;
		 * o '1' – фискальные документы (включая отчеты открытия/закрытия смены); o '2' – не фискальные документы;
		 * o '3' - отчеты закрытия смены;
		 * • Znumber - Номер Отчета закрытия смены (1-1830);
		 * Ответ(2): {ErrorCode}<SEP>{TextData}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • TextData – Текст документа (до 48 символов);
		 * Ответ(3): {ErrorCode}<SEP>{Data}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Data – Данные документа, в структуре и формату base64;
		 * Ответ(4): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void infoEJ(char op, char doc_num, char rec_type);

		/**
		 * stampOperation
		 *
		 * command's number: 127
		 * \code
		 * 127 (7Fh) Операции со штампами
		 * Обязательные параметры:
		 * • Type – Тип операции;
		 * o '0' – Печать штампа;
		 * o '1' – Переименование загруженного штампа командой 203;
		 * • Name – Имя файла в формате 8.3; Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void stampOperation(char type, const char *name);

		/**
		 * OFD - FN registration/deregistration/change registration
		 *
		 * command's number: 144
		 *
		 * @param op type of operation;
		 * '1' - Registration;
		 * '2' - Reregistration same FN;
		 * '3' - Deregistration;
		 * '4' - Reregistration new FN;
		 * '5' - Force close current FN ( use when FN is broken or lost ). Require service jumper!;
		 * '9' - Erase FN (Service program only!!!). Only development FN can by erased;
		 *
		 * @param pass Current inspector password;
		 * \code
		 * 144 (90h) ФН-ОФД операции Регистрации/Изменения параметров регистрации/Закрытия ФН
		 * Параметры команды: {OperationType}<SEP>{InsPssword}<SEP> Обязательные параметры:
		 * • OperationType – Тип операции;
		 * o '1' – Регистрация (активация ФН)
		 * o '2' – Изменения параметров регистрации (тот же ФН);
		 * o '3' – Закрытие архива ФН;
		 * o '4' – Перерегистрация (новый ФН);
		 * o '5' – Аварийное закрытие ФН (например при поломке) (требуется установить сервисную перемычку;
		 * o '6' – Сброс параметров регистрации ККТ к заводским настройкам.;
		 * o '9' – Очистка ФН (доступно только в сервисной прошивке). Функция работает только с отладочным МГМ ФН-1;
		 * o '10' – Регистрация (активация ФН) только для ФФД 1.0
		 * o '12' – Регистрация (активация ФН) только для ФФД 1.05
		 * • InspPssword – Текущий пароль Системного администратора;
		 * Ответ(1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ(2): {ErrorCode}<SEP>{nFD}<SEP>{SignFP}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • nFD – Номер фискального документа ФД (1-99999999);
		 * • SignFP – Номер фискального признака документа ФП (1-99999999).
		 * \endcode
		 */
		void registerOFD(char op, const char *pass);


		/**
		 * a group of commands which load logo
		 *
		 * command's number: 202
		 *
		 * o START – begins of data transfer
		 * o STOP – stops of data transfer
		 * o YmFzZTY0ZGF0YQ== - logo in base64 format;
		 * o POWEROFF – shuts down the device
		 * o RESTART – restarts the device
		 * \code
		 * 202 (CAh) Загрузка логотипа
		 * Параметры команды: {Parameter}<SEP> Обязательные параметры:
		 * • Parameter – тип операции;
		 * o START – Подготовка к процессу загрузки;
		 * o STOP – Завершение передачи данных;
		 * o YmFzZTY0ZGF0YQ== - логотип в формате кодировке base64;
		 * o POWEROFF – Выключение устройства;
		 * o RESTART – Перезагрузка устройства;
		 * Ответ(1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ(2): {ErrorCode}<SEP>{Chechsum}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Checksum – Контрольная сумма декодированных base64 данных;
		 * \endcode
		 */
		void loadLogo(const char *logo);

		/**
		 * a group of commands which load stamp info
		 *
		 * command's number: 203
		 *
		 * o START – begin of data transfer
		 * o STOP – stop of data transfer
		 * o YmFzZTY0ZGF0YQ== - stamp in base64 format;
		 *
		 * \code
		 * 203 (CBh) Загрузка штампа
		 * Параметры команды: {Parameter}<SEP> Обязательные параметры:
		 * • Parameter – тип операции;
		 * o START – Подготовка к процессу загрузки;
		 * o STOP – Завершение передачи данных;
		 * o YmFzZTY0ZGF0YQ== - логотип в формате кодировке base64;
		 * Ответ(1) Ответ(2) Ответ(2)
		 * Ответ(1): {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * Ответ(2): {ErrorCode}<SEP>{Chechsum}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * • Checksum – Контрольная сумма декодированных base64 данных;
		 * \endcode
		 */
		void loadStamp(const char *cmd);

		/**
		 * Entering of service parameters
		 *
		 * command's number: 253
		 *
		 * @param op Type of entered parameter;
		 * '0' - Service password (Password of the Service man);
		 * '1' - Command to FN
		 *
		 * @param cmd Value of entered parameter;
		 * Service password (Password of the Service man). Text up to 8 symbols. The default password
		 * may be blank ( 0 symbols ) or "70";
		 * \code
		 * 253 (FDh) Установка сервисных параметров / прямое обращение к ФН
		 * Параметры команды:
		 * Обязательные параметры:
		 * • Type – Тип устанавливаемого параметра;
		 * o '0' – Пароль режима Сервис (Пароль сервисного специалиста);
		 * • Value – Значение вводимого параметра;
		 * o Пароль режима Сервис (Пароль сервисного специалиста). Текст до 8 символов. Пароль по умолчанию пустой (0 символов) или "70";
		 * Обязательные параметры:
		 * • Type – Тип устанавливаемого параметра;
		 * o '1' – Прямое обращение к ФН;
		 * • Cmd – Команда обращения к ФН согласно описанию интерфейса взаимодействия с ФН; Например: для команды 0x45h (69d), значение Cmd должно быть равно 69; Значения введенного параметра.
		 * • CmdData – Данные команды согласно документации ФН. Формат данных – ASCII HEX;
		 * Примечание: Ввод пароля Сервисного режима делает возможным изменение некоторых параметров с помощью команды 255 (в том числе изменение самого сервисного пароля). Считывание сервисного пароля с помощью команды 255 также возможно только после ввода сервисного пароля. Если установлена сервисная перемычка, то ввод сервисного пароля не требуется;
		 * Ответ: {ErrorCode}<SEP>
		 * • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		 * \endcode
		 */
		void serviceParameters(char op, char cmd, const char *data);

		/**
		 * Programming
		 *
		 * command's number: 255
		 *
		 * @param name Variable name;
		 *
		 * @param param used for index if variable is array. For variable that is not array can be left blank. Default: 0;
		 * Note:For example: Header[], Index 0 refer to line 1. Index 9 refer to line 10.
		 *
		 * @param value If this parameter is blank ECR will return current value (Answer(2)). If the value is set, then
		 * ECR will program this value (Answer(1));
		 * \code
		 * 255 (FFh) Программирование
		 * Параметры команды: {Name}<SEP>{Index}<SEP>{Value}<SEP> Обязательные параметры:
		 * • Name – Наименование переменной; o Установки устройства;
		 * ▪ FpComPort – Номер COM порта для связи с ПК (0 – отключен, 1 – COM1, 2 – COM2, 3 – USB, 4-Bluetooth)
		 * ▪ FpComBaudRate – Скорость обмена данными по COM-порту при связи с ПК (0 ÷ 9);
		 * ▪ ComPortDevice – Назначение COM порта для внешних периферийных устройств. (0 – нет, 1 – сканер штрих-кодов, 2 – весы, 3 – дисплей покупателя;) Номер COM порта определяется в параметре Index.
		 * ▪ ComPortBaudRate – Скорость обмена данными по COM-порта периферийного устройства (0-999999); Номер COM порта определяется в параметре Index.
		 * ▪ ComPortProtocol – Протокол взаимодействия с внешними периферийными устройствами привязанным к COM порту (от 0 до 9 для весов). Номер COM порта определяется в параметре Index.
		 * ▪ BthDiscoverability – Включение/выключение видимости Bluetooth (0 – скрытый, 1 – видимый) (только для устройств с поддержкой Bluetooth);
		 * ▪ BthPairing – настройки спаривания (0 – небезопасный, 1 – сбросить и сохранить, 2 – сбросить (только для устройств с поддержкой Bluetooth);
		 * ▪ BthPinCode – пин код для Bluetooth (по умолчанию 0000) (только для устройств с поддержкой Bluetooth);
		 * ▪ BthVersion - версия прошивки Bluetooth модуля ) (только для устройств с поддержкой Bluetooth);
		 * ▪ BthAddress – адрес устройства Bluetooth (только для устройств с поддержкой Bluetooth);
		 * ▪ BarCodeHeight – Высота штрих-кода от '1' (7 мм) до '10' (70 мм);
		 * ▪ BarcodeName - Включение/Выключение печати данных штрих-кода;
		 * ▪ TimeOutBeforePrintFlush – Таймоут команд фискального регистратора перед началом автоматической печати (миллисекунды, 1-999999999);
		 * ▪ NetInterfaceToUse – Сетевой интерфейс для связи с сервером ОФД. 0 – LAN, 1 – GPRS, 2 – Wi-Fi, Обязательно требуется сервисный пароль.
		 * ▪ MainInterfaceType – Интерфейс связи с ПК. 0 – автоматический выбор, 1 – RS232, 2- Bluetooth, 3 – USB, 4-LAN. При установленной сервисной перемычке MainInterfaceType = 0;
		 * o Параметры ККТ;
		 * ▪ EcrLogNumber – логический номер (от 1 до 9999);
		 * ▪ EcrAskForPassword – запрос пароля после печати каждого чека (1 – включено, 0 –  выключено);
		 * ▪ EcrAskForVoidPassword – запрос пароля для операций отмены (1 – включено, 0 – выключено);
		 * ▪ EcrConnectedOperReport - Печать Отчета по операторам при снятии Отчета о закрытии смены (1 - включено, 0 - выключено);
		 * ▪ EcrConnectedDeptReport - Печать Отчета по отделам при снятии Отчета о  закрытии смены(1 – включено, 0 – выключено);
		 * ▪ EcrConnectedPluSalesReport - Печать Отчета по товарам с оборотами при снятии Отчета о закрытии смены(1 – включено, 0 – выключено);
		 * ▪ EcrConnectedGroupsReport - Печать Отчета по отделам и группам товаров при снятии Отчета о закрытии смены(1 – включено, 0 – выключено);
		 * ▪ EcrConnectedCashReport - Печать Отчета кассы при снятии Отчета о закрытии смены(1 – включено, 0 – выключено);
		 * ▪ EcrPluDailyClearing – Обнуление оборотов по товарам при каждом отчете закрытия смены (1 - включено, 0 - выключено);
		 * ▪ EcrOnlyAdminOpenShift - Открытие смены только для Администраторов;
		 * ▪ EcrEnableForeign – Работа с зарубежной валютой (1 - включено, 0 - выключено);
		 * ▪ EcrScaleBarMask - Текст до 10 символов. Если второй символ данных штрих-кода со стороны весов содержит любой из указанных символов, то штрих-код будет считаться действительным.
		 * ▪ EcrNumberBarcode – Подсчет использованных штрих-кодов для каждого запрограммированного товара (1-4);
		 * ▪ AutoPowerOff – Количество минут простоя, после которых ККТ автоматически выключится. (0 – выключен, от 1 до 15 минут);
		 * ▪ BkLight_AutoOff - Количество минут простоя, после которых автоматически выключится подсветка экрана ККТ. (0 – выключен, от 1 - до 5 минут);
		 * ▪ RegModeOnIdle – Время сброса данных с экрана после печати последнего чека, в миллисекундах (1-2147483647);
		 * ▪ WorkBatteryIncluded – ККТ работает от АКБ при подключенном блоке питания (1 - включена, 0 - выключена);
		 * o Настройки валюты;
		 * ▪ CurrNameLocal – Наименование локальной валюты (до 3-х символов);
		 * ▪ CurrNameForeign – Наименование иностранной валюты (до 3-х символов);
		 * ▪ ExchangeRate – Курс обмена ( от 0 до 999999999, десятичный знак находится перед последними пяти знаками);
		 * o Заголовок чека;
		 * ▪ Header – Строка текста до 48 символов. Номер строки заголовка определен в "Index". Index 0 для первой строки, Index 1 для второй строки;
		 * o Рекламный текст в чеке;
		 * ▪ Footer - Строка текста до 48 символов. Номер строки рекламного текста определен в "Index". Index 0 для первой строки, Index 9 для десятой строки;
		 * o Другой текст в чеке;
		 * ▪ SenderAddress – “Адрес отправителя”- Строка текста до 64 символов.
		 * ▪ CheckFPDAddress- “Адрес сайта для проверки ФПД“ – Строка текста до 128
		 * символов. o Операторы;
		 * ▪ OperName – Наименование кассира. Строка текста до 32 символов. Номер кассира определен в "Index";
		 * ▪ OperPasw – Пароль кассира. Строка текста до 8 цифр. Номер кассира определен в "Index"; o Типы оплаты;
		 * ▪ PayName – Наименование формы оплаты. Строка текста до 16 символов. Номер оплаты определен в "Index";
		 * Примечание: Если наименование формы оплаты пустое или " ", то использование этой оплаты невозможно. Наименование форм оплат по умолчанию: " НАЛИЧНЫМИ ", " ЭЛЕКТРОННЫМИ ", " ", " ".
		 * ▪ Payment_forbidden – Запрет формы оплаты (1 – запрещен, 0 – не запрещен); Номер оплаты определен в "Index";
		 * o Сервис (Требуется пароль режима Сервис или установка сервисной перемычки);
		 * ▪ ServPasw – Пароль режима Сервис (Пароль сервисного специалиста). Строка
		 * текста до 8 символов. По умолчанию: пусто (0 символов) или "70";
		 * Примечание: Использование букв вместо цифр разрешается, но это будет сделать невозможным. Включить сервисный режим (режим 7 главного меню) используя клавиатуру ККТ.
		 * ▪ ServMessage – Сообщение, которое будет печататься когда наступит "ServiceDate". Количество строк определено в "Index" (0-9);
		* ▪ ServiceDate – Дата сервисного обслуживания (формат DD-MM-YY hh:mm:ss);
		* o Параметры чека;
		* ▪ PrnQuality – Контрастность печати (от 0 до 20);
		* ▪ DublReceipts – Печать дубликатов чеков (1 - включена, 0 - выключена);
		* ▪ InUseReceipts – Количество служебных чеков ( от 0 до 9);
		* ▪ BarcodePrint – Печать штрих-кода PLU в чеке (1 - включена, 0 - выключена);
		* ▪ LogoPrint - Включение/Выключение печати логотипа (1 - включена, 0 - выключена);
		* Примечание: Файл логотипа должен отвечать определенным условиям. Файл должен быть в формате BMP со следующими характеристиками: Ширина: 576 точек Высота: до 360 точек
			* ▪ ForeignPrint – Печать курса обмена при оплате в иностранной валюте (1 - включена, 0 - выключена);
		* ▪ QRcodeSize – Размер QR-кода;
		* ▪ VatSysByDefault – Система налогообложения в фискальных чеках (1,2,4,8,16,32).
			* o 1 - "Общая";
		* o 2 - "Упрощенная доход";
		* o 4 - "Упрощенная доход минус расход";
		* o 8 - "Единый налог на вмененный доход";
		* o 16 - "Единый сельскохозяйственный налог"; o 32 - "Патентная система налогообложения";
		* o Модем (только для моделей ККТ, оборудованных модемом GPRS);
		* ▪ ModemModel – Модель модема (0 – Quectel M72, 1 – Quectel UC20, 2-Quectel M66);
		* ▪ SimPin – пин код СИМ-карты. Текст до 16 символов.
			* ▪ APN – Наименование точки доступа. Строка текста до 64 символов. Номер APN определен в "Index";
		* ▪ APN_User – Имя пользователя APN. Строка текста до 32 символов. Номер APN определен в "Index";
		* ▪ APN_Pass – Пароль APN. Строка текста до 32 символов. Номер APN определен в "Index";
		* ▪ SimICCID – номер ICC СИМ-карты Текст до 31 символов (только для чтения);
		* ▪ SimIMSI – номер IMSI СИМ-карты. Текст до 16 символов (только для чтения);
		* ▪ SimTelNumber – номер MSISDN СИМ-карты. Текст до 16 символов (только для чтения);
		* ▪ IMEI – номер IMEI модема. (только для чтения);
		* o Сеть (только для ККТ оборудованных интерфейсом LAN);
		* ▪ LanMac – MAC адрес сетевой карты Ethernet (до 12 знаков);
		* ▪ DHCPenable – Флаг "Использовать DHCP при соединении по LAN" (1 - включен, 0 - выключен);
		* ▪ LAN_IP – IP-адрес, при выключенном флаге DHCP (до 15 символов);
		* ▪ LAN_NetMask – Маска подсети, при выключенном флаге DHCP (до 15 символов); ▪ LAN_Gateway – Шлюз по умолчанию, при выключенном флаге DHCP (до 15 символов);
		* ▪ LAN_PriDNS - Предпочитаемый DNS, при выключенном флаге DHCP (до 15 символов);
		* ▪ LAN_SecDNS - Альтернативный DNS, при выключенном флаге DHCP (до 15 символов);
		* o WiFi (только для модели ars.mobile Ф);
		* ▪ NetInterfaceAutoDetect – Автовыбор интерфейса передачи данных в ОФД - WiFi/GPRS ( 0 - выключен, 1 - включен ), по умолчанию – 1;
		* ▪ WLAN_DHCPenable – Флаг "Использовать DHCP при соединении по WLAN" (1 - включен, 0 - выключен);
		* ▪ WLAN_IP – IP-адрес, при выключенном флаге DHCP (до 15 символов);
		* ▪ WLAN_NetMask – Маска подсети, при выключенном флаге DHCP (до 15 символов); ▪ WLAN_Gateway – Шлюз по умолчанию, при выключенном флаге DHCP (до 15 символов);
		* ▪ WLAN_PriDNS - Предпочитаемый DNS, при выключенном флаге DHCP (до 15 символов);
		* ▪ WLAN_SecDNS - Альтернативный DNS, при выключенном флаге DHCP (до 15 символов);
		* ▪ WLAN_AP_SSID - SSID точки доступа WLAN. Текст до 32 символов. Количество WLAN APN определено в "Index";
		* ▪ WLAN_AP_Password – Пароль точки доступа WLAN. Текст до 32 символов. Количество WLAN APN определено в "Index";
		* ▪ WLAN_AP_Security – Тип шифрования точки доступа WLAN. 0 - Open, 1 - WEP, 2 -
			* WPA, 3 - WPA2; Количество WLAN APN определено в "Index"; o Дисконтные карты;
		* ▪ DiscountSystemCardName – Наименование дисконтной карты в системе (до 16 символов). Номер карты определен в "Index";
		* ▪ DiscountSystemCardNumber – Номер дисконтной карты в системе (от 1 до 999). Номер карты определен в "Index";
		* ▪ DiscountSystemPercentage – Размер процентной скидки в дисконтной системе (от 0 до 9999). Номер карты определен в "Index";
		* o Параметры сервера ОФД (Требуется ввод пароля режима Сервис или установка сервисной перемычки);
		* ▪ DtOFD – наименование ОФД;
		* ▪ DtFNS – Адрес сайта ФНС;
		* ▪ DtINNOFD – ИНН оператора фискальных данных (до 12 символов);
		* ▪ DtServerAddress – IP-адрес или имя сервера ОФД (до 127 символов);
		* ▪ DtServerPort – порт сервера ОФД (0-65535);
		* ▪ DtFNReadingPeriod – Интервал опроса фискального накопителя в секундах; ▪ DtFNConnectPeriod – Таймер С! в секундах. Таймер соединения с ОФД;
		* o Параметры регистрации ККТ;
		* ▪ param_1_user_line_1 – Наименование пользователя строка 1;
		* ▪ param_1_user_line_2 – Наименование пользователя строка 2;
		* ▪ param_1_user_line_3 – Наименование пользователя строка 3;
		* ▪ param_1_user_line_4 – Наименование пользователя строка 4;
		* ▪ param_2_address_line_1 – Адрес расчетов строка 1;
		* ▪ param_2_address_line_2 – Адрес расчетов строка 2;
		* ▪ param_2_address_line_3 – Адрес расчетов строка 3;
		* ▪ param_2_address_line_4 – Адрес расчетов строка 4;
		* ▪ param_3_inn – ИНН;
		* ▪ param_4_regid – Регистрационный номер;
		* ▪ param_5_kkt_id – Номер ККТ (только для чтения);
		* ▪ param_6_vat_system – Система налогообложения;
		* ▪ param_7_autonomus – Флаг «Автономный режим»;
		* ▪ param_8_auto_mode – Флаг «Автоматическая»;
		* ▪ param_9_sign_bso – Флаг БСО;
		* ▪ param_10_sign_services – Флаг «ККТ для услуг»;
		* ▪ param_11_sign_crypto – Флаг «Шифрование»;
		* ▪ param_12_sign_epays – Флаг «ККТ для интернета»;
		* ▪ param_13_automat_id – Флаг «Номер автомата»;
		* ▪ param_14_inspector_name – ФИО кассира;
		* ▪ param_15_service_name – Наименование сервисного центра (АСЦ);
		* ▪ param_16_rereg_reason – причина изменения параметров ККТ/перерегистрации;
		* param_17_sender – Электронный адрес отправителя; param_18_prn_in_automat – Принтер в автомате;
		* • param_19_inspector_inn – ИНН кассира;
		* • VatSysByDefault – Система налогообложения по умолчанию; •
			* o Переменные для ФП (Только чтение);
		* ▪ nZreport – Номер текущего Отчета закрытия смены;
		* ▪ nReset – Номер текущего обнуления ОЗУ;
		* ▪ nVatChanges – Номер текущего изменения ставок НДС;
		* ▪ nIDnumberChanges – Флаг состояния заводского номера на данный момент (0 – не запрограммирован; 1 - запрограммирован);
		* ▪ nFMnumberChanges – Флаг состояния номера ФП на данный момент (0 – не запрограммирован; 1 - запрограммирован);
		* ▪ nTAXnumberChanges – Флаг состояния номера РНМ (0 – не запрограммирована;1 - запрограммирована);
		* ▪ nREGnumberChanges - Флаг состояния регистрационного номера (0 – не запрограммирована;1 - запрограммирована);
		* ▪ valVat – Текущее значение ставки НДС. Номер ставки НДС определен в "Index";
		* ▪ FMDeviceID – Идентификационный номер ФП;
		* ▪ IDnumber – Заводской номер ККТ;
		* ▪ TAXnumber – РН ККТ;
		* ▪ FmWriteDateTime – Дата и время записи блока в ФП;
		* ▪ LastValiddate – Последняя корректная дата (записана в ФП и в ЭЖ);
		* o Внутренние переменные (Только чтение);
		* ▪ Fiscalized – Флаг состояния фискализации (1 - фискализирован, 0 – не фискализирован);
		* ▪ DFR_needed - Флаг, который показывает, есть ли фискальный чек после последнего Отчета закрытия смены (1 - есть, 0 - нет);
		* ▪ DecimalPoint – количество знаков после десятичного разделителя;
		* ▪ nBon – Общее количество чеков;
		* ▪ nFBon – Общее количество фискальных чеков;
		* ▪ nFBonDailyCount – Количество фискальных чеков текущей смене;
		* ▪ Block24h – Дата и время блокировки 'Прошло 24 часа';
		* ▪ CurrClerk – Номер текущего кассира;
		* ▪ EJNewJurnal – Новый Электронный журнал (ЭЖ);
		* ▪ EJNumber – Номер текущего ЭЖ;
		* ▪ fDateTimeUserSet – Флаг установки времени (один раз после Отчета закрытия смены, 1 – установлен, 0 – не установлен);
		* ▪ printed_copy – Флаг показывает была ли печать дубликатов чеков ( 1 – была, 0 – не была);
		* o Группы товаров;
		* ▪ ItemGroups_name – Наименование группы товаров. Строка текста до 32 символов. Номер группы товаров определен в "Index";
		* o Отделы;
		* ▪ Dept_name – Наименование отдела. Строка текста до 32 символов. Номер отдела определен в "Index";
		* ▪ Dept_ext_name – Расширенное наименование отдела. Строка текста до 32 символов. Номер отдела определен в "Index";
		* ▪ Dept_price – Максимальная цена для операций в отделе (0-999999999). Если Dept_price = 0.00, то ограничений нет; Номер отдела определен в "Index";
		* ▪ Dept_vat – Ставка НДС в отделе ( от 1 до 6). По умолчанию 1. Номер отдела определен в "Index";
		* • Index – Используется как индекс если переменная является массивом. Если переменная не является массивом, то "Index" должен быть пустым. По умолчанию: 0;
		* • Value – Если этот параметр пустой, то ККТ вернет текущее значение (Ответ(2)). Если значение установлено, то ККТ запрограммирует это значение (Ответ(1));
		* Ответ(1): {ErrorCode}<SEP>
			* • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		* Ответ(2): {ErrorCode}<<SEP>{VarValue}<SEP>
			* • ErrorCode - Указывает на ошибку или ее отсутствие; Ответ «0» = команда выполнена;
		* • VarValue – Текущее значение переменной;
		* \endcode
		*/
		void programming(const char *name, const char *param, const char *value);
	};
};
