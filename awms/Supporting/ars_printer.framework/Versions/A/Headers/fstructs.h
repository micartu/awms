#pragma once

namespace arsp {
	const char lib_version[] = "1.21.5";

	/**
	 * constants of possible commands which printer can execute
	 */
	typedef enum
	{
		FC_38_OpeningANonFiscalReceipt = 38,
		FC_39_ClosingANonFiscalReceipt = 39,
		FC_42_PrintingOfAFreeNonFiscalText = 42,
		FC_44_PaperFeed = 44,
		FC_45_CheckModeConnection = 45,
		FC_48_OpenFiscalReceipt = 48,
		FC_49_RegistrationOfSale = 49,
		FC_50_ReturnVAT = 50,
		FC_51_Subtotal = 51,
		FC_53_PaymentsCalculation = 53,
		FC_54_PrintFreeFiscalText = 54,
		FC_56_CloseFiscalReceipt = 56,
		FC_57_WriteGSMorEMailToClient = 57,
		FC_58_RegisterSaleOfProgrammedIcon = 58,
		FC_60_CancelFiscalReceipt = 60,
		FC_61_SetDateTime = 61,
		FC_62_ReadDateTime = 62,
		FC_64_LastFiscalEntry = 64,
		FC_65_dailyTaxationInfo = 65,
		FC_68_remainingEntriesZReportsFM = 68,
		FC_69_Reports = 69,
		FC_70_cashInOutOps = 70,
		FC_71_printDiagnostic = 71,
		FC_72_Fiscalization = 72,
		FC_74_ReadStatus = 74,
		FC_76_ReadReceiptStatus = 76,
		FC_77_ReadFNStatus = 77,
		FC_80_PlaySound = 80,
		FC_81_SwitchOffPrinting = 81,
		FC_83_ProgramVATRates = 83,
		FC_84_PrintBarCode = 84,
		FC_86_LastFiscalDate = 86,
		FC_87_ItemGroupInfo = 87,
		FC_88_DepartmentInfo = 88,
		FC_89_FiscalMemoryTest = 89,
		FC_90_DiagnosticInformation = 90,
		FC_91_SerialNumberProgramming = 91,
		FC_92_PrintSeparateLine = 92,
		FC_94_FiscalMemoryReport = 94,
		FC_95_FiscalMemoryZReport = 95,
		FC_98_programTAX = 98,
		FC_99_readTAXNumber = 99,
		FC_100_ReadError = 100,
		FC_101_SetPassword = 101,
		FC_102_RegisterOperator = 102,
		FC_103_ReceiptInfo = 103,
		FC_105_OperatorsReport = 105,
		FC_106_DrawerOpenning = 106,
		FC_107_ItemsOperation = 107,
		FC_109_PrintDublicateReceipt = 109,
		FC_110_AdditionalDInfo = 110,
		FC_111_PluReport = 111,
		FC_112_InfoForOperator = 112,
		FC_116_ReadFM = 116,
		FC_121_ReadMem = 121,
		FC_124_SearchDocument = 124,
		FC_125_InfoEJ = 125,
		FC_127_StampOperation = 127,
		FC_144_OFD_Registration = 144,
		FC_202_Load_Logo = 202,
		FC_203_Load_Stamp = 203,
		FC_253_ServiceParameters = 253,
		FC_255_Programming = 255,

	} FiscalCommand;

	/**
	 * status of printer
	 */
	struct printerStatus {
		// first byte
		int syntaxError;
		int commandCodeInvalid;
		int notUsed0[2];
		int printingMechanismFailure;
		int commonError;
		int coverClosed;
		int alwaysOne0;

		// second byte
		int executionOverflow;
		int badCommand;
		int notUsed1[6];

		// third byte
		int noPaper;
		int paperNearEnd;
		int EJFull;
		int fiscalReceiptIsOpenned;
		int EJNearlyFull;
		int nonFiscalReceiptIsOpenned;
		int notUsed2[2];

		// fourth byte
		int notUsed3[8];

		// fifth byte
		int errInFN;
		int taxNumberIsSet;
		int serialAndFMNumerAreSet;
		int spaceForLessThen60Records;
		int fiscalMemoryFull;
		int notUsed4[3];
	};

	/**
	 * struct used as a parameter for printNonFiscalText
	 */
	struct nonFiscalTextParams {
		/// text to be printed
		char text[48];
		/// text should be bold or not
		int bold;
		/// .. italic or not
		int italic;
		/// text of double height
		int doubleH;
		/// underlined or not
		int underline;
		/// {0 = left, 1 = center, 2 = right} aligned
		int alignment;
		/// condensed or not
		int condensed;
	};

	/**
	 * struct used as a parameter for openFiscalReceipt
	 */
	struct fiscalReceiptParams {
		/// Operator number from 1...30
		int OpCode;
		/// Operator password, ascii string of digits. Lenght from 1...8
		char OpPwd[8];
		/// Number of point of sale from 1...99999
		int TillNmb;

		/**
		 * Type of sale
		 * o 0 - SALES "Приход";
		 * o 1 - RETURN_SALES "Возврат прихода";
		 * o 2 - PURCHASES "Расход";
		 * o 3 - RETURN_PURCHASES "Возврат расхода";
		 * o 4 - SHIFT_OPEN "Открытие смены";
		 * o 10 - CORRECTION OF SALES "Коррекция Приход";
		 * o 11 - CORRECTION OF RETURN_SALES "Коррекция Возврат прихода";
		 * o 12 - CORRECTION OF PURCHASES "Коррекция Расход";
		 * o 13 - CORRECTION OF RETURN_PURCHASES "Коррекция Возврат расхода";
		 */
		int Type;
		/// Buyer's e-mail address or phone number for electronic copy of the receipt
		char Buyer[255];
		/// Vat system for the receipt. If is empty default vat system is used;
		char VatSystem[10];
		/// opearator name for OpCode = 30 name can be changed many times otherwise only once
		char OpName[100];
	};

	/**
	 * struct used as a parameter for saleRegistration and saleRegistrationProgIt (a programmed item)
	 */
	struct saleRegistrationParams {
		/// product name or code (for a programmed item)
		char PluName[64];
		/// VAT group (digit 1...6)
		int TaxGr;
		/// Product price ( 0.00...9999999.99 ). With sign '-' at void operations
		double Price;
		/// @details Optional parameters: {Quantity},{DiscountType},{DiscountValue}
		/// Department number (0...99). If '0' - Without department
		int Department;
		/// Quantity of the product ( 0.001...99999.999 ). Default: 1.000
		double Quantity;
		/// @details !!! Max value of {Price} * {Quantity} is 9999999.99 !!!
		/// Type. Default: 0;
		/// DiscountType: '0' - no discount; '1' - surcharge by percentage; '2' - discount by percentage
		int DiscountType;
		/// Value  ( 0.00...100.00 ) - for percentage operations
		double DiscountValue;
	};

	/**
	 * struct used as a parameter for subtotal
	 */
	struct subtotalParams {
		/// print out. Default: 0 (No print out); '1' - The sum of the subtotal will be printed out;
		int Print;
		/// Show the subtotal on the client display. Default: 0; '1' - The sum of the subtotal will appear on the display;
		int Display;
		/// DiscountType: '0' - no discount; '1' - surcharge by percentage; '2' - discount by percentage
		int DiscountType;
		/// Value  ( 0.00...100.00 ) - for percentage operations
		double DiscountValue;
	};

	enum PaidModes {
		PaidModeCash = 0,
		PaidModeCard = 1,
		PaidModeCredit = 2,
		PaidModeTicket = 3,
		PaidModeCoupon = 4,
		PaidModePay5 = 5,
		PaidModeForeign = 6,
	};

	/**
	 * struct used as a parameter for paymentsCalculation
	 */
	struct paymentsCalculationParams {
		/// Type of payment
		enum PaidModes PaidMode;
		/// Amount to pay ( 0.00÷9999999.99)
		double Amount;
		/// Type of change. Only if PaidMode = '6': '0' - main currency; '1' - foreign currency ( Forbidden !);
		int Change;
	};

	enum gsmEmailType {
		/// GSM number
		gsmEmailTypeGSM = 0,
		/// E-mail address;
		gsmEmailTypeEmail = 1,
		/// Cancel data (erase previously entered data);
		gsmEmailTypeCancel = 2,
		/// Correction Type
		gsmEmailTypeCorrection = 3,
		/// Ground for correction
		gsmEmailTypeGround4Correction = 4,
		/// Date of the document
		gsmEmailTypeDocumentDate = 5,
		/// Number of document
		gsmEmailTypeDocumentNumber = 6,
		/// address of transfer operator
		gsmEmailTypeAddrTransferOperator = 1005,
		/// INN of transfer operator
		gsmEmailTypeINNTransferOp = 1016,
		/// Name of transfer operator
		gsmEmailTypeNameTransferOp = 1026,
		/// Operation of payment agent
		gsmEmailTypePayAgentOperation = 1044,
		/// Agent sign
		gsmEmailTypeAgentSign = 1057,
		/// Telephone of Payment Agent
		gsmEmailTypeTelephonePayAgent = 1073,
		/// Telephone of Payment Operator
		gsmEmailTypeTelephonePayOperator = 1074,
		/// Telephone of Transfer Operator
		gsmEmailTypeTelephoneTransfOperator = 1075,
		/// Telephone of Supplier
		gsmEmailTypeTelephoneSupplier = 1171,
	};

	/**
	 * struct used as a parameter for writeGSMorEmailForFiscalReceipt
	 */
	struct gsmEmailParams {
		enum gsmEmailType Type;
		union {
			char phone[16];
			char email[64];
			char out[64];
		};
	};

	/**
	 * struct used as a parameter for setDateTime
	 */
	struct dateTime {
		/// day of the month
		uint8_t day;
		/// month of the year
		uint8_t month;
		/// year
		uint8_t year;
		/// hour in 24h format
		uint8_t hour;
		/// minute (0-60)
		uint8_t minute;
		/// second (0-60)
		uint8_t second;
	};

	/**
	 * struct used as a parameter for lastFiscalEntryInfo
	 */
	struct lastFiscalEntryParams {
		/**
		 * Type of returned data. Default: 0;
		 * o 0 - Turnovers by TAX groups including CORRECTIONS;
		 * o 1 - VAT amounts by TAX groups including CORRECTIONS;
		 * o 2 - Turnovers by TAX groups from CORRECTIONS only;
		 * o 3 - VAT amounts by TAX groups from CORRECTIONS only;
		 */
		uint8_t type;
		/**
		 * Type of receipts. Default: 0; o '0' - SALES "Приход";
		 * o '1' - RETURN_SALES "Возврат прихода";
		 * o '2' - PURCHASES "Расход";
		 * o '3' - RETURN_PURCHASES "Возврат расхода";
		 */
		uint8_t rec_type;
	};

	// possible parameters (as constants) for report

	/**
	 * parameter which generates X-Report
	 */
	const char XReport = 'X';

	/**
	 * parameter which generates Z-Report
	 */
	const char ZReport = 'Z';

	/**
	 * possible types of a separate line
	 */
	enum separateLineType {

		separateLineTypeMinus = 1,
		separateLineTypeMinusAndBlank = 2,
		separateLineTypeEqual = 3,
	};

	/**
	 * struct used as a parameter for fiscalization
	 */
	struct fiscalizationParams {
		/// Serial Number (10 ascii digits);
		char serialNumber[10];

		/// TAX number (10 or 12 ascii digits);
		char taxNumber[12];

		/// REG number (up to 16 ascii digits);
		char regNumber[16];

		///  New inspector password (up to 8 digits);
		char InspPssword[8];
	};

	/**
	 * struct used as a parameter for programVAT
	 */
	struct programVATParams {
		/**
		 * Value of VAT rate X = index of array + 1;
		 * o 0.00÷99.99 - taxable;
		 * o 100.00 - exempt;
		 * o 100.01 - disabled;
		 */
		float tax[6];

		/**
		 * how to interpret the prices:
		 * o '0' - work with integer prices (Forbidden!);
		 * o '2' - work with fractional prices;
		 */
		uint8_t decimal_point;
	};

	/**
	 * struct used as a parameter for printBarCode
	 */
	struct barCodeParams {
		/**
		 * Type of the barcode;
		 * o '1' - EAN8 barcode. Data must contain only 8 digits;
		 * o '2' - EAN13 barcode. Data must contain only 13 digits;
		 * o '3' - Code128 barcode. Data must contain symbols with ASCII codes between 32 and 127.
		 * Length of Data is between 3 and 31 symbols;
		 * o '4' - QR code. Data must contain symbols with ASCII codes between 32 and 127.
		 * Length of Data is between 3 and 279 symbols;
		 */
		char type;

		/**
		 * Data of the barcode. Length of Data depents on Type;
		 */
		char data[280];

		/**
		 * Dots multiplier (3÷10). Default: 5;
		 */
		char QRcodeSize;
	};

	/**
	 * struct used as a parameter for fiscalMemoryReport
	 */
	struct fiscalMemoryReportParams {
		/**
		 * Type of report;
		 * o '0' - Short;
		 * o '1' - Detailed;
		 * o '10' - Short to file;
		 * o '11' – Detailed to file
		 * o '3' - Read next line of report 10 or 11 as text;
		 */
		char type;

		/**
		 * Start date. Default: Date of „Ввод ИНН и РН ККТ” (format DD-MM-YY);
		 */
		struct dateTime startDate;

		/**
		 * End date. Default: Current date (format DD-MM-YY);
		 */
		struct dateTime endDate;
	};

	/**
	 * struct used as a parameter for fiscalMemoryZReport
	 */
	struct fiscalMemoryZReportParams {
		/**
		 * Type of report;
		 * o '0' - Short;
		 * o '1' - Detailed;
		 * o '10' - Short to file;
		 * o '11' - Detailed to file
		 * o '3' - Read next line of report 10 or 11 as text;
		 * o '5' - Shift status report. Optional parameters First and Last do not matter;
		 * o '6' - Print documents from FN; Print all fiscal documents from First to Last;
		 * o '7' - FN reregistrations report; Print all reregistrations from First to Last.
		 */
		char type;

		/**
		 * First block in the report. Default: 1;
		 */
		char first;

		/**
		 * Last block in the report. Default: number of last Z report;
		 */
		char last;
	};

	/**
	 * struct used as a parameter for setPassword
	 */
	struct password {
		/**
		 * Operator number (1÷30)
		 */
		char OpCode;

		/**
		 * Operator password, ascii string of digits. Lenght 4÷8;
		 */
		char newPass[8];

		/**
		 * Operator old password or administrator (oper29 & oper30) password.
		 * Can be blank if service jumper is on.
		 */
		char oldPass[8];
	};

	/**
	 * struct used as a parameter for command FC_107_ItemsOperation
	 */
	struct itemsInfo {
		/// item number (1-3000)
		short PluNum;
		///  last item to delete (1÷3000). Default: {firstPLU = PluNum}; (for deletion command)
		short lastPlu;
		/// VAT group (digit '1'÷'6' or letter 'A'÷'F' or cyrillic 'А'÷'Е')
		char TaxGr[2];
		/// Department number (0...99). If '0' - Without department
		int Dep;
		/// Item group (1÷99);
		char Group;
		///  Price type ('0' - fixed price, '1' - free price, '2' - max price);
		char PriceType;
		/// Product price (0.00...9999999.99). With sign '-' at void operations
		double Price;
		/// Stock quantity (0.000÷99999.999)
		double Quantity;
		/// item's name
		char name[32];
		/// Barcode X (up to 13 digits)
		int BarCode[4];
	};

	/**
	 * struct used as a parameter for searchDocuments
	 */
	struct searchDocsParams {
		/**
		 * Start date of searching. Default: Date of first document;
		 * if day equals 0, then date is ignored
		 */
		struct dateTime startDate;

		/**
		 * End date of searching. Default: Date of last document;
		 * if day equals 0, then date is ignored
		 */
		struct dateTime endDate;

		/**
		 * Type of document;
		 * o '0' - all types;
		 * o '1' - fiscal receipts (including Z-reports); o '2' - non fiscal receipts;
		 * o '3' - Z-reports;
		 */
		char type;
	};
};
