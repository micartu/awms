#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "fstructs.h"

#define MAX_STATUS_BITS 	(8 * 8)

namespace arsp {

	/**
	 * class for notification about printer's answer
	 */
	class rtask
	{
		uint8_t* 	_data;
		int    		_len;
		const char* 	_msg;
		int    		_mlen;
		const uint8_t* 	_status;
		int    		_cmd;
		int    		_seq;
		struct printerStatus _prStatus;

		void parseStatus(const uint8_t* data) {
			size_t len2status = sizeof(_prStatus) / sizeof(int);
			int *status = (int*)&_prStatus;
			memset(&_prStatus, 0, sizeof(_prStatus));
			if (len2status > MAX_STATUS_BITS)
				len2status = MAX_STATUS_BITS;
			for (size_t i = 0; i < len2status; ++i) {
				uint8_t byte = data[i / 8];
				int offset = (i % 8);
				status[i] = (byte >> offset) & 0x1;
			}
		}

	public:

		rtask(int command, int seq) :
			_data(NULL),
		       	_len(0),
			_msg(NULL),
			_mlen(0),
			_status(NULL),
		       	_cmd(command),
		       	_seq(seq) {
			}
		~rtask() { if (_data) free(_data); }

		/**
		 * returns the data part of a packet which was received
		 * from printer. Data consist of:
		 * message itself (string) + 0x4 + 8 bytes of status
		 */
		uint8_t* data() { return _data; }

		/**
		 * sets data after parsing the packet (shouldn't be called directly by user)
		 */
		void setData(uint8_t* data) { _data = data; }

		/**
		 * returns the length of data
		 */
		int len() { return _len; }

		/**
		 * sets the length of data (shouldn't be called directly by user)
		 */
		void setLen(int len) {
			// parse message out of data
			if (len && _data) {
				_msg = (const char*)_data;
				_mlen = len - 9;
				_status = _data + len - 8;
			}
			_len = len;
		}

		/**
		 * returns sequence number of the received packet
		 */
		int seq() { return _seq; }

		/**
		 * retuns command number of the packet
		 */
		int cmd() { return _cmd; }

		/**
		 * a pointer to message within data
		 */
		const char* msg() { return _msg; }

		/**
		 * message's length = _len - 9
		 */
		int msg_len() { return _mlen; }


		/**
		 * status bytes of the packet
		 */
		struct printerStatus* status() {
			if (_len > 8) {
				parseStatus(_status);
				return &_prStatus;
			}
			return NULL;
		}
	};
};
