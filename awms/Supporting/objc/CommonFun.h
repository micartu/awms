//
//  CommonFun.h
//  ScannerPrototype
//
//  Created by Michael Artuerhof on 26.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#ifndef CommonFun_h
#define CommonFun_h

@class NSString;

void PFDebugLog(NSString *format, ...);

#endif /* CommonFun_h */
