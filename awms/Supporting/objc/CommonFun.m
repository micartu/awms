//
//  CommonFun.m
//  ScannerPrototype
//
//  Created by Michael Artuerhof on 26.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import <Foundation/Foundation.h>

void PFDebugLog(NSString *format, ...) {
    va_list ap;
    va_start(ap, format);
    NSLog(format, ap);
    va_end(ap);
}
