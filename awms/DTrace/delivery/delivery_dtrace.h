//
//  delivery_dtrace.h
//  awms
//
//  Created by Michael Artuerhof on 19.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#ifndef delivery_dtrace_h
#define delivery_dtrace_h
#import "delivery.h"

/// probe called every time inner state of the delivery interactor has changed
static inline void BMTraceDeliveryStateChanged(int d) {
    if (AWMS_DELIVERY_DELIVERY_STATE_CHANGED_ENABLED()) {
        AWMS_DELIVERY_DELIVERY_STATE_CHANGED(d);
    }
}

#endif /* delivery_dtrace_h */
