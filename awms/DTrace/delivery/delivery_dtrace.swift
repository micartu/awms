//
//  delivery_dtrace.swift
//  awms
//
//  Created by Michael Artuerhof on 19.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

@inline(__always) func DTraceDeliveryStateChanged(to: DeliveryInteractorStatus) {
    BMTraceDeliveryStateChanged(Int32(to.rawValue))
}
