provider awms_graph {
    // graph is being recalculated for a given start node
    probe begin_recalc_graph_for(int);
    // graph has been recalculated for a given start node
    probe end_recalc_graph_for(int);
    // relax node1 to node2 with weight
    probe relax_node(int, int, int);
    // calculated weight (2) in node (1)
    probe node_weight(int, int);
    // next edge in path: From, to
    probe path_edge(int, int);
};
