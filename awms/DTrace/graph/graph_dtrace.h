//
//  graph_dtrace.h
//  awms
//
//  Created by Michael Artuerhof on 19.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#ifndef graph_dtrace_h
#define graph_dtrace_h
#import "graph.h"

/// called every time graph is being recalculated for a given starting point
static inline void BMTraceGraphBeginRecalculateFor(int d) {
    if (AWMS_GRAPH_BEGIN_RECALC_GRAPH_FOR_ENABLED()) {
        AWMS_GRAPH_BEGIN_RECALC_GRAPH_FOR(d);
    }
}

/// called every time graph recalculation is finished
static inline void BMTraceGraphEndRecalculateFor(int d) {
    if (AWMS_GRAPH_END_RECALC_GRAPH_FOR_ENABLED()) {
        AWMS_GRAPH_END_RECALC_GRAPH_FOR(d);
    }
}

/// called if the node is being relaxed
static inline void BMTraceGraphRelaxNode(int d, int to, int weight) {
    if (AWMS_GRAPH_RELAX_NODE_ENABLED()) {
        AWMS_GRAPH_RELAX_NODE(d, to, weight);
    }
}

/// called if the node is being relaxed
static inline void BMTraceGraphNodeWeight(int d, int weight) {
    if (AWMS_GRAPH_NODE_WEIGHT_ENABLED()) {
        AWMS_GRAPH_NODE_WEIGHT(d, weight);
    }
}

/// called from path creation
static inline void BMTraceGraphPathTo(int from, int to) {
    if (AWMS_GRAPH_PATH_EDGE_ENABLED()) {
        AWMS_GRAPH_PATH_EDGE(from, to);
    }
}

#endif /* graph_dtrace_h */
