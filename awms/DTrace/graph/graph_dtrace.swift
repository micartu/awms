//
//  graph_dtrace.swift
//  awms
//
//  Created by Michael Artuerhof on 19.03.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

@inline(__always) func ScaledDouble(_ w: Double) -> Int32 {
    return Int32(w * 100)
}

@inline(__always) func DTraceGraphBeginRecalculateFor(node: Int) {
    BMTraceGraphBeginRecalculateFor(Int32(node))
}

@inline(__always) func DTraceGraphEndRecalculateFor(node: Int) {
    BMTraceGraphEndRecalculateFor(Int32(node))
}

@inline(__always) func DTraceGraphRelax(_ w: Int, _ v: Int, _ weight: Double) {
    BMTraceGraphRelaxNode(Int32(w), Int32(v), ScaledDouble(weight))
}

@inline(__always) func DTraceGraphNodeWeight(_ w: Int, weight: Double) {
    BMTraceGraphNodeWeight(Int32(w), ScaledDouble(weight))
}

@inline(__always) func DTraceGraphPath(from: Int, to: Int) {
    BMTraceGraphPathTo(Int32(from), Int32(to))
}
